Thank you for considering contributing to the Raven CMS! We don't have any specific
guidelines for this right now, but email us at hello@crowandraven.com if you are
interested.