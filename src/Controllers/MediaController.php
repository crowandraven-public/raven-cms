<?php
namespace CrowAndRaven\CMS\Controllers;

ini_set('upload_max_filesize', '20M');
ini_set('post_max_size', '20M');

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use CrowAndRaven\CMS\Models\Media;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class MediaController extends Controller
{
    /**
     * Check if user is logged in and admin to allow to view these pages
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display media admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['media'] = Media::orderBy('updated_at', 'desc')->paginate(15);
        $this->view_data['request'] = $request;

        return view('raven::media.index', $this->view_data);
    }

    public function getAllWithJSON(Request $request)
    {
        if (!$request->input('params')) {
            return;
        }
        if (!array_key_exists('q', $request->input('params'))) {
            return;
        }
        $all_media = Media::where('title', 'like', $request->input('params')['q'].'%')->get();
        return response()->json($all_media);
    }

    /**
     * Upload new media
     * @param  Request $request
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        
        if (!$request->file('file')) {
            return Redirect::back();
        }

        $upload = $request->file;
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

        //$path = $request->file('file')->store('public/uploads');
        $newfilename = substr(md5(rand()), 0, 9);
        $path = $request->file('file')->storeAs('public/uploads', $newfilename.'.'.$ext);

        // save new media
        $media = new Media;
        $media->title = $request->title;
        $media->type = $upload->getClientMimeType();
        $media->file = $newfilename.'.'.$ext;
        $media->created_by = $user->id;

        // generate flash messages
        if ($media->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', 'Media Uploaded');
            $request->session()->flash('flash_message', 'Woohoo, good work.');
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', 'Media Error');
            $request->session()->flash('flash_message', 'Hmmm, there was a problem uploading the media.');
        }

        return Redirect::back();
    }

    /**
     * Update media
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        $media = Media::findOrFail($id);

        $media->title = $request->title;

        // generate flash messages
        if ($media->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', 'Media Updated');
            $request->session()->flash('flash_message', 'Woohoo, good work.');
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', 'Media Error');
            $request->session()->flash('flash_message', 'Hmmm, there was a problem updating the media.');
        }

        return Redirect::back();
    }

    /**
     * Delete media
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $media = Media::findOrFail($id);

        if ($media->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', 'Media Deleted');
            $request->session()->flash('flash_message', 'The media was successfully deleted.');
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', 'Deletion Error');
            $request->session()->flash('flash_message', 'Hmmm, there was an error deleting that media.');
        }

        // actually delete the file
        Storage::delete('public/uploads/'.$media->file);

        return Redirect::back();
    }
}
