<?php

namespace CrowAndRaven\CMS\Controllers;

use CrowAndRaven\CMS\Models\Like;
use CrowAndRaven\CMS\Models\Post;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function likePost($id)
    {
        $exists = Post::find($id)->exists();

        if ($exists) {
            return $this->handleLike('posts', $id);
        }

        $this->getJsonFail();
    }

    public function handleLike($type, $id)
    {
        $existing_like = Like::withTrashed()->whereLikeableType($type)->whereLikeableId($id)->whereUserId(Auth::id())->first();
        $liked = null;

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => Auth::id(),
                'likeable_id'   => $id,
                'likeable_type' => $type,
            ]);
            $liked = true;
        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
                $liked = false;
            } else {
                $existing_like->restore();
                $liked = true;
            }
        }
        
        return response()->json([
            'success' => true,
            'liked' => $liked,
            'post_id' => $id,
            'type' => $type
        ]);
    }

    public function getJsonFail()
    {
        return response()->json([
            'success' => false
        ]);
    }
}
