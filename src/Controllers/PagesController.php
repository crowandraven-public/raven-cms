<?php

namespace CrowAndRaven\CMS\Controllers;

use Auth;
use Config;
use CrowAndRaven\CMS\Models\Media;
use CrowAndRaven\CMS\Models\Page;
use CrowAndRaven\CMS\Models\Post;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Mail;

class PagesController extends Controller
{
    /**
     * Show the application home page.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $page = Page::getPageBySlug('home');
        
        return view('raven::pages.templates.'.$page->template, [
            'page' => $page
        ]);
    }

    /**
     * Show a specific static page
     * @param  Request $request
     * @param  string  $parent  parent slug
     * @param  string  $slug    page slug
     * @return
     */
    public function show(Request $request, $parent, $slug = null)
    {
        if (!$slug) {
            $slug = $parent;
        }
        
        $page = Page::getPageBySlug($slug);

        return view('raven::pages.templates.'.$page->template, [
            'page' => $page
        ]);
    }

    /**
     * Show the blog landing page.
     *
     * @return Response
     */
    public function blog(Request $request)
    {
        $page = Page::getPageBySlug('blog');
        
        return view('raven::pages.templates.'.$page->template, [
            'page' => $page,
            'posts' => Post::getPosts('blog')->latest()->paginate(10)
        ]);
    }

    /**
     * Contact form
     * @param  Request $request form data to send email
     */
    public function contact(Request $request)
    {
        $user = Auth::user();
        if (!$user) {
            $data['from_email'] = $request->email;
            $data['from_name'] = $request->name;
        } else {
            $data['from_email'] = $user->email;
            $data['from_name'] = $user->name;
        }

        $data['email_subject'] = 'Contact Us';
        $data['content'] = $request->content;

        Mail::send('pages.emails.contact', ['data' => $data], function ($message) use ($data) {
            $message->from(Config::get('site.contact.email'), Config::get('site.contact.name'));
            $message->to(Config::get('site.contact.email'), Config::get('site.contact.name'));
            $message->replyTo($data['from_email'], $data['from_name']);
            $message->subject($data['email_subject']);
        });

        $request->session()->flash('flash_type', 'info');
        $request->session()->flash('flash_title', 'Message Sent');
        $request->session()->flash('flash_message', 'Your message has been successfully sent.');

        return Redirect::back();
    }

    /**
     * Show the terms page.
     *
     * @return Response
     */
    public function terms(Request $request)
    {
        $page = Page::getPageBySlug('terms');
        
        return view('raven::pages.templates.default', [
            'page' => $page
        ]);
    }
}
