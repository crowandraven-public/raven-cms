<?php

namespace CrowAndRaven\CMS\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use CrowAndRaven\CMS\Models\Resource;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ResourcesController extends Controller
{
    /**
     * Check if user is logged in and admin to allow to view these pages
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display resources admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $this->view_data['user'] = Auth::user();

        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['resources'] = Resource::where('title', 'like', '%'.$request->search.'%')->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = 'Resources for Query "'.$request->search.'"';
        } else {
            $this->view_data['resources'] = Resource::orderBy('updated_at', 'desc')->paginate(20);
        }

        $this->view_data['request'] = $request;

        return view('cms::resources.index', $this->view_data);
    }

    /**
     * Display add resource form
     * @param  Request $request
     */
    public function create(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['request'] = $request;

        return view('cms::resources.create', $this->view_data);
    }

    /**
     * Create a new resource
     * @param  Request $request
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        
        // select file over link if it exists
        if (isset($request->url_file) && $request->url_file !='') {
            $url = $request->url_file;
            $type = 'local';
        } elseif (isset($request->url_link) && $request->url_link !='') {
            $url = $request->url_link;
            $type = 'external';
        } else {
            return Redirect::back();
        }

        // save new resources
        $resource = new Resource;
        $resource->title = $request->title;
        $resource->url = $url;
        $resource->type = $type;

        // generate flash messages
        if ($resource->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', 'Resource Created');
            $request->session()->flash('flash_message', 'Woohoo, good work.');
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', 'Resource Error');
            $request->session()->flash('flash_message', 'Hmmm, there was a problem creating the reseource.');
        }

        return Redirect::to('/admin/resources');
    }

    /**
     * Display edit resource
     * @param  Request $request
     */
    public function edit(Request $request, $id)
    {
        $this->view_data['user'] = Auth::user();
        $resource = Resource::findOrFail($id);

        if ($resource->type == 'external') {
            $resource->url_link = $resource->url;
            $resource->url_file = null;
        } else {
            $resource->url_file = $resource->url;
            $resource->url_link = null;
        }

        $this->view_data['resource'] = $resource;
        $this->view_data['request'] = $request;

        return view('cms::resources.edit', $this->view_data);
    }

    /**
     * Update resources
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $resource = Resource::findOrFail($id);

        // select file over link if it exists
        if (isset($request->url_file) && $request->url_file !='') {
            $url = $request->url_file;
            $type = 'local';
        } elseif (isset($request->url_link) && $request->url_link !='') {
            $url = $request->url_link;
            $type = 'external';
        } else {
            return Redirect::back();
        }

        $resource->title = $request->title;
        $resource->url = $url;
        $resource->type = $type;

        // generate flash messages
        if ($resource->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', 'Resource Updated');
            $request->session()->flash('flash_message', 'Woohoo, good work.');
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', 'Resource Error');
            $request->session()->flash('flash_message', 'Hmmm, there was a problem updating the resource.');
        }

        return Redirect::to('/admin/resources');
    }

    /**
     * Delete resource
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $resource = Resource::findOrFail($id);

        if ($resource->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', 'Resource Deleted');
            $request->session()->flash('flash_message', 'The resource was successfully deleted.');
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', 'Deletion Error');
            $request->session()->flash('flash_message', 'Hmmm, there was an error deleting that resource.');
        }

        return Redirect::back();
    }
}
