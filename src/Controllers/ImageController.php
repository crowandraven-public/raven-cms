<?php
namespace CrowAndRaven\CMS\Controllers;

use Illuminate\Contracts\Filesystem\Filesystem;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;
use \CrowAndRaven\CMS\Models\Attachment;

class ImageController extends Controller
{
    public function show(Filesystem $filesystem, $path)
    {
                
        if (preg_match('/jpe?g|png|gif|png|svg/', Attachment::getExt($path))) {
            $server = ServerFactory::create([
                'response' => new LaravelResponseFactory(app('request')),
                'source' => $filesystem->getDriver(),
                'source_path_prefix' => 'public/uploads',
                'cache' => $filesystem->getDriver(),
                'cache_path_prefix' => 'public/uploads/.cache',
                'base_url' => '',
            ]);
            return $server->getImageResponse($path, request()->all());
        }
        return response()->file(storage_path().'/app/public/uploads/'.$path);
    }
}
