<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use CrowAndRaven\CMS\Models\FieldGroup;
use CrowAndRaven\CMS\Models\Field;
use CrowAndRaven\CMS\Models\Page;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class FieldGroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }


    /**
     * Display field groups admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $this->view_data['fieldgroups'] = FieldGroup::orderBy('updated_at', 'desc')->paginate(20);
        $this->view_data['request'] = $request;
        $this->view_data['all_pages'] = Page::getPages()->pairs()->prepend('Select One', 0);
        $this->view_data['field_types_array'] = $this->getFieldTypes();

        return view('raven::admin.fieldgroups.index', $this->view_data);
    }


    /**
     * Display add form
     * @param  Request $request
     */
    public function create(Request $request)
    {
        $this->view_data['request'] = $request;
        $this->view_data['field_types_array'] = $this->getFieldTypes();
        $this->view_data['all_pages'] = Page::getPages()->pairs()->prepend('Select One', 0);

        return view('raven::admin.fieldgroups.create', $this->view_data);
    }


    /**
     * Duplicate a field group
     * @param  Request $request
     */
    public function copy(Request $request, $id)
    {
        $existing_field_group = FieldGroup::findOrFail($id);
        $field_group = new FieldGroup;
        $field_group->title = $existing_field_group->title.' (Copy)';
        $field_group->page_id = $existing_field_group->page_id;
        $field_group->save();

        $existing_fields = $existing_field_group->fields;

        foreach ($existing_fields as $f) {
            $new_field = new Field;
            $new_field->fieldgroup_id = $field_group->id;
            $new_field->name = $f->name;
            $new_field->type = $f->type;
            $new_field->attribs = $f->attribs;
            $new_field->order = $f->order;
            $new_field->description = $f->description;
            $new_field->page_id = $f->page_id;
            $new_field->fieldgroup_id = $field_group->id;
            $new_field->save();
        }

        $request->session()->flash('flash_type', 'success');
        $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.copy.title'));
        $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.copy.message'));

        return Redirect::back();

    }


    /**
     * Create a new Field Group
     * @param  Request $request
     */
    public function store(Request $request)
    {
        $field_group = new FieldGroup;
        $field_group->title = $request->title;
        $field_group->page_id = $request->page_id;

        if ($field_group->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.create.error.message'));
        }

        return Redirect::to('/admin/fieldgroups');
    }


    /**
     * Display edit page
     * @param  Request $request
     */
    public function edit(Request $request, $id)
    {
        $this->view_data['fieldgroup'] = FieldGroup::findOrFail($id);
        $this->view_data['all_pages'] = Page::getPages()->pairs()->prepend('Select One', 0);
        $this->view_data['field_types_array'] = $this->getFieldTypes();
        $this->view_data['request'] = $request;
        
        return view('raven::admin.fieldgroups.edit', $this->view_data);
    }


    /**
     * Update field group
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        
        $page = Page::find($request->page_id);
        
        if (!$page) {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.edit.error.message'));
            return Redirect::to('/admin/fieldgroups');
        }

        $field_group = FieldGroup::findOrFail($id);
        $field_group->title = $request->title;
        $field_group->page_id = $request->page_id;
        $all_fields = $request->all();
        $field_groups = (array_key_exists('fieldgroups', $all_fields)) ? $all_fields['fieldgroups'] : [];

        // diff previous fields (in db) to the fields sent over request
        $all_fields_ids = array_values(array_map(function ($f) {
            if (array_key_exists('field_id', $f)) {
                return $f['field_id'];
            }
        }, $field_groups));

        $all_previous_fields_ids = $field_group->fields->pluck('id')->toArray();
        $fields_to_remove = array_diff($all_previous_fields_ids, $all_fields_ids);

        // remove fields in the diff
        if (is_array($fields_to_remove) && count($fields_to_remove) > 0) {
            foreach ($fields_to_remove as $id_to_remove) {
                $field_object = Field::find($id_to_remove);
                if (!$field_object) {
                    continue;
                }
                $field_object->delete();
            }
        }


        foreach ($field_groups as $f) {
            $new_field = new Field;

            if (array_key_exists('field_id', $f)) {
                $existing_field = Field::find($f['field_id']);
                if ($existing_field) {
                    if (!strlen($existing_field->name)) {
                        $existing_field->delete();
                        continue;
                    } else {
                        $new_field = $existing_field;
                    }
                }

            }
            if (!strlen($f['name'])) {
                continue;
            }
            $new_field->page_id = $request->page_id;
            $new_field->name = $f['name'];
            $new_field->type = $f['type'];
            $new_field->attribs = (array_key_exists('attribs', $f)) ? $f['attribs'] : null;
            $new_field->order = (array_key_exists('order', $f)) ? $f['order'] : 1;
            $new_field->description = (array_key_exists('description', $f)) ? $f['description'] : '';
            $new_field->fieldgroup_id = $field_group->id;
            $new_field->save();
        }

        // generate flash messages
        if ($field_group->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.edit.error.message'));
        }

        return Redirect::to('/admin/fieldgroups');
    }


    /**
     * Delete field group
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $field_group = FieldGroup::findOrFail($id);
        $fields = $field_group->fields;

        foreach ($fields as $field) {
            $field->delete();
        }

        if ($field_group->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.delete.success.title'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.fieldgroups.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.fieldgroups.flash.delete.error.message'));
        }

        return Redirect::back();
    }


    public function getFieldTypes()
    {
        return [
            'text',
            'textarea',
            'select',
            'checkbox'
        ];
    }
}
