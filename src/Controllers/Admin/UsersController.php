<?php
namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use CrowAndRaven\CMS\Models\Role;
use CrowAndRaven\CMS\Models\RoleUser;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display users admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['users'] = User::where('name', 'like', '%'.$request->search.'%')->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.users.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['users'] = User::orderBy('updated_at', 'desc')->paginate(20);
        }

        $this->view_data['request'] = $request;

        return view('raven::admin.users.index', $this->view_data);
    }

    /**
     * Display add user form
     * @param  Request $request
     */
    public function create(Request $request)
    {
        $this->view_data['all_roles'] = Role::getAllRoles()->prepend(__('raven::messages.roles.create.form.select_one'), 0);
        $this->view_data['request'] = $request;

        return view('raven::admin.users.create', $this->view_data);
    }

    /**
     * Create a new user
     * @param  Request $request
     */
    public function store(Request $request)
    {
        // check to see if email already used
        $email_check = User::where('email', $request->email)->get();
        if (is_array($email_check) && count($email_check) == 1) {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.users.flash.create.email.title'));
            $request->session()->flash('flash_message', __('raven::messages.users.flash.create.email.message'));

            return Redirect::back();
        }

        // truncate bio
        $bio = strip_tags(substr($request->bio, 0, 250));

        // save new user
        $user_new = new User;
        $user_new->name = $request->name;
        $user_new->email = $request->email;
        $user_new->password = bcrypt($request->password);
        $user_new->bio = $bio;
        $user_new->is_admin = $request->is_admin;
        $user_new->save();

        if ($user_new) {
            $roles = [];
            if ($request['roles']) {
                $roles = array_filter($request['roles']);
            }
            if (is_array($roles) && count($roles) > 0) {
                foreach ($roles as $role) {
                    RoleUser::attach($role, $user_new->id);
                }
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.users.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.users.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.users.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.users.flash.create.error.message'));
        }

        return Redirect::to('/admin/users');
    }

    /**
     * Display edit user
     * @param  Request $request
     */
    public function edit(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->view_data['user'] = $user;
        $this->view_data['all_roles'] = Role::getAllRoles()->prepend(__('raven::messages.roles.create.form.select_one'), 0);
        $this->view_data['request'] = $request;

        return view('raven::admin.users.edit', $this->view_data);
    }

    /**
     * Update user
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        $user_updated = User::findOrFail($id);

        if ($user_updated->email != $request->email) {
            // check to see if email already used
            $email_check = User::where('email', $request->email)->get();
            if (is_array($email_check) && count($email_check) == 1) {
                $request->session()->flash('flash_type', 'warning');
                $request->session()->flash('flash_title', __('raven::messages.users.flash.edit.email.title'));
                $request->session()->flash('flash_message', __('raven::messages.users.flash.edit.email.message'));

                return Redirect::back();
            }
        }

        // truncate bio
        $bio = strip_tags(substr($request->bio, 0, 250));
        
        $user_updated->name = $request->name;
        $user_updated->email = $request->email;
        $user_updated->is_admin = $request->is_admin;
        $user_updated->bio = $bio;
        $user_updated->save();

        if ($user_updated) {
            // update roles
            RoleUser::detach($user_updated->id);
            $roles = [];
            if ($request['roles']) {
                $roles = array_filter($request['roles']);
            }
            if (is_array($roles) && count($roles) > 0) {
                foreach ($roles as $role) {
                    RoleUser::attach($role, $user_updated->id);
                }
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.users.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.users.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.users.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.users.flash.edit.error.message'));
        }

        return Redirect::to('/admin/users');
    }

    /**
     * Delete user
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $user = User::findOrFail($id);

        if ($user->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.users.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.users.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.users.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.users.flash.delete.error.message'));
        }

        return Redirect::back();
    }
}
