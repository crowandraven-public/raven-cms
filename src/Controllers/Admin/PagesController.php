<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use CrowAndRaven\CMS\Models\Field;
use CrowAndRaven\CMS\Models\Media;
use CrowAndRaven\CMS\Models\Meta;
use CrowAndRaven\CMS\Models\Page;
use CrowAndRaven\CMS\Models\User;
use CrowAndRaven\CMS\Models\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
     * Display pages admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $this->view_data['user'] = Auth::user();

        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['pages'] = Page::whereRaw("LOWER(title) LIKE '%".strtolower($request->search)."%'")
                ->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.pages.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['pages'] = Page::orderBy('updated_at', 'desc')->paginate(20);
        }
        
        $this->view_data['request'] = $request;
        return view('raven::admin.pages.index', $this->view_data);
    }

    /**
     * Display add page
     * @param  Request $request
     */
    public function create(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['templates'] = Page::getTemplates();

        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.pages.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
        }

        
        $this->view_data['request'] = $request;

        return view('raven::admin.pages.create', $this->view_data);
    }

    /**
     * Create a new page
     * @param  Request $request
     */
    public function store(Request $request)
    {
        $page_saved = Page::create($request->all());

        if ($page_saved) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.pages.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.pages.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.pages.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.pages.flash.create.error.message'));
        }

        return Redirect::to('/admin/pages');
    }

    /**
     * Display edit page
     * @param  Request $request
     */
    public function edit(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        //dd($page->fieldGroup->fields);
        $this->view_data['page'] = Page::findOrFail($id);
        $this->view_data['templates'] = Page::getTemplates();
        
        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.pages.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
        }
        
        $this->view_data['request'] = $request;

        return view('raven::admin.pages.edit', $this->view_data);
    }

    /**
     * Update page
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        $meta = $request;
        $page_updated = Page::updatePage($request->all(), $id);
        $meta_updated = Page::updatePageMeta($meta, $page_updated);

        if ($page_updated) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.pages.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.pages.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.pages.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.pages.flash.edit.error.message'));
        }

        return Redirect::to('/admin/pages');
    }

    /**
     * Delete page
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $page = Page::findOrFail($id);

        if ($page->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.pages.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.pages.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.pages.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.pages.flash.delete.error.message'));
        }

        return Redirect::back();
    }
}
