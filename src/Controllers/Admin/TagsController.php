<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use CrowAndRaven\CMS\Models\Tag;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class TagsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
    * Display tag admin page
    * @param  Request $request
    */
    public function index(Request $request)
    {
        $locale = $locale ?? app()->getLocale();

        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['tags'] = Tag::where("name->{$locale}", 'like', '%'.$request->search.'%')->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.tags.index.search.title', ['search' => $request->search]);
        } elseif (isset($request->show) && (trim($request->show) != '')) {
            $this->view_data['tags'] = Tag::where('type', $request->show)->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = $request->show.' Tags';
        } else {
            $this->view_data['tags'] = Tag::orderBy('updated_at', 'desc')->paginate(20);
        }
        $this->view_data['request'] = $request;

        return view('raven::admin.tags.index', $this->view_data);
    }

    /**
    * Display tag create form/page
    * @param  Request $request
    */
    public function create(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['request'] = $request;
        
        return view('raven::admin.tags.create', $this->view_data);
    }

    /**
    * Store tag in databasse
    * @param  Request $request
    */
    public function store(Request $request)
    {
        $tag_saved = Tag::create($request->all());

        if ($tag_saved) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.tags.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.tags.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.tags.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.tags.flash.create.error.message'));
        }

        // currently just need to fluch all cache to let application rebuild
        // down the road we'll want to apply cache tags
        \Cache::flush();

        return Redirect::to('admin/tags');
    }

    /**
    * Display tag edit page
    * @param  Request $request
    */
    public function edit(Request $request, $id)
    {
        $this->view_data['tag'] = Tag::findOrFail($id);
        $this->view_data['request'] = $request;

        return view('raven::admin.tags.edit', $this->view_data);
    }

    /**
    * Update tag
    * @param  Request $request
    * @param  integer  $id
    */
    public function update(Request $request, $id)
    {
        $tag_updated = Tag::updateTag($request->all(), $id);

        // generate flash messages
        if ($tag_updated) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.tags.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.tags.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.tags.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.tags.flash.edit.error.message'));
        }

        // currently just need to fluch all cache to let application rebuild
        // down the road we'll want to apply cache tags
        \Cache::flush();

        return Redirect::to('/admin/tags');
    }

    /**
     * Delete tag
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $tag = Tag::findOrFail($id);

        if ($tag->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.tags.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.tags.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.tags.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.tags.flash.delete.error.message'));
        }

        // currently just need to fluch all cache to let application rebuild
        // down the road we'll want to apply cache tags
        \Cache::flush();

        return Redirect::back();
    }
}
