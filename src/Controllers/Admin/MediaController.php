<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

ini_set('upload_max_filesize', '20M');
ini_set('post_max_size', '20M');

use App\Http\Controllers\Controller;
use Auth;
use CrowAndRaven\CMS\Models\Media;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class MediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
     * Display media admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['media'] = Media::orderBy('updated_at', 'desc')->paginate(15);
        $this->view_data['request'] = $request;

        return view('raven::admin.media.index', $this->view_data);
    }

    /**
     * Get media as JSON
     * @param  Request $request
     */
    public function getAllWithJSON(Request $request)
    {
        if (!$request->input('params')) {
            return;
        }
        if (!array_key_exists('q', $request->input('params'))) {
            return;
        }
        $all_media = Media::where('title', 'like', $request->input('params')['q'].'%')->get();
        return response()->json($all_media);
    }

    /**
     * Upload new media
     * @param  Request $request
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        
        if (!$request->file('file')) {
            return Redirect::back();
        }
        $upload = $request->file;
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

        //$path = $request->file('file')->store('public/uploads');
        $newfilename = substr(md5(rand()), 0, 9);
        $path = $request->file('file')->storeAs('public/uploads', $newfilename.'.'.$ext);

        // save new media
        $media = new Media;
        $media->title = $request->title;
        $media->type = $upload->getClientMimeType();
        $media->file = $newfilename.'.'.$ext;
        $media->created_by = $user->id;

        // generate flash messages
        if ($media->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.media.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.media.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.media.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.media.flash.create.error.message'));
        }

        return Redirect::back();
    }


    /**
     * Update a media item's field
     * @param Request $request
     * @param $id
     */
    public function updateImageField(Request $request, $id) {
        $incomplete_request = false;
        if (!$id) {
            $incomplete_request = true;
        }
        if (!$request->input('field')) {
            $incomplete_request = true;
        }
        if (!$request->input('value')) {
            $incomplete_request = true;
        }
        $media = Media::find($id);
        if (!$media) {
            $incomplete_request = true;
        } 
        if ($incomplete_request == false && array_key_exists($request->input('field'), $media->getAttributes())) {
            $media->{$request->input('field')} = $request->input('value');
            if ($media->save()) {
                return response()->json([ 'success' => true ]);
            }
        }
        if ($request->input('using_ajax') === true && $incomplete_request === true) {
            return response()->json([ 'success' => false ]);
        }
        if ($incomplete_request === true) {
            return Redirect::back();
        }
    }

    /**
     * Upload new media via ajax
     * @param Request $request
     */
    public function storeViaAjax(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image64:jpeg,jpg,png,gif'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        } else {
            $imageData = $request->get('image');
            $image = Image::make($request->get('image'));
            $encoded = $image->encode();
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Storage::put('public/uploads/'.$fileName, $encoded);

            $user = Auth::user();

            // save new media
            $media = new Media;
            $media->title = (!$request->title) ? $fileName : $request->title;
            $media->type = $image->mime();
            $media->file = $fileName;
            $media->created_by = $user->id;

            if ($media->save()) {
                return response()->json([ 
                    'success' => true, 
                    'media_id' => $media->id,
                    'file' => $media->file
                ]);
            }
            return response()->json([ 'success' => false ]);
        }
    }

    /**
     * Update media
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        $media = Media::findOrFail($id);

        $media->title = $request->title;

        // generate flash messages
        if ($media->save()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.media.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.media.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.media.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.media.flash.edit.error.message'));
        }

        return Redirect::back();
    }

    /**
     * Delete media
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $media = Media::findOrFail($id);

        if ($media->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.media.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.media.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.media.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.media.flash.delete.error.message'));
        }

        // actually delete the file
        Storage::delete('public/uploads/'.$media->file);

        return Redirect::back();
    }
}
