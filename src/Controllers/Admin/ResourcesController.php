<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use CrowAndRaven\CMS\Models\Resource;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class ResourcesController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
     * Display resources admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $this->view_data['user'] = Auth::user();

        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['resources'] = Resource::whereRaw("LOWER(title) LIKE '%".strtolower($request->search)."%'")
                ->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.resources.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['resources'] = Resource::orderBy('updated_at', 'desc')->paginate(20);
        }

        $this->view_data['request'] = $request;

        return view('raven::admin.resources.index', $this->view_data);
    }

    /**
     * Display add resource form
     * @param  Request $request
     */
    public function create(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['request'] = $request;

        return view('raven::admin.resources.create', $this->view_data);
    }

    /**
     * Create a new resource
     * @param  Request $request
     */
    public function store(Request $request)
    {
        $resource_saved = Resource::create($request->all());

        if ($resource_saved) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.resources.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.resources.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.resources.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.resources.flash.create.error.message'));
        }

        return Redirect::to('/admin/resources');
    }

    /**
     * Display edit resource
     * @param  Request $request
     */
    public function edit(Request $request, $id)
    {
        $this->view_data['user'] = Auth::user();
        $resource = Resource::findOrFail($id);

        if ($resource->type == 'external') {
            $resource->url_link = $resource->url;
            $resource->url_file = null;
        } else {
            $resource->url_file = $resource->url;
            $resource->url_link = null;
        }

        $this->view_data['resource'] = $resource;
        $this->view_data['request'] = $request;

        return view('raven::admin.resources.edit', $this->view_data);
    }

    /**
     * Update resources
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        $resource_updated = Resource::updateResource($request->all(), $id);

        if ($resource_updated) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.resources.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.resources.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.resources.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.resources.flash.edit.error.message'));
        }

        return Redirect::to('/admin/resources');
    }

    /**
     * Delete resource
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $resource = Resource::findOrFail($id);

        if ($resource->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.resources.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.resources.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.resources.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.resources.flash.delete.error.message'));
        }

        return Redirect::back();
    }
}
