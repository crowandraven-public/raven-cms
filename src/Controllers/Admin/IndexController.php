<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use CrowAndRaven\CMS\Models\CMS;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
    * Display tag admin page
    * @param  Request $request
    */
    public function index(Request $request)
    {
        $this->view_data['results'] = '';
        $this->view_data['request'] = $request;
        $this->view_data['version'] = CMS::version();

        return view('raven::admin.index', $this->view_data);
    }
}
