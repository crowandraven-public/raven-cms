<?php
namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use Auth;
use CrowAndRaven\CMS\Models\Post;
use CrowAndRaven\CMS\Models\User;
use CrowAndRaven\CMS\Models\Media;
use CrowAndRaven\CMS\Models\Resource;
use CrowAndRaven\CMS\Models\Tag;
use CrowAndRaven\CMS\Models\Attachment;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
     * Display posts admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['posts'] = Post::whereRaw("LOWER(title) LIKE '%".strtolower($request->search)."%'")
                ->orderBy('updated_at', 'desc')->paginate(20);

            $this->view_data['title'] = __('raven::messages.posts.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['posts'] = Post::orderBy('updated_at', 'desc')->paginate(20);
        }

        $this->view_data['request'] = $request;

        return view('raven::admin.posts.index', $this->view_data);
    }


    /**
     * Display add post
     * @param  Request $request
     */
    public function create(Request $request)
    {

        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.pages.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
            $this->view_data['all_files'] = Attachment::getAllAttachmentsByType('document')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
            $this->view_data['all_files'] = Media::getAllFiles()->prepend('Select One', 0);
        }

        $this->view_data['request'] = $request;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_types'] = Tag::tagPairsByType('type');
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        $this->view_data['all_authors'] = User::getUsersByRole('author')->prepend('Select One', 0);
        $this->view_data['all_resources'] = Resource::getAllResources()->prepend('Select One', 0);
        
        return view('raven::admin.posts.create', $this->view_data);
    }


    /**
     * Create a new post
     * @param  Request $request
     */
    public function store(Request $request)
    {
        $post_saved = Post::create($request->all());

        if ($post_saved) {
            // add tags
            $tags = [];
            // clean and add tags
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $post_saved->tags()->attach($tags);

            // add resources
            if ($request['resources']) {
                $resources = array_filter($request['resources']);
                $post_saved->resources()->attach($resources);
            }
        
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.posts.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.posts.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.posts.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.posts.flash.create.error.message'));
        }

        return Redirect::to('/admin/posts');
    }


    /**
     * Display edit post
     * @param  Request $request
     */
    public function edit(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.pages.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
            $this->view_data['all_files'] = Attachment::getAllAttachmentsByType('document')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
            $this->view_data['all_files'] = Media::getAllFiles()->prepend('Select One', 0);
        }

        $this->view_data['post'] = $post;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_types'] = Tag::tagPairsByType('type');
        $this->view_data['post_tags'] = $post->tags->pluck('id');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['all_authors'] = User::getUsersByRole('author')->prepend('Select One', 0);
        $this->view_data['resources'] = $post->resources->pluck('id');
        $this->view_data['all_resources'] = Resource::getAllResources()->prepend('Select One', 0);
        $this->view_data['request'] = $request;

        return view('raven::admin.posts.edit', $this->view_data);
    }


    /**
     * Update post
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        $post_updated = Post::updatePost($request->all(), $id);

        if ($post_updated) {
            // sync tags
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $post_updated->tags()->sync($tags);

            // sync resources
            $resources = [];
            if ($request['resources']) {
                $resources = array_filter($request['resources']);
                $post_updated->resources()->sync($resources);
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.posts.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.posts.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.posts.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.posts.flash.edit.error.message'));
        }

        return Redirect::to('/admin/posts');
    }


    /**
     * Delete post
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $post = Post::findOrFail($id);

        if ($post->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.posts.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.posts.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.posts.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.posts.flash.delete.error.message'));
        }

        return Redirect::to('/admin/posts');
    }
}
