<?php

namespace CrowAndRaven\CMS\Controllers\Admin;


use App\Http\Controllers\Controller;
use Auth;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RavenFieldsController extends Controller
{
    
    /**
     * Get data for multiple fields
     * @param  Request $request
     * @param $model
     * @param $id
     */
    public function getMultipleFieldsData(Request $request, $model, $id) 
    {
       return $this->getData($request, $model, $id, true);
    }

    /**
     * Get data for one field
     * @param  Request $request
     * @param $model
     * @param $id
     */
    public function getSingleFieldData(Request $request, $model, $id) 
    {
        return $this->getData($request, $model, $id, false);
    }

    /**
     * Get data for field
     * @param  Request $request
     * @param $model
     * @param $id
     * @param $is_multiple
     */
    public function getData(Request $request, $model, $id, $is_multiple=false)
    {

        if (!$this->checkRequest($request, $model, $id, $is_multiple)) {
            return $this->getError();
        }

        $model = '\\CrowAndRaven\CMS\\Models\\'.ucfirst(Str::camel($model));
        $item = $model::where('id', $id)->first();
       
        if (!is_var_iterable($item)) {
            return $this->getError();
        }

        $field_data = $this->getFieldData($request, $item, $is_multiple);
        
        return response()->json([
            'success' => true,
            'data' => $field_data
        ]);
    }

    public function getFieldData($request, $item, $is_multiple=false)
    {
         // is multiple
         if ($is_multiple) {
            $fields = explode(',', $request->input('fields'));
            $field_data = [];
            foreach($fields as $column_name) {
                $field_data[$column_name] = $this->checkAndGetMetaIfExists($item, $column_name);
            }
        } else {
            $column_name = $request->input('field');
            $field_data = $this->checkAndGetMetaIfExists($item, $column_name);
        }

        return $field_data;
    }

    public function getFieldDataNonMeta($request, $item, $is_multiple=false)
    {
        $field_data = [];
         // is multiple
         if ($is_multiple) {
            $fields = explode(',', $request->input('fields'));
            foreach($fields as $column_name) {
                $field_data[$column_name] = localize($item->{$column_name});
            }
            $field_data['id'] = $item->id;
        } else {
            $column_name = $request->input('field');
            $field_data[] = localize($item->{$column_name});
        }

        return $field_data;
    }

    public static function getSoundsLikeTextQuery($locale, $textQuery) 
    {
        $soundsLike = sprintf("LOWER(title->'$.%s') SOUNDS LIKE '%s'  ", $locale, strtolower($textQuery));
        $fallbackLike = sprintf("LOWER(title->'$.%s') LIKE '%%%s%%'  ", $locale, strtolower($textQuery));
        return $soundsLike.' OR '.$fallbackLike;
    }

    public function getCollection(Request $request, $model)
    {
        if (!(isset($model) && strlen($model))) {
            return false;
        }

        $model = "\\CrowAndRaven\\CMS\\Models\\".ucfirst(Str::camel($model));
       
        $locale = $locale ?? app()->getLocale();
        
        if ($request->get_all && $request->get_all == true) {
            $results = $model::all();
        } else {
            if (is_array($locale)) {
                $queryWhere = [];
                foreach($locale as $l) {
                    $queryWhere[] = self::getSoundsLikeTextQuery($l, $request->text_query);
                }
                $results = $model::whereRaw(join(' OR ',$queryWhere))
                    ->orderBy('updated_at', 'desc')->get();
            } else {
                $results = $model::whereRaw(self::getSoundsLikeTextQuery($locale, $request->text_query))
                ->orderBy('updated_at', 'desc')->get();
            }
        }

        $data = [];
        foreach($results as $result) {
            $data[] = array_map(
                function($r) { 
                    $d = json_decode($r);
                    if (is_object($d)) {
                        if (!is_var_iterable($d)) {
                            return '';
                        }
                        return localize($r);
                    }
                    return $r;
                }, 
                $result->toArray());
        }
        
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function updateModelFields(Request $request, $model, $id) 
    {
        $model = '\\CrowAndRaven\CMS\\Models\\'.ucfirst(Str::camel($model));
        $item = $model::where('id', $id)->first();
        if (!$item) 
        {
            return $this->getError();
        }
        if (!isset($request->fieldValues)) {
            return $this->getError();
        }

        if (method_exists($item, 'getTranslatableFields')) {
            $translatableFields = $item::getTranslatableFields();

            foreach ($translatableFields as $field) {
                $fieldset = [];
                foreach ($request->fieldValues as $key => $value) {
                    
                    if (strpos($key, $field.'-') === 0) {
                        $loc = substr($key, strpos($key, "-") + 1);
                        $fieldset[$loc] = $value;
                    }
                }
                $item->{$field} = json_encode($fieldset);
            }
        } else {
            foreach($request->fieldValues as $key => $value) {
                $item->{$key} = $value;
            }
        }
        
        if ($item->save()) {
            return response()->json([
                'success' => true
            ]);
        }

    }

    /**
     * If the value is empty check for a meta field relationship
     * @param $value
     * @param $model_object
     */
    public function checkAndGetMetaIfExists($model_object, $column_name)
    {   
        // check meta fields if that relationship exists
        if (!$model_object->{$column_name} && $model_object->fields->count()) {
            foreach($model_object->fields as $field) {
                if ($field->name != $column_name) { 
                    continue;
                }
                if ($field->type == 'json_editor' && isset($field->meta) && is_string($field->meta->value)) {
                    return json_decode($field->meta->value);
                }
            }
        }
        return $model_object->{$column_name};
    }

    /**
     * Get data for field
     * @param  Request $request
     * @param $model
     * @param $id
     * @param $is_multiple
     */
    public function checkRequest(Request $request, $model, $id, $is_multiple=false) {
        if (!(isset($model) && strlen($model))) {
            return false;
        }
        if (!($request->input('field') || $request->input('fields'))) {
            return false;
        }
        if ($is_multiple === false && $request->input('fields')) {
            return false;
        }
        if ($is_multiple === true && !$request->input('fields')) {
            return false;
        }
        if (!$id) {
            return false;
        }
        return true;
    }

    /**
     * Get errors
     */
    public function getError() 
    {
        return $this->getJsonFail();
    }

    /**
     * Get a json object indicating failure
     */
    public function getJsonFail()
    {
        return response()->json([
            'success' => false
        ]);
    }
}
