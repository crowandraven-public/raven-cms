<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;

class CleanController extends Controller
{
    /**
    * Check if user is logged in and admin to allow to view these pages
    */
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function nukeConfig()
    {
        // delete the config file
        if (File::exists(config_path('raven-cms.php'))) {
            self::deleteFiles(config_path('raven-cms.php'));
        }
    }

    public function nukeAssets()
    {
        // delete all assets related to the CMS
        if (File::isDirectory(public_path('raven'))) {
            self::deleteFiles(public_path('raven'));
        }
    }

    public function nukeLang()
    {
        // delete all assets related to the CMS
        if (File::isDirectory(public_path('lang/raven'))) {
            self::deleteFiles(resource_path('lang/raven'));
        }
    }

    public function nukeViews()
    {
        // delete all views related to the CMS
        if (File::isDirectory(public_path('vendor/raven'))) {
            self::deleteFiles(resource_path('vendor/raven'));
        }
    }

    public function nukeUploads()
    {
        // delete all uploads related to the CMS
        if (File::isDirectory(public_path('public/uploads'))) {
            self::deleteFiles(storage_path('public/uploads'));
        }
    }

    public function nukeDatabaseTables()
    {
        // drop the tables (and fields) associated with the CMS
        if (Schema::hasTable('field_groups')) {
            Schema::drop('field_groups');
        }
        if (Schema::hasTable('fields')) {
            Schema::drop('fields');
        }
        if (Schema::hasTable('media')) {
            Schema::drop('media');
        }
        if (Schema::hasTable('meta')) {
            Schema::drop('meta');
        }
        if (Schema::hasTable('pages')) {
            Schema::drop('pages');
        }
        if (Schema::hasTable('posts')) {
            Schema::drop('posts');
        }
        if (Schema::hasTable('resourcables')) {
            Schema::drop('resourcables');
        }
        if (Schema::hasTable('resources')) {
            Schema::drop('resources');
        }
        if (Schema::hasTable('taggables')) {
            Schema::drop('taggables');
        }
        if (Schema::hasTable('tags')) {
            Schema::drop('tags');
        }
        if (Schema::hasColumn('users', 'is_admin')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropColumn('is_admin');
            });
        }
    }

    public function nukeMigrations()
    {
        // delete all migrations related to the CMS
        self::deleteFiles(database_path('file'));
        self::deleteFiles(database_path('file'));
        self::deleteFiles(database_path('file'));
        self::deleteFiles(database_path('file'));
        self::deleteFiles(database_path('file'));
        self::deleteFiles(database_path('file'));
        self::deleteFiles(database_path('file'));
    }

    public function dumpAutoload()
    {
        shell_exec('composer dump-autoload');
    }

    public function clearCache()
    {
        shell_exec('php artisan cache:clear');
    }

    public function clearViews()
    {
        shell_exec('php artisan view:clear');
    }

    public function deleteFiles($target)
    {
        if (is_dir($target)) {
            $files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned
            
            foreach ($files as $file) {
                $this->deleteFiles($file);
            }
          
            rmdir($target);
        } elseif (is_file($target)) {
            unlink($target);
        }
    }
}
