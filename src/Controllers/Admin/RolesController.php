<?php
namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use CrowAndRaven\CMS\Models\Role;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
    * Display role admin page
    * @param  Request $request
    */
    public function index(Request $request)
    {
        $this->view_data['roles'] = Role::orderBy('updated_at', 'desc')->paginate(20);
        $this->view_data['request'] = $request;

        return view('raven::admin.roles.index', $this->view_data);
    }

    /**
    * Display role create form/page
    * @param  Request $request
    */
    public function create(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['request'] = $request;
        
        return view('raven::admin.roles.create', $this->view_data);
    }

    /**
    * Store role in databasse
    * @param  Request $request
    */
    public function store(Request $request)
    {
        $role_saved = Role::create($request->all());

        if ($role_saved) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.roles.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.roles.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.roles.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.roles.flash.create.error.message'));
        }

        return Redirect::to('admin/roles');
    }

    /**
    * Display role edit page
    * @param  Request $request
    */
    public function edit(Request $request, $id)
    {
        $this->view_data['role'] = Role::findOrFail($id);
        $this->view_data['request'] = $request;

        return view('raven::admin.roles.edit', $this->view_data);
    }

    /**
    * Update role
    * @param  Request $request
    * @param  integer  $id
    */
    public function update(Request $request, $id)
    {
        $role_updated = Role::updateRole($request->all(), $id);

        if ($role_updated) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.roles.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.roles.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.roles.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.roles.flash.edit.error.message'));
        }

        return Redirect::to('/admin/roles');
    }

    /**
     * Delete role
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $role = Role::findOrFail($id);

        if ($role->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.roles.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.roles.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.roles.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.roles.flash.delete.error.message'));
        }

        return Redirect::back();
    }
}
