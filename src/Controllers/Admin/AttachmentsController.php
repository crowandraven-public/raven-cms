<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use CrowAndRaven\CMS\Models\Attachment;
use CrowAndRaven\CMS\Models\Attachable;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class AttachmentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }


    /**
     * Display attachments admin page
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $this->view_data['user'] = Auth::user();

        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['attachments'] = Attachment::whereRaw("LOWER(title) LIKE '%".strtolower($request->search)."%'")
                ->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.attachments.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['attachments'] = Attachment::orderBy('updated_at', 'desc')->paginate(20);
        }

        $this->view_data['request'] = $request;

        return view('raven::admin.attachments.index', $this->view_data);
    }

    /**
     * Display add attachment form
     * @param  Request $request
     */
    public function create(Request $request)
    {
        $this->view_data['user'] = Auth::user();
        $this->view_data['request'] = $request;

        return view('raven::admin.attachments.create', $this->view_data);
    }

    /**
     * Create a new attachment
     * @param  Request $request
     */
    public function store(Request $request)
    {   
        // $validator = Validator::make($request->all(), [
            // 'file' => 'max:10000000',
        // ]);

        // $validator->validate();

        $attachment_saved = Attachment::create($request);

        if ($attachment_saved) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.attachments.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.attachments.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.attachments.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.attachments.flash.create.error.message'));
        }

        return redirect('/admin/attachments/'.$attachment_saved->id.'/edit');
    }

    public function syncAttachablesViaAjax(Request $request) 
    {
        if (isset($request->attachable_attachments) && isset($request->attachable_model) && isset($request->attachable_id)) {
            
            $modelObject = $request->attachable_model::find($request->attachable_id);

            if (is_object($modelObject) && Attachment::syncAttachments($request->attachable_attachments, $modelObject)) {
                
                $attachments = $modelObject->getPreppedAttachments();
                return response()->json([ 
                    'success' => true,
                    'attachments' => $attachments
                ]);
            }
        }
        return response()->json([ 'success' => false ]);
    }

    /**
     * Upload new media via ajax
     * @param Request $request
     */
    public function storeViaAjax(Request $request)
    {
        $attachment = Attachment::create($request, true);
        
        $translatableFields = Attachment::getTranslatableFields();
        
        if ($attachment) { 
            $attachmentFormatted = null;
            foreach($attachment->toArray() as $key => $value) {
                if (in_array($key, $translatableFields)) {
                    $attachmentFormatted[$key] = localize($value);
                    continue;
                }
                $attachmentFormatted[$key] = $value;
            }
            $attachmentFormatted['order'] = 0;

            return response()->json([ 
                'success' => true,
                'attachment' => $attachmentFormatted
            ]);
        }
        return response()->json([ 'success' => false ]);
    }

    /**
     * Display edit attachment
     * @param  Request $request
     */
    public function edit(Request $request, $id)
    {
        $this->view_data['user'] = Auth::user();
        $attachment = Attachment::findOrFail($id);
        $this->view_data['attachment'] = $attachment;
        $this->view_data['request'] = $request;

        return view('raven::admin.attachments.edit', $this->view_data);
    }

    /**
     * Update attachments
     * @param  Request $request
     * @param  integer  $id
     */
    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
            // 'file' => 'max:10000'
        // ]);

        // $validator->validate();

        $attachment_updated = Attachment::updateAttachment($request, $id);

        if ($attachment_updated) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.attachments.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.attachments.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.attachments.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.attachments.flash.edit.error.message'));
        }

        return back();
    }

    /**
     * Delete attachment
     * @param  Request $request
     * @param  integer  $id
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $attachment = Attachment::findOrFail($id);

        if ($attachment->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.attachments.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.attachments.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.attachments.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.attachments.flash.delete.error.message'));
        }

        return Redirect::back();
    }
}
