<?php

namespace CrowAndRaven\CMS\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Auth;
use Config;

use CrowAndRaven\CMS\Models\Post;
use CrowAndRaven\CMS\Models\Page;
use CrowAndRaven\CMS\Models\Tag;
use CrowAndRaven\CMS\Models\User;

class PostsController extends Controller
{
    /**
     * Show the application posts index.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $page = Page::getPageBySlug('blog');
        $this->view_data['page'] = $page;
        $this->view_data['posts'] = Post::getPostsByType('blog')->paginate(10);
        $this->view_data['request'] = $request;

        return view('raven::pages.templates.'.$page->template, $this->view_data);
    }

    /**
     * Show a specific post
     * @param  Request $request
     * @param  string  $parent  parent slug
     * @param  string  $slug    page slug
     * @return
     */
    public function show(Request $request, $parent, $slug = null)
    {
        $slug = (!$slug) ? $parent : $slug;

        $post = Post::getPostBySlug($slug);
        $this->view_data['post'] = $post;
        $this->view_data['author'] = User::where('id', $post->author)->first();
        $this->view_data['request'] = $request;

        return view('raven::posts.show', $this->view_data);
    }

    /**
     * Ask the Coach form
     * @param  Request $request form data to send email
     */
    public function contact(Request $request)
    {
        $user = Auth::user();
        
        if (!$user) {
            $data['from_email'] = $request->email;
            $data['from_name'] = $request->name;
        } else {
            $data['from_email'] = $user->email;
            $data['from_name'] = $user->name;
        }

        $data['email_subject'] = 'EVENPULSE Post Question';
        $data['content'] = $request->content;
        $data['post'] = $request->post_title;
        $data['to_email'] = $request->author_email;
        $data['to_name'] = $request->author_name;

        Mail::send('raven::posts.emails.contact', ['data' => $data], function ($message) use ($data) {
            //$message->from($data['from_email'], $data['from_name']);
            $message->from(Config::get('site.contact.email'), Config::get('site.contact.name'));
            $message->to($data['to_email'], $data['to_name']);
            $message->replyTo($data['from_email'], $data['from_name']);
            $message->subject($data['email_subject']);
        });

        $request->session()->flash('flash_type', 'info');
        $request->session()->flash('flash_title', 'Message Sent');
        $request->session()->flash('flash_message', 'Your message has been successfully sent.');

        return Redirect::back();
    }
}
