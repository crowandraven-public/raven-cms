<?php
Route::group(['prefix' => 'admin', 'namespace' => 'CrowAndRaven\CMS\Controllers\Admin', 'middleware' => ['web', 'content']], function () {
    Route::get('/', 'IndexController@index');
    Route::resource('fieldgroups', 'FieldGroupsController');
    Route::post('fieldgroups/{id}/copy', 'FieldGroupsController@copy'); // clone fieldgroup
    Route::resource('media', 'MediaController');
    //Route::post('attachments/updatefield/{id}', 'AttachmentsController@updateAttachmentField');
    Route::get('media/json/all', 'MediaController@getAllWithJSON');
    Route::resource('pages', 'PagesController');
    Route::resource('posts', 'PostsController');
    Route::resource('resources', 'ResourcesController');
    Route::resource('attachments', 'AttachmentsController');
    Route::resource('roles', 'RolesController');
    Route::resource('tags', 'TagsController');
    Route::resource('users', 'UsersController');

    Route::get('ravenfields/collection/{model}', 'RavenFieldsController@getCollection');
    Route::get('ravenfields/{model}/{id}', 'RavenFieldsController@getSingleFieldData');
    Route::get('ravenfields/multiple/{model}/{id}', 'RavenFieldsController@getMultipleFieldsData');
    Route::post('ravenfields/{model}/{id}', 'RavenFieldsController@updateModelFields');

    Route::post('ajaxmedia', 'MediaController@storeViaAjax');
    Route::post('ajaxattachments', 'AttachmentsController@storeViaAjax');
    Route::post('syncattachablesajax', 'AttachmentsController@syncAttachablesViaAjax');

});

/* img */
Route::get('/storage/uploads/{path}', 'CrowAndRaven\CMS\Controllers\ImageController@show')->where('path', '.*');
Route::get('/img/{path}', 'CrowAndRaven\CMS\Controllers\ImageController@show')->where('path', '.*');


Route::group(['middleware' => ['web', 'auth']], function () {
    Route::post('post/like/{id}', ['as' => 'post.like', 'uses' => 'CrowAndRaven\CMS\Controllers\LikeController@likePost']);
});
    
