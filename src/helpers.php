<?php

if (! function_exists('localize')) {
    function localize($data, $locale = null)
    {
        $data = $data ?: '';
        $locale = $locale ?? app()->getLocale();
        $data = json_decode($data);

        if (json_last_error() == JSON_ERROR_NONE) {
            return $data->{$locale};
        }

        return;
    }
}

if (! function_exists('is_var_iterable')) {
    function is_var_iterable($element)
    {
        if (is_object($element) && count((array) $element) > 0) {
            return true;
        }

        if (is_array($element) && count($element) > 0) {
            return  true;
        }

        return false;
    }
}