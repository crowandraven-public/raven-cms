<?php

namespace CrowAndRaven\CMS;

use Collective\Html\HtmlServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
                                                                                                            
/************************************ version 0.1 ***************************************
                                                                              
                                                                                                                    
                      ......
                  'coxO0000Odoc'.                .ool.            .;cloo'.
                ;xXKko:,,'.;lxKX0ko:,.           'OXXk       ,ok0Oolcd00xdl;..
              .:dOx       .oKXNKOKXKxl;        .kNNNXO,   'cxKNKd,.   ..dkO0xl;
              .;oo;.''',;ckXNNk' .:oOK0x:'.     lNNNNNd':kXNKxc'         .;ldOOx
       ...,lo;,::;lc:;;lONNN0:.    'cdNNXkdol;' XNNNNOk 0d;..              .cOKKd:
    ok0O00oco,..    .lXNNx.         .,cKNNXXOx WNNNO Xkoodxd;              .ck0KK0
   0Oxl;':dlxl.    .oXXo.         .....kXXX0dOWN KxO c;,;d0k:.             .;xXKkood
  kl:'.  .lxOO     'xXO'        'oxlllldk0XXkd0XXxdO;     .cxxc'.            .cO  ':lo
 Od.      .ckO      'kXx.       :xc..    'dKXOxkkOK        'lOOdl        .lOk  ;,..
xc        .;l:     .,cl'     .::.        .;xKXXXXK          'xK0xxk       'dKO   .'c
,        ..'.       .;o:   .;,.           ,OXXNNd.          .oOc. ..     .d00    .co
.        ,ol        'l:.  .'.             .l0XN0,          .x0:   .l      xd:.   ox.
  .;    d...        .'cdc;oko:.            .:OXKk;          'od'   .O    xdl    Oc
    .,cOo'....     .':oo::ccl,            ,clxxOl'         .,;,.   .,l  ':do'...,. ,Ox
    .:kkc,,.;l      ..,;:c',c:,'.           ;c.:xkc.       .,:;';'''.l  ,,,::;,oOko, .:
   ...lkoox;.coo:l,  ..;ll:cx:....;;        .loxK0xlc   ''. .,l:.'',,;k',,lo:.,::ddcl;..
  'lx00kldl,,:ll:,.,'.odcodc:c;;odc:;,;::,,.'lx0KXX:::'.:llo,.,:,,.':,::;cdxd;l0xdo;','..
;:;:d0XNNXN0KX0xOKk00Okl. .,;cc,';lkklccokd:oxlc;,;,;,:oc,'cdOK0l.,,''.;;,.';:;,.........

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 ____  _   _    _    ____   _____        __  __  __  ___  _   _ ____ _____ _____ ____
/ ___|| | | |  / \  |  _ \ / _ \ \      / / |  \/  |/ _ \| \ | / ___|_   _| ____|  _ \
\___ \| |_| | / _ \ | | | | | | \ \ /\ / /  | |\/| | | | |  \| \___ \ | | |  _| | |_) |
 ___) |  _  |/ ___ \| |_| | |_| |\ V  V /   | |  | | |_| | |\  |___) || | | |___|  _ <
|____/|_| |_/_/   \_\____/ \___/  \_/\_/    |_|  |_|\___/|_| \_|____/ |_| |_____|_| \_\

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

class CMSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        // register middleware
        $router->aliasMiddleware('admin', '\CrowAndRaven\CMS\Middleware\AdminAuthentication');
        $router->aliasMiddleware('content', '\CrowAndRaven\CMS\Middleware\ContentAuthorization');

        // register other providers
        $this->app->register(HtmlServiceProvider::class);
        AliasLoader::getInstance([
            'Form'=>'\Collective\Html\FormFacade'
        ]);

        // register morphMap
        Relation::morphMap([
            'posts' => 'CrowAndRaven\CMS\Models\Post',
            'resources' => 'CrowAndRaven\CMS\Models\Resource'
        ]);

        if ($this->app->runningInConsole()) {
            if (!class_exists('CreatePagesTable')) {
                $timestamp = date('Y_m_d_His', time());
                $this->publishes([__DIR__.'/../database/migrations/create_pages_table.php.stub' => database_path('migrations/'.$timestamp.'_create_pages_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreatePostsTable')) {
                $timestamp = date('Y_m_d_His', time() + 10);
                $this->publishes([__DIR__.'/../database/migrations/create_posts_table.php.stub' => database_path('migrations/'.$timestamp.'_create_posts_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreateResourcesTables')) {
                $timestamp = date('Y_m_d_His', time() + 20);
                $this->publishes([__DIR__.'/../database/migrations/create_resources_tables.php.stub' => database_path('migrations/'.$timestamp.'_create_resources_tables.php')], 'raven-migrations');
            }

            if (!class_exists('CreateMediaTable')) {
                $timestamp = date('Y_m_d_His', time() + 30);
                $this->publishes([__DIR__.'/../database/migrations/create_media_table.php.stub' => database_path('migrations/'.$timestamp.'_create_media_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreateFieldGroupsTable')) {
                $timestamp = date('Y_m_d_His', time() + 40);
                $this->publishes([__DIR__.'/../database/migrations/create_field_groups_table.php.stub' => database_path('migrations/'.$timestamp.'_create_field_groups_table.php'),
                    ], 'raven-migrations');
            }

            if (!class_exists('CreateFieldsTable')) {
                $timestamp = date('Y_m_d_His', time() + 50);
                $this->publishes([__DIR__.'/../database/migrations/create_fields_table.php.stub' => database_path('migrations/'.$timestamp.'_create_fields_table.php')], 'raven-migrations');
            }

            if (!class_exists('AddIsAdminToUsersTable')) {
                $timestamp = date('Y_m_d_His', time() + 60);
                $this->publishes([__DIR__.'/../database/migrations/add_is_admin_to_users_table.php.stub' => database_path('migrations/'.$timestamp.'_add_is_admin_to_users_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreateTagTables')) {
                $timestamp = date('Y_m_d_His', time() + 70);
                $this->publishes([__DIR__.'/../database/migrations/create_tag_tables.php.stub' => database_path('migrations/'.$timestamp.'_create_tag_tables.php')], 'raven-migrations');
            }

            if (!class_exists('CreateMetaTable')) {
                $timestamp = date('Y_m_d_His', time() + 80);
                $this->publishes([__DIR__.'/../database/migrations/create_meta_table.php.stub' => database_path('migrations/'.$timestamp.'_create_meta_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreateRolesTable')) {
                $timestamp = date('Y_m_d_His', time() + 90);
                $this->publishes([__DIR__.'/../database/migrations/create_roles_table.php.stub' => database_path('migrations/'.$timestamp.'_create_roles_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreateRoleUserTable')) {
                $timestamp = date('Y_m_d_His', time() + 100);
                $this->publishes([__DIR__.'/../database/migrations/create_role_user_table.php.stub' => database_path('migrations/'.$timestamp.'_create_role_user_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreateLikeablesTable')) {
                $timestamp = date('Y_m_d_His', time() + 110);
                $this->publishes([__DIR__.'/../database/migrations/create_likeables_table.php.stub' => database_path('migrations/'.$timestamp.'_create_likeables_table.php')], 'raven-migrations');
            }

            if (!class_exists('CreateAttachmentsTable')) {
                $timestamp = date('Y_m_d_His', time() + 120);
                $this->publishes([__DIR__.'/../database/migrations/create_attachments_table.php.stub' => database_path('migrations/'.$timestamp.'_create_attachments_table.php')], 'raven-migrations');
            }

            $this->commands([
                Console\Commands\Install::class,
                Console\Commands\NukeCMS::class,
                Console\Commands\SetAdmin::class,
            ]);

            $this->publishes([__DIR__.'/../database/seeds' => database_path('seeds')], 'raven-seeds');

            $this->publishes([
                __DIR__.'/../config/raven-cms.php' => config_path('raven-cms.php'),
            ], 'raven-config');

            $this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/raven'),
            ], 'raven-lang');

            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/raven'),
            ], 'raven-views');

            $this->publishes([
                __DIR__.'/../dist' => public_path(),
            ], 'raven-assets');
        }

        // for media uploads via ajax
        Validator::extend('image64', function ($attribute, $value, $parameters, $validator) {
            $type = explode('/', explode(':', substr($value, 0, strpos($value, ';')))[1])[1];
            if (in_array($type, $parameters)) {
                return true;
            }
            return false;
        });
    
        Validator::replacer('image64', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':values', join(",", $parameters), $message);
        });

        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'raven');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'raven');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(raven::class, function () {
            return new CMS();
        });

        $this->app->alias(raven::class, 'raven');
    }
}
