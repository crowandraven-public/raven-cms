<?php

namespace CrowAndRaven\CMS\Middleware;

use Closure;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;

class ContentAuthorization
{
    protected $auth;

    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            $check = User::find($request->user()->id)->authorizeRole('content');

            if ($check) {
                return $next($request);
            }
        }
        
        $request->session()->flash('flash_type', 'warning');
        $request->session()->flash('flash_title', __('raven::messages.errors.flash.auth.title'));
        $request->session()->flash('flash_message', __('raven::messages.errors.flash.auth.message'));

        return Redirect::to('/');
    }
}
