<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;

    protected $table = 'likeables';

    protected $fillable = ['likeable_id', 'likeable_type', 'user_id'];

    public function posts()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Post', 'likeable');
    }

    public function lessons()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Lesson', 'likeable');
    }

    public function series()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'likeable');
    }
}
