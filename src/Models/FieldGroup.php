<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class FieldGroup extends Model
{
    protected $fillable = [
        'title',
        'description',
        'created_by',
        'updated_by'
    ];

    public function fields()
    {
        return $this->hasMany('CrowAndRaven\CMS\Models\Field', 'fieldgroup_id');
    }

    public function page()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\Page')->withTimestamps();
    }
}
