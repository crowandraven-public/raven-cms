<?php

namespace CrowAndRaven\CMS\Models;

use Auth;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Page extends Model
{
    use \CrowAndRaven\CMS\Traits\AttachablesTrait;
    
    protected $fillable = [
        'status',
        'template',
        'title',
        'slug',
        'content',
        'image',
        'created_by',
        'updated_by'
    ];


    public function pageImage()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Attachment', 'id', 'image');
    }
    
    public function author()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\User');
    }

    public function editor()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\User');
    }

    public function meta()
    {
        return $this->belongsToMany('CrowAndRaven\CMS\Models\Meta');
    }

    public function fields()
    {
        return $this->hasMany('CrowAndRaven\CMS\Models\Field');
    }

    public function fieldGroup()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\FieldGroup');
    }

    public function resources()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Resource', 'resourcable');
    }

    public static function create($request)
    {
        $translatable = ['title', 'subtitle', 'content']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // create unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'title-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, false);
            }
        }
        $slug = json_encode($fieldset);

        // create excerpt
        foreach ($request as $key => $value) {
            if (strpos($key, 'content-') === 0) {
                $fieldset[$loc] = strip_tags(substr($value, 0, 200));
            }
        }
        $excerpt = json_encode($fieldset);

        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        if ($subtitle == '[]') {
            $subtitle = null;
        }

        // get or set user id
        if (isset($request['updated_by'])) {
            $user_id = $request['updated_by'];
        } else {
            $user_id = Auth::user()->id;
        }

        // save new page
        $page = new Page;
        $page->subtitle = $subtitle;
        $page->excerpt = $excerpt;
        $page->status = $request['status'];
        $page->template = $request['template'];
        $page->title = $title;
        $page->slug = $slug;
        $page->content = $content;
        $page->image = $image;
        $page->created_by = $user_id;
        $page->updated_by = $user_id;
        $page->save();

        return $page;
    }

    public static function updatePage($request, $id)
    {
        $translatable = ['title', 'subtitle', 'excerpt', 'content']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // check for unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'slug-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, true);
            }
        }
        $slug = json_encode($fieldset);

        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        if ($subtitle == '[]') {
            $subtitle = null;
        }
        
        // get or set user id
        if (isset($request['updated_by'])) {
            $user_id = $request['updated_by'];
        } else {
            $user_id = Auth::user()->id;
        }

        $page = Page::findOrFail($id);

        $page->subtitle = $subtitle;
        $page->status = $request['status'];
        $page->template = $request['template'];
        $page->title = $title;
        $page->slug = $slug;
        $page->content = $content;
        $page->excerpt = $excerpt;
        $page->image = $image;
        $page->updated_by = $user_id;
        $page->save();

        return $page;
    }

    public static function updatePageMeta($request, $page)
    {
        
        // work with meta fields
        $field_names = current((array) $page->fields->pluck('name'));

        // iterate over post vars and check and see if it's a post meta field
        foreach ($request->all() as $k => $v) {
            if (in_array($k, $field_names)) {
                // update existing
                $meta_field = Field::where(['name' => $k, 'page_id' => $page->id])->get()->first();
                $m = ($meta_field->meta) ? $meta_field->meta : new Meta;
                $m->value = $v;
                $m->field_id = $meta_field->id;
                $m->save();
            }
        }
        $request_fields_keys = array_keys($request->all());
        
        // update unchecked checkboxes
        if ($page->fieldgroup) {
            $field_type_and_names = $page->fieldgroup->fields->pluck('type', 'name')->toArray();

            foreach ($field_type_and_names as $key => $type) {
                if ($type !== 'checkbox') {
                    continue;
                }
                if (!in_array($key, $request_fields_keys)) {
                    $meta_field = Field::where(['name' => $key, 'page_id' => $page->id])->get()->first();
                    if ($meta_field->meta) {
                        $meta_field->meta->delete();
                    }
                }
            }
        }
    }

    /**
     * Create a unique slug name
     * @param  string  $slug   The slug name to check if unique
     * @param  string  $locale What is the locale passed
     * @param  boolean $update Are we updating a page (true) or is this a new one (false)?
     * @return string          The updated slug
     */
    public static function uniqueSlug($slug, $locale = null, $update = false)
    {
        $locale = $locale ?? app()->getLocale();
        $result = Page::where("slug->{$locale}", $slug)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $slug = $slug.'-'.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $slug = $slug.'-'.rand(1, 100);
            }
        }

        return $slug;
    }

    public static function getPages($orderby = null)
    {
        $locale = $locale ?? app()->getLocale();

        $pages = Page::where('status', 1);

        if ($orderby == 'title') {
            $pages->orderBy("title->{$locale}", 'desc');
        } else {
            $pages->orderBy('updated_at', 'desc');
        }

        return $pages;
    }

    /**
     * Query scope to get all featured posts
     * @param  object $query an existing query
     * @param  integer $take the number of posts to get
     */
    public function scopeFeatured($query, $take = null)
    {
        $query->where('is_featured', 1)->orderBy('created_at', 'desc');

        if (isset($take)) {
            $query->take($take);
        }

        return $query;
    }

    /**
     * Query scope to get the latest post
     * @param  object $query an existing query
     */
    public function scopeLatest($query)
    {
        return $query->orderBy('created_at', 'desc')->first();
    }

    /**
     * Query scope to get the post pairs for forms
     * @param  object $query an existing query
     */
    public function scopePairs($query)
    {
        $locale = $locale ?? app()->getLocale();

        $pages = $query->orderBy("title->{$locale}")->get();

        $pages = $pages->map(function ($l) use ($locale) {
            $title = localize($l->title, $locale);
            $l->title = $title;
            return $l;
        });

        return $pages->pluck('title', 'id');
    }

    public static function getPageById($id)
    {
        $page = Page::where('id', $id)->firstOrFail();

        foreach ($page->fields as $field) {
            $page[''.$field->name.''] = $field->meta['value'];
        }

        return $page;
    }

    public static function getPageBySlug($slug)
    {
        $locale = $locale ?? app()->getLocale();
        $page = Page::where("slug->{$locale}", $slug)->firstOrFail();
        foreach ($page->fields as $field) {
            $page->attributes[$field->name] = $field->meta['value'];
        }
        return $page;
    }
    
    public static function getTemplates()
    {
        $templates = [];
        $files = File::allFiles(resource_path('views/vendor/raven/pages/templates'));
        foreach ($files as $file) {
            $name = explode('.', $file->getFilename());
            $templates[$name[0]] = ucwords($name[0]).' '.__('raven::messages.pages.create.form.template');
        }

        return $templates;
    }
}
