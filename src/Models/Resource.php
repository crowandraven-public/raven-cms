<?php

namespace CrowAndRaven\CMS\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $fillable = [
        'title',
        'url',
        'type',
        'created_by',
        'updated_by'
    ];

    public function posts()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Post', 'resourcable');
    }

    public function pages()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Page', 'resourcable');
    }

    public function lessons()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Lesson', 'resourcable');
    }

    public function series()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'resourcable');
    }

    /**
     * Create a new resource
     * @param  object $values These are $request values from form data
     * @return boolean        Return true if resource created
     */
    public static function create($values)
    {
        $translatable = ['title']; // these must be of type JSON
        $user = Auth::user();

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($values as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // select file over link if it exists
        if (isset($values['url_file']) && $values['url_file'] !='') {
            $url = $values['url_file'];
            $type = 'local';
        } elseif (isset($values['url_link']) && $values['url_link'] !='') {
            $url = $values['url_link'];
            $type = 'external';
        } else {
            return;
        }

        // save new resources
        $resource = new Resource;
        $resource->title = $title;
        $resource->url = $url;
        $resource->type = $type;
        $resource->created_by = $user->id;
        $resource->updated_by = $user->id;
        $resource->save();

        return $resource;
    }

    /**
     * Update a given resource
     * @param  object $values These are $request values from form data
     * @param  integer $id    The id of the resource to update
     * @return boolean        Return true if resource updated
     */
    public static function updateResource($values, $id)
    {
        $user = Auth::user();
        $resource = Resource::findOrFail($id);
        if (!$resource) {
            return;
        }

        $translatable = ['title']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($values as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // select file over link if it exists
        if (isset($values['url_file']) && $values['url_file'] !='') {
            $url = $values['url_file'];
            $type = 'local';
        } elseif (isset($values['url_link']) && $values['url_link'] !='') {
            $url = $values['url_link'];
            $type = 'external';
        } else {
            return;
        }

        $resource->title = $title;
        $resource->url = $url;
        $resource->type = $type;
        $resource->updated_by = $user->id;
        $resource->save();

        return $resource;
    }

    /**
     * Get all resources - mostly for display in the admin forms/selects to attach
     * resourcesto content.
     * @return object The object containing all resources.
     */
    public static function getAllResources($locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $resources = Resource::orderBy('title', 'asc')->pluck("title->{$locale} AS title", 'id');

        return $resources;
    }
}
