<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class Attachable extends Model
{
    protected $table = 'attachables';
    
    public $timestamps = false;

    protected $fillable = ['attachment_id', 'attachable_id', 'attachable_type'];
}
