<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class CMS extends Model
{
    /**
     * Get the CMS version
     */
    public static function version()
    {
        return '0.2';
    }
}
