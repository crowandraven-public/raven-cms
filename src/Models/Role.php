<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    /**
     * Provide relationship between role and user
     */
    public function users()
    {
        return $this->belongsToMany('CrowAndRaven\CMS\Models\User');
    }

    /**
     * Create a new role
     * @param  object $values These are $request values from form data
     * @return boolean        Return true if role created
     */
    public static function create($values)
    {
        $translatable = ['name', 'description']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($values as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        $slug = self::uniqueSlug(Str::slug($values['slug'], '-'));

        $role = new Role;
        $role->name = $name;
        $role->slug = $slug;
        $role->description = $description;
        $role->save();

        return $role;
    }

    /**
     * Update a given role
     * @param  object $values These are $request values from form data
     * @param  integer $id    The id of the role to update
     * @return boolean        Return true if role updated
     */
    public static function updateRole($values, $id)
    {
        $role = Role::findOrFail($id);
        if (!$role) {
            return;
        }

        $translatable = ['name', 'description']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($values as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        $slug = self::uniqueSlug(Str::slug($values['slug'], '-'), true);

        $role->name = $name;
        $role->slug = $slug;
        $role->description = strip_tags(substr($description, 0, 255));
        $role->save();

        return $role;
    }

    /**
     * Create a unique slug name
     * @param  string  $slug   The slug name to check if unique
     * @param  string  $locale What is the locale passed
     * @param  boolean $update Are we updating a role (true) or is this a new one (false)?
     * @return string          The updated role
     */
    public static function uniqueSlug($slug, $update = false)
    {
        $result = Role::where('slug', $slug)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $slug = $slug.'-'.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $slug = $slug.'-'.rand(1, 100);
            }
        }

        return $slug;
    }

    /**
     * Get a list of roles for admin forms
     * @param  string $locale Get a locale or show the default
     * @return array          An array of roles
     */
    public static function getAllRoles($locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $roles = Role::all();
        $roles = $roles->map(function ($r) use ($locale) {
            $name = localize($r->name, $locale);
            $r->name = $name;
            return $r;
        });

        return $roles->pluck('name', 'id');
    }
}
