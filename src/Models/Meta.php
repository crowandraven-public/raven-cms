<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $table = 'meta';

    protected $fillable = [
        'field_id',
        'value',
    ];

    /**
     * Get the Field associated with a given meta entry
     */
    public function field()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Field', 'id');
    }
}
