<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Image;

class Media extends Model
{
    protected $fillable = ['title', 'type', 'file', 'created_by'];

    public static function getAllMedia()
    {
        $media = Media::orderBy('title', 'asc')->pluck('title', 'file');

        return $media;
    }

    public static function getAllFiles()
    {
        $type_array = ['application/pdf'];
        $media = Media::whereIn('type', $type_array)->orderBy('title', 'asc')->pluck('title', 'file');

        return $media;
    }

    public static function getAllImages()
    {
        $type_array = ['image/jpeg', 'image/png', 'image/gif'];
        $media = Media::whereIn('type', $type_array)->orderBy('title', 'asc')->pluck('title', 'file');
        
        return $media;
    }

    public static function getExt($file)
    {
        $filename = basename($file);
        $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
        $ext = str_replace('jpeg', 'jpg', $ext);
        $ext = substr($ext, 0, 3);

        return $ext;
    }

    public static function getMime($file)
    {
        $img_mime = getimagesize($file);
        $mime = (isset($img_mime) ? $img_mime['mime'] : null);

        return $mime;
    }

    public static function uploadFile($orig, $newfile, $ext)
    {
        $save_path = storage_path('app/public/uploads/'.$newfile.'.'.$ext);
        $base64Image = base64_encode(file_get_contents('$orig'));
        $saved = Image::make($base64Image)->save($save_path);

        return $saved;
    }

    public static function uploadFavicon($orig, $newfile, $ext)
    {
        $save_path = storage_path('app/public/uploads/favicons/'.$newfile.'.'.$ext);
        $saved = Image::make($orig)->save($save_path);

        return $saved;
    }
}
