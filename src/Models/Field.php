<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'type',
        'attribs',
        'name',
        'description',
        'created_by',
        'updated_by'
    ];

    public function page()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\Page');
    }

    public function fieldGroup()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\FieldGroup');
    }

    public function meta()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Meta');
    }
}
