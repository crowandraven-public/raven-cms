<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table = 'role_user';

    public $timestamps = false;

    protected $fillable = [
        'role_id',
        'user_id'
    ];

    /**
     * Attach a role to a user
     * @param  integer $resource  The id of the role you're attaching
     * @param  integer $source    The id of the user which you want to attach the role
     * @return boolean            Was the attachment successful?
     */
    public static function attach($role_id, $user_id)
    {
        $tagged = new RoleUser;
        $tagged->role_id = $role_id;
        $tagged->user_id = $user_id;
        $tagged->save();

        return $tagged;
    }

    /**
     * Detach all roles from a user
     * @param  integer $source The id of the user which you want to detach the roles
     * @return boolean         Was the detachment successful?
     */
    public static function detach($user_id)
    {
        $detach = RoleUser::where('user_id', $user_id)->delete();

        return $detach;
    }
}
