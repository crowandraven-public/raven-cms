<?php

namespace CrowAndRaven\CMS\Models;

use App\User as AppUser;

class User extends AppUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'bio'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * Core Relationships
     */

    public function pages()
    {
        return $this->hasMany('CrowAndRaven\CMS\Models\Page', 'created_by');
    }

    public function posts()
    {
        return $this->hasMany('CrowAndRaven\CMS\Models\Post', 'author');
    }

    public function roles()
    {
        return $this->belongsToMany('CrowAndRaven\CMS\Models\Role');
    }

    public function likedPosts()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Post', 'likeable')->whereDeletedAt(null);
    }

    /**
     * Lessons Package Relationships
     */
    
    public function lessons()
    {
        return $this->hasMany('CrowAndRaven\CMS\Models\Lesson', 'author');
    }

    public function series()
    {
        return $this->hasMany('CrowAndRaven\CMS\Models\Series', 'author');
    }

    public function likedLessons()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Lesson', 'likeable')->whereDeletedAt(null);
    }

    public function likedSeries()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'likeable')->whereDeletedAt(null);
    }

    public function completedLessons()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Lesson', 'completeable')->whereDeletedAt(null);
    }

    public function completedSeries()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'completeable')->whereDeletedAt(null);
    }

    public function completedQuizzes()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Quiz', 'completeable')->whereDeletedAt(null);
    }

    public function enrolledSeries()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'enrollable')->whereDeletedAt(null);
    }

    /**
    * @param string|array $roles
    */
    public function authorizeRole($role)
    {
        return $this->roles()->where('slug', $role)->first();
    }

    /**
     * Get users by role for admin forms.
     */
    public static function getUsersByRole($role = 'admin')
    {
        $users = User::whereHas('roles', function ($query) use ($role) {
            $query->where('slug', $role);
        })->orderBy('name')->pluck('name', 'id');
        
        return $users;
    }
}
