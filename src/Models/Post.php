<?php

namespace CrowAndRaven\CMS\Models;

use Auth;
use Carbon\Carbon;
use CrowAndRaven\CMS\Models\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    use \CrowAndRaven\CMS\Traits\AttachablesTrait;
    
    protected $fillable = [
        'status',
        'type',
        'title',
        'subtitle',
        'slug',
        'content',
        'excerpt',
        'file',
        'image',
        'author',
        'created_by',
        'updated_by',
        'published_at',
        'is_featured'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'published_at'
    ];


    public function postImage()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Attachment', 'id', 'image');
    }

    public function postFile()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Attachment', 'id', 'file');
    }

    public function tags()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Tag', 'taggable');
    }

    public function resources()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Resource', 'resourcable');
    }

    public function likes()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\User', 'likeable')->whereDeletedAt(null);
    }
    
    public function author()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\User');
    }

    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }

    public static function create($request)
    {
        $translatable = ['title', 'subtitle', 'content']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // create unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'title-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, false);
            }
        }
        $slug = json_encode($fieldset);

        // create excerpt
        foreach ($request as $key => $value) {
            if (strpos($key, 'content-') === 0) {
                $fieldset[$loc] = strip_tags(substr($value, 0, 200));
            }
        }
        $excerpt = json_encode($fieldset);

        // get or set user id
        if (isset($request['updated_by'])) {
            $updated_by = $request['updated_by'];
        } else {
            $updated_by = Auth::user()->id;
        }

        $published_at = (isset($request['published_at']) && $request['published_at'] != '') ? $request['published_at'] : Carbon::now();
        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        $file = (isset($request['file']) && $request['file'] != '0') ? $request['file'] : null;
        $is_featured = (isset($request['is_featured']) ? $request['is_featured'] : null);
        $type = (isset($request['type']) ? $request['type'] : null);
        $author = (isset($request['author']) ? $request['author'] : null);

        // save new post
        $post = new Post;
        $post->status = $request['status'];
        $post->type = $type;
        $post->title = $title;
        $post->subtitle = $subtitle;
        $post->slug = $slug;
        $post->content = $content;
        $post->excerpt = $excerpt;
        $post->image = $image;
        $post->file = $file;
        $post->author = $author;
        $post->is_featured = $is_featured;
        $post->published_at = $published_at;
        $post->created_by = $updated_by;
        $post->updated_by = $updated_by;
        $post->save();

        return $post;
    }

    public static function updatePost($request, $id)
    {
        $translatable = ['title', 'subtitle', 'excerpt', 'content']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // check unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'slug-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, true);
            }
        }
        $slug = json_encode($fieldset);

        // get or set user id
        if (isset($request['updated_by'])) {
            $updated_by = $request['updated_by'];
        } else {
            $updated_by = Auth::user()->id;
        }

        $published_at = ($request['published_at'] != '' ? $request['published_at'] : Carbon::now());
        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        $file = (isset($request['file']) && $request['file'] != '0') ? $request['file'] : null;
        $is_featured = (isset($request['is_featured']) ? $request['is_featured'] : null);
        $type = (isset($request['type']) ? $request['type'] : null);
        $author = (isset($request['author']) ? $request['author'] : null);

        $post = Post::findOrFail($id);
        $post->status = $request['status'];
        $post->type = $type;
        $post->title = $title;
        $post->subtitle = $subtitle;
        $post->slug = $slug;
        $post->content = $content;
        $post->excerpt = $excerpt;
        $post->image = $image;
        $post->file = $file;
        $post->author = $author;
        $post->is_featured = $is_featured;
        $post->published_at = $published_at;
        $post->updated_by = $updated_by;
        $post->save();

        return $post;
    }

    /**
     * Create a unique slug name
     * @param  string  $slug   The slug name to check if unique
     * @param  string  $locale What is the locale passed
     * @param  boolean $update Are we updating a post (true) or is this a new one (false)?
     * @return string          The updated slug
     */
    public static function uniqueSlug($slug, $locale = null, $update = false)
    {
        $locale = $locale ?? app()->getLocale();
        $result = Post::where("slug->{$locale}", $slug)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $slug = $slug.'-'.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $slug = $slug.'-'.rand(1, 100);
            }
        }

        return $slug;
    }

    /**
     * Create a new (or update an) excerpt for the post. Pulls the first $size characters
     * @param  object  $data Post data which includes the post content
     * @param  integer $size Number of characters to grab for the excerpt
     * @return string        The new (or updated) excerpt
     */
    public static function getExcerpt($data, $size = 200)
    {
        $excerpt = (isset($data->excerpt) ? strip_tags(substr($data->excerpt, 0, $size)) : strip_tags(substr($data->content, 0, $size)));

        return $excerpt;
    }

    public static function getPosts($type = null, $track = null, $orderby = null)
    {
        $locale = $locale ?? app()->getLocale();

        $posts = Post::where('status', 1);

        if (isset($type)) {
            $post_type = Tag::getIdBySlug($type);
            $posts->where('type', $post_type);
        }

        if (isset($track)) {
            $posts->whereHas('tags', function ($query) use ($locale, $track) {
                $query->where("slug->{$locale}", $track);
            });
        }

        if ($orderby == 'name') {
            $posts->orderBy("title->{$locale}", 'desc');
        } else {
            $posts->orderBy('published_at', 'desc');
        }

        return $posts;
    }

    /**
     * Query scope to get the latest post
     * @param  object $query an existing query
     */
    public function scopeLatest($query)
    {
        return $query->orderBy('published_at', 'desc')->first();
    }

    /**
     * Query scope to get all featured posts
     * @param  object $query an existing query
     * @param  integer $take the number of posts to get
     */
    public function scopeFeatured($query, $take = null)
    {
        $query->where('is_featured', 1)->orderBy('published_at', 'desc');

        if (isset($take)) {
            $query->take($take);
        }

        return $query;
    }

    /**
     * Query scope to get the post pairs for forms
     * @param  object $query an existing query
     */
    public function scopePairs($query)
    {
        $locale = $locale ?? app()->getLocale();

        $posts = $query->orderBy("title->{$locale}")->get();

        $posts = $posts->map(function ($l) use ($locale) {
            $title = localize($l->title, $locale);
            $l->title = $title;
            return $l;
        });

        return $posts->pluck('title', 'id');
    }

    public static function getPostBySlug($slug)
    {
        $locale = $locale ?? app()->getLocale();

        $post = Post::where("slug->{$locale}", $slug)->firstOrFail();

        return $post;
    }
}
