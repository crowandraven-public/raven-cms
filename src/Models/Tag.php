<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tag extends Model
{
    /**
     * Get tags for a particular post
     */
    public function posts()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Post', 'taggable');
    }

    public function series()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'taggable');
    }

    public function lessons()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Lesson', 'taggable');
    }

    /**
     * Create a new tag
     * @param  object $values These are $request values from form data
     * @return boolean        Return true if tag created
     */
    public static function create($values)
    {
        $translatable = ['name', 'description']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($values as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // create unique name
        foreach ($values as $key => $value) {
            if (strpos($key, 'name-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueName($value, $loc, false);
            }
        }
        $name = json_encode($fieldset);

        // create unique slug
        foreach ($values as $key => $value) {
            if (strpos($key, 'name-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, false);
            }
        }
        $slug = json_encode($fieldset);

        $tag = new Tag;
        $tag->name = $name;
        $tag->slug = $slug;
        $tag->description = $description;
        $tag->type = $values['type'];
        $tag->synonyms = $values['synonyms'];
        $tag->save();

        return $tag;
    }

    /**
     * Update a given tag
     * @param  object $values These are $request values from form data
     * @param  integer $id    The id of the tag to update
     * @return boolean        Return true if tag updated
     */
    public static function updateTag($values, $id)
    {
        $tag = Tag::findOrFail($id);
        if (!$tag) {
            return;
        }

        $translatable = ['name', 'description']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($values as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // make sure unique name
        foreach ($values as $key => $value) {
            if (strpos($key, 'name-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueName($value, $loc, true);
            }
        }
        $name = json_encode($fieldset);

        // make sure unique slug
        foreach ($values as $key => $value) {
            if (strpos($key, 'slug-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, true);
            }
        }
        $slug = json_encode($fieldset);

        $tag->name = $name;
        $tag->slug = $slug;
        $tag->description = strip_tags(substr($description, 0, 255));
        $tag->type = $values['type'];
        $tag->synonyms = $values['synonyms'];
        $tag->save();

        return $tag;
    }

    /**
     * Create a unique tag name
     * @param  string  $name   The tag name to check if unique
     * @param  string  $locale What is the locale passed
     * @param  boolean $update Are we updating a tag (true) or is this a new one (false)?
     * @return string          The updated name
     */
    public static function uniqueName($name, $locale = null, $update = false)
    {
        $locale = $locale ?? app()->getLocale();
        $result = Tag::where("name->{$locale}", $name)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $name = $name.' '.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $name = $name.' '.rand(1, 100);
            }
        }

        return $name;
    }

    /**
     * Create a unique slug name
     * @param  string  $slug   The slug name to check if unique
     * @param  string  $locale What is the locale passed
     * @param  boolean $update Are we updating a tag (true) or is this a new one (false)?
     * @return string          The updated slug
     */
    public static function uniqueSlug($slug, $locale = null, $update = false)
    {
        $locale = $locale ?? app()->getLocale();
        $result = Tag::where("slug->{$locale}", $slug)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $slug = $slug.'-'.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $slug = $slug.'-'.rand(1, 100);
            }
        }

        return $slug;
    }

    /**
     * Get a list of tags by type for admin forms
     * @param  string $locale Get a locale or show the default
     * @return array          An array of tags
     */
    public static function tagPairsByType($type, $locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $tags = Tag::where('type', $type)->get();
        $tags = $tags->map(function ($t) use ($locale) {
            $name = localize($t->name, $locale);
            $t->name = ucwords($name);
            return $t;
        });

        return $tags->pluck('name', 'id');
    }

    /**
     * Get a list of tags by type
     * @param  string $locale Get a locale or show the default
     * @return array          An array of tags
     */
    public static function tagsByType($type, $locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $tags = Tag::where('type', $type)->get();

        return $tags;
    }

    /**
     * Get the tag name by ID. Mostly used in views, but it could be used elsewhere.
     * @param  integer $id     This is the tag ID.
     * @param  string  $locale This is the locale we'll use for the translation.
     *                         If blank, we'll use the default app locale.
     * @return string          This is the returned tag name.
     */
    public static function getNameById($id, $locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $name_array = Tag::where('id', $id)->pluck("name->{$locale} AS name");
        $name = str_replace('"', '', $name_array[0]);

        return $name;
    }

    /**
     * Get the tag slug by ID. Mostly used in views, but it could be used elsewhere.
     * @param  integer $id     This is the tag ID.
     * @param  string  $locale This is the locale we'll use for the translation.
     *                         If blank, we'll use the default app locale.
     * @return string          This is the returned tag slug.
     */
    public static function getSlugById($id, $locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $slug_array = Tag::where('id', $id)->pluck("slug->{$locale} AS slug");
        $slug = str_replace('"', '', $slug_array[0]);

        return $slug;
    }

    public static function getIdBySlug($slug, $locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $slug = Tag::where("slug->{$locale}", $slug)->pluck('id');

        return $slug;
    }

    public static function getBySlug($slug, $locale = null)
    {
        $locale = $locale ?? app()->getLocale();
        $tag = Tag::where("slug->{$locale}", $slug)->firstorFail();

        return $tag;
    }
}
