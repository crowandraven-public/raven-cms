<?php

namespace CrowAndRaven\CMS\Traits;

Trait AttachablesTrait
{   
    
    public function attachments()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Attachment', 'attachable')->withPivot('meta');
    }

    public function updateAttachments($requestAttachments) 
    {
        $attachmentsPostMeta = json_decode($requestAttachments);
        if (is_object($attachmentsPostMeta)) {
            $itemsToSync = [];
            foreach($attachmentsPostMeta->items as $item) {       
                $order = (isset($item->order)) ? $item->order : 0; 
                $itemsToSync[$item->id] = ['meta' => json_encode(['order' => $order])];
            }
            $this->attachments()->sync($itemsToSync);
        }   
    }

    public function getPreppedAttachments() 
    {
        $items = $this->attachments;
        $formatted = [];
        foreach($items as $item) {
            if (!isset($item->pivot->meta)) {
                return [];
            }
            $meta = json_decode($item->pivot->meta);
            $attachment = $item->toArray();
            $attachment = array_map(function($a) {
                if (is_string($a) && is_object($object = json_decode($a))) {
                    return localize($a);
                }
                return $a;
            }, $attachment);
            $attachment['order'] = (isset($meta->order)) ? $meta->order : 0;
            $formatted[] = $attachment;
        }
        return json_encode($formatted);
    }

    public static function getModelName() 
    {
        $path = explode('\\', __CLASS__);
        $model = strtolower(end($path));
        return $model;
    }

    public function getAttachmentAttribute($relationField, $field, $fallback=null) 
    {
        $model = self::getModelName();
        $relation = $this->{$model.ucfirst($relationField)};
        
        if (!$relation) {
            return $fallback;
        }

        $value = isset($relation->{$field}) ? $relation->{$field} : null;
        
        if (!$value) {
            return $fallback;
        }
        return $value;
    }
}

