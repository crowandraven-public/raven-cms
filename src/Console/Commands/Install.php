<?php

namespace CrowAndRaven\CMS\Console\Commands;

use CrowAndRaven\CMS\Console\Command;
use CrowAndRaven\CMS\Models\User;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'raven:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Raven CMS on your application.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line("Initializing installation script.", 'green');
        
        shell_exec('php artisan vendor:publish --provider="CrowAndRaven\CMS\CMSServiceProvider" --tag=raven-config');
        $this->line("The CMS configuration file was published.", 'green');

        shell_exec('php artisan vendor:publish --provider="CrowAndRaven\CMS\CMSServiceProvider" --tag=raven-migrations');
        $this->line("The CMS database migrations have been published.", 'green');
        
        shell_exec('composer dump-autoload');
        
        shell_exec('php artisan migrate');
        $this->line("The CMS migrations have been run.", 'green');

        shell_exec('php artisan vendor:publish --provider="CrowAndRaven\CMS\CMSServiceProvider" --tag=raven-seeds');
        shell_exec('composer dump-autoload');
        shell_exec('php artisan db:seed');
        $this->line("The CMS database tables have been seeded.", 'green');

        shell_exec('php artisan vendor:publish --provider="CrowAndRaven\CMS\CMSServiceProvider" --tag=raven-assets');
        $this->line("All CMS assets have been published.", 'green');

        shell_exec('php artisan vendor:publish --provider="CrowAndRaven\CMS\CMSServiceProvider" --tag=raven-lang');
        $this->line("All CMS language files have been published.", 'green');

        shell_exec('php artisan vendor:publish --provider="CrowAndRaven\CMS\CMSServiceProvider" --tag=raven-views');
        $this->line("All CMS views have been published.", 'green');

        // set admin
        $email = $this->ask('What is the email for the user you want to make an admin?');
        
        $user = User::where('email', $email)->first();

        if ($user) {
            $user->is_admin = '1';
            $user->save();
            $this->line('Nice work. That user is now a Raven CMS admin.', 'green');
        } else {
            $this->error('That email could not be found.');
            $this->error('Quitting.');
            $this->error('You\'ll need to provide a valid user before the install can complete.');
        }

        $this->line("Wohoo! The install is complete.", 'green');
    }
}
