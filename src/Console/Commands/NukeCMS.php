<?php

namespace CrowAndRaven\CMS\Console\Commands;

use CrowAndRaven\CMS\Console\Command;
use CrowAndRaven\CMS\Controllers\Admin\CleanController;

class NukeCMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'raven:nuke';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all remenants of the Raven CMS from my system.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CleanController $cleanController)
    {
        $this->line("");
        $this->line("This script will remove Raven CMS and all related info from your application.");
        $this->line("This includes views, configs, uploads, assets, and database tables.");
        $this->line("Are you sure you want to nuke it? Have you backed up your database?");

        if ($this->confirm("Do you wish to continue?")) {
            $this->line("Preparing to nuke the Raven CMS...", 'green');

            $cleanController->nukeConfig($this);
            $this->line("The CMS config file has been removed.", 'green');

            $cleanController->nukeLang($this);
            $this->line("The CMS language files files have been removed.", 'green');

            $cleanController->nukeAssets($this);
            $this->line("All CMS assets have been removed.", 'green');

            $cleanController->nukeUploads($this);
            $this->line("All uploads have been removed.", 'green');
            
            $cleanController->nukeViews($this);
            $this->line("CMS-related views have been removed.", 'green');
            
            $cleanController->nukeDatabaseTables($this);
            $this->line("CMS-related database tables have been deleted...", 'green');

            //$cleanController->nukeMigrations($this);
            //echo 'Migrations removed...\n';
            
            $cleanController->dumpAutoload($this);

            $cleanController->clearCache($this);

            $cleanController->clearViews($this);

            $this->line("...Nuke is complete.", 'green');
        }
    }
}
