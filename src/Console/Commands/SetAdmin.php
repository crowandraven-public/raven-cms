<?php

namespace CrowAndRaven\CMS\Console\Commands;

use CrowAndRaven\CMS\Console\Command;
use CrowAndRaven\CMS\Models\User;

class SetAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'raven:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give admin priveleges to an existing user in the application.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask('What is the email for the user you want to make an admin?');
        
        $user = User::where('email', $email)->first();

        if ($user) {
            $user->is_admin = '1';
            $user->save();
            $this->line('Nice work. That user is now a Raven CMS admin.', 'green');
        } else {
            $this->error('That email could not be found.');
            $this->error('Quitting.');
        }
    }
}
