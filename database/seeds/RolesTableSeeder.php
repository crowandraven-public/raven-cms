<?php
use Illuminate\Database\Seeder;

use CrowAndRaven\CMS\Models\Role;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        $role_employee = new Role();
        $role_employee->name = '{"en": "Employee"}';
        $role_employee->slug = 'employee';
        $role_employee->description = '{"en": "An employee user"}';
        $role_employee->save();

        $role_member = new Role();
        $role_member->name = '{"en": "Member"}';
        $role_member->slug = 'member';
        $role_member->description = '{"en": "A member user"}';
        $role_member->save();

        $role_content = new Role();
        $role_content->name = '{"en": "Content Manager"}';
        $role_content->slug = 'content';
        $role_content->description = '{"en": "A content manager user"}';
        $role_content->save();

        $role_admin = new Role();
        $role_admin->name = '{"en": "App Admin"}';
        $role_admin->slug = 'admin';
        $role_admin->description = '{"en": "The super admin user"}';
        $role_admin->save();

        $role_admin = new Role();
        $role_admin->name = '{"en": "Author"}';
        $role_admin->slug = 'author';
        $role_admin->description = '{"en": "An author of posts"}';
        $role_admin->save();
    }
}
