<?php
use Illuminate\Database\Seeder;

use Carbon\Carbon;
use CrowAndRaven\CMS\Models\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag1 = new Tag();
        $tag1->name = '{"en": "blog","es": "blog"}';
        $tag1->slug = '{"en": "blog","es": "blog"}';
        $tag1->type = 'type';
        $tag1->description = '{"en":"This is the blog post type.","es":"Este es el tipo de publicaci\u00f3n de blog."}';
        $tag1->synonyms = 'post, articles';
        $tag1->created_at = Carbon::now();
        $tag1->updated_at = Carbon::now();
        $tag1->save();

        $tag2 = new Tag();
        $tag2->name = '{"en": "news","es": "noticias"}';
        $tag2->slug = '{"en": "news","es": "noticias"}';
        $tag2->type = 'type';
        $tag2->description = '{"en":"This is the news post type.","es":"Este es el tipo de publicaci\u00f3n de noticias."}';
        $tag2->synonyms = 'post, articles';
        $tag2->created_at = Carbon::now();
        $tag2->updated_at = Carbon::now();
        $tag2->save();

        $tag3 = new Tag();
        $tag3->name = '{"en": "topic1","es": "tema1"}';
        $tag3->slug = '{"en": "topic1","es": "tema1"}';
        $tag3->type = 'topic';
        $tag3->description = '{"en":"This is a sample topic tag.","es":"Esta es una etiqueta de tema de muestra."}';
        $tag3->synonyms = 'synonym1, synonym2';
        $tag3->created_at = Carbon::now();
        $tag3->updated_at = Carbon::now();
        $tag3->save();

        $tag4 = new Tag();
        $tag4->name = '{"en": "topic2","es": "tema2"}';
        $tag4->slug = '{"en": "topic2","es": "tema2"}';
        $tag4->type = 'topic';
        $tag4->description = '{"en":"This is another sample topic tag.","es":"Este es otro tema de muestra."}';
        $tag4->synonyms = 'synonym1, synonym2';
        $tag4->created_at = Carbon::now();
        $tag4->updated_at = Carbon::now();
        $tag4->save();
    }
}
