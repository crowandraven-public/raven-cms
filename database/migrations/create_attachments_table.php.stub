<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->json('title');
            $table->json('description')->nullable();
            $table->json('caption')->nullable();
            $table->json('alt_text')->nullable();
            $table->string('external_url')->nullable();
            $table->string('file_type')->nullable();
            $table->string('file_name')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });

        Schema::create('attachables', function (Blueprint $table) {
            $table->integer('attachment_id')->unsigned();
            $table->integer('attachable_id')->unsigned();
            $table->string('attachable_type');
            $table->json('meta')->nullable();
            $table->foreign('attachment_id')->references('id')->on('attachments')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachables');

        Schema::dropIfExists('attachments');
    }
}
