const state = {
    components: [],
}

const actions = {
    addComponentToStore(context, { componentId }) {
        context.commit('addEmptyComponentSlot', { 
            componentId: componentId,
            textQuery: '',
            selectedItems: [],
            searchResultItems: [],
            paginatedResultItems: [],
            storedSearchResultItems: [],
            searchResultItemsCached: [],
            isLoading: false,
            currentPage: 1
        });
    },
    updateItems(context, data) {
        let componentId = data.componentId;
        let foundComponent = context.getters.getComponentById(componentId);
        context.commit('updateItems', { component: foundComponent, items: data.items });
    },
    updateSearchResultItems(context, data) {
        let componentId = data.componentId;
        let foundComponent = context.getters.getComponentById(componentId);
        context.commit('updateSearchResultItems', { component: foundComponent, items: data.items });
    },
    updateStoredSearchResultItems(context, data) {
        let componentId = data.componentId;
        let foundComponent = context.getters.getComponentById(componentId);
        context.commit('updateStoredSearchResultItems', { component: foundComponent, items: data.items });
    },
    updatePaginatedResultItems(context, data) {
        let componentId = data.componentId;
        let foundComponent = context.getters.getComponentById(componentId);
        context.commit('updatePaginatedResultItems', { component: foundComponent, items: data.items });
    },
    updateTextQuery(context, data) {
        let componentId = data.componentId;
        let foundComponent = context.getters.getComponentById(componentId);
        context.commit('updateTextQuery', { component: foundComponent, textQuery: data.textQuery });
    },
    nextPage(context, data) {
        let componentId = data.componentId;
        let foundComponent = context.getters.getComponentById(componentId);
        context.commit('nextPage', { component: foundComponent });
    },
    previousPage(context, data) {
        let componentId = data.componentId;
        let foundComponent = context.getters.getComponentById(componentId);
        context.commit('previousPage', { component: foundComponent });
    },
}

const getters = {
    getComponentById: (context) => (id) => {
       return context.components.find(c => c.componentId == id);
    }
}

const mutations = {
    addEmptyComponentSlot(context, data) {
        context.components.push(data);
    },
    updateItems(context, { component, items }) {
        component.selectedItems = items;
    },
    updateSearchResultItems(context, { component, items }) {
        component.searchResultItems = items;
    },
    updatePaginatedResultItems (context, { component, items }) {
        component.paginatedResultItems = items; 
    },
    updateStoredSearchResultItems(context, { component, items }) {
        component.storedSearchResultItems = items;
    },
    updateTextQuery (context, { component, textQuery }) {
        component.textQuery = textQuery;
    },
    nextPage: function(context, { component }) {
        component.currentPage+=1;
    },
    previousPage: function(context, { component }) {
        if(component.currentPage == 0) {
            return;
        }
        component.currentPage-=1;
    }
}
export default  {
    namespaced: true,
    actions,
    state,
    getters,
    mutations
}
