import Vuex from 'vuex'
import Vue from 'vue'
import addbysearch from './modules/addbysearchstore'
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        addbysearch: addbysearch
    },
    strict: debug
})