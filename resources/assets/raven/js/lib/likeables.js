// Likeable is general way of saving something for later
// It's general enough and can be used for things like bookmarks or marking something as complete.
// It's binary, meaning it can be 0 or 1, or liked or not liked.

// Create the constructor function
let Likeable = function($element, config) {
    let suggestedConfig = (typeof config != 'undefined') ? config : {};
    this.$element = $element;
    this.config = this.setConfig(suggestedConfig);
    if(this.config === false) {
        return;
    }
    this.setLiked();
    this.setUpEvents();

    // store reference of "this" on element data
    this.$element.data('likeable', this);
}

// Make the prototype, properties and methods
Likeable.prototype = {
    config: {
        likeableActionUrl: null,
        headers: null,
        onSuccessCallback: null,
        testItem: false,
        usingSpans: false,
        liked: false,
        clickDisabled: false
    },
    onFinish: null,
    setLiked: function() {
        this.setLikedInHtml(this.config.liked);
    },
    setConfig: function(suggestedConfig) {
        let config = {};
        
        if(typeof this.$element.data('likeableActionUrl') == 'undefined') {
            return false;
        }
        if(typeof this.$element.data('liked') == 'undefined') {
            return false;
        }

        config.liked = (this.$element.data('liked') == '1') ? true : false;
        
        if(typeof this.$element.data('likeableActionUrl') != 'undefined') {
            config.likeableActionUrl = this.$element.data('likeableActionUrl');
        }
        if(typeof suggestedConfig.testItem != 'undefined') {
            config.testItem = suggestedConfig.testItem;
        }
        if(this.$element.find('.likeable-on').length) {
            config.usingSpans = true;
        }
        if(typeof this.$element.data('likeableDisableClick') != 'undefined' && this.$element.data('likeableDisableClick') == '1') {
            config.clickDisabled = true;
        } else {
            config.clickDisabled = false;
        }
        return config;
    },
    setToLiked: function() {
        if(this.config.liked == false) {
            this.sendData();
        }
    },
    setToUnliked: function() {
        if(this.config.liked == true) {
            this.sendData();
        }
    },
    setLikedInHtml: function(liked) {
        if(this.config.usingSpans === false) {
            if(liked === true) {
                this.$element.addClass('likeable-action-liked');
                this.$element.data('liked', 1);
                return;
            } else {
                this.$element.removeClass('likeable-action-liked');
                this.$element.data('liked', 0);
            } 
        }
        if(liked === true) {
            this.$element.addClass('likeable-action-liked');
            this.$element.find('.likeable-on').addClass('on').removeClass('off');
            this.$element.find('.likeable-off').addClass('off').removeClass('on');
            this.$element.data('liked', 1);
            return;
        }
        this.$element.removeClass('likeable-action-liked');
        this.$element.find('.likeable-on').addClass('off').removeClass('on');
        this.$element.find('.likeable-off').addClass('on').removeClass('off');
        this.$element.data('liked', 0);
        return;
    },
    setUpEvents: function() {
        // reg click event if not disabled
        if(this.config.clickDisabled === false) {
            this.$element.on('click', this.handleClick.bind(this));
        } else {
            this.$element.on('click', function(e) {
                e.preventDefault();
            });
        }
    },
    handleClick: function(e) {
        e.preventDefault();
        this.sendData();
    },
    getHeaders: function() {
        if(this.config.headers != null) {
            return this.config.headers;
        }
        if(typeof likeables.defaultHeaders != 'undefined') {
            return likeables.defaultHeaders;
        }
        return {};
    },
    sendData: function() {
        if(this.config.testItem === true) {
            this.returnTestData();
            return;
        }
        let headers = this.getHeaders();
        let self = this;
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: this.config.likeableActionUrl,
            headers: headers
        }).then(function(d) {
            if(d.success) {
                if(d.liked) {
                    self.liked = true;
                } else {
                    self.liked = false;
                }

                self.setLikedInHtml(d.liked);
                if(self.config.onSuccessCallback != null) {
                    self.config.onSuccessCallback(self.$element, d, this); 
                }

                let onFinishEvent = new CustomEvent('onFinish', { detail: { liked: d.liked } });
                self.$element[0].dispatchEvent(onFinishEvent);
            }
        }.bind(this));
    },
    returnTestData: function() {
        let self = this;
        if(typeof likeables != 'undefined') {
            item = null;
            likeables.pretendDataBase.forEach(function(l) {
                if(l.postId === self.config.likeableId) {
                    if(l.liked === true) {
                        l.liked = false;
                    } else {
                        l.liked = true;
                    }
                    item = l;
                }
            });
            if(item != null) {
                let data = {};
                data.liked = item.liked;
                self.setLikedInHtml(data.liked);
                if(self.config.onSuccessCallback != null) {
                    self.config.onSuccessCallback(self.$element, data, this); 
                }
            }
        }
    }
}

/* global likeables
 * What does this do?
 * It searches a pages for likeables and instantiates objects of Likeable if it finds them.
 * What's necessary?
 * For an DOM element to be searched and created into a Likeable, it needs to have these data attributes:
 *  - [data-likeable-action-url] This becomes part of the action url for when an AJAX request is made (e.g. "like", "setcomplete")
 *  - [data-liked] The inital liked state
 *  - [data-likeable] Must be set to 1, e.g. data-likeable="1"
 */

let likeables = likeables || {};
likeables.items = [];
likeables.defaultHeaders = {};
likeables.defaultOnSuccessCallback = function($element, data, likeableObject) {} 

if(typeof window.Spark != 'undefined') {
    if(typeof window.Spark.csrfToken != 'undefined') {
        likeables.defaultHeaders['X-CSRF-TOKEN'] = window.Spark.csrfToken;
    }
}

likeables.initAll = function() {
    let self = this;
    if($('[data-likeable="1"]').length) {
        $('[data-likeable="1"]').each(function() {
            self.items.push(new Likeable($(this)));
        });
    }
    $('head').append('<style>.likeable-on.on, .likeable-off.on { display: block; } .likeable-off.off, .likeable-on.off { display: none; }</style>');
}

likeables.runTest = function() {
    let self = this;
    this.testItems = [];
    this.pretendDataBase = [];
    
    $('head').append('<style>.likeable-on.on, .likeable-off.on { display: block; } .likeable-off.off, .likeable-on.off { display: none; }</style>');
    $('body').append('<i data-likeable-test-item="1" data-liked="1"><span class="likeable-off">Not favorited</span><span class="likeable-on">Favorited</span></i>');
    $('body').append('<i data-likeable-test-item="1" data-liked="0"><span class="likeable-off">Not Bookmarked</span><span class="likeable-on">Bookmarked</span></i>');
    
    $('[data-likeable-test-item="1"]').each(function() {
        let config = {};
        config.testItem = true;
        config.liked = $(this).data('liked');
        self.testItems.push(new Likeable($(this), config));
        
        // testing callbacks
        config.onSuccessCallback = function($element, data, likeableObject) {

            // stuff here
            
        }
        self.pretendDataBase.push(
            {
                postId: config.likeableId,
                liked: (config.liked == 1) ? true : false
            }
        );
    });
}

window.likeables = likeables;

$(function() {
    likeables.initAll();
});