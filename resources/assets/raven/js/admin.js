import Vue from 'vue'
import FileUpload from './components/FileUpload';
import FieldGroupsEditor from './components/FieldGroupsEditor';
import QuizEditor from './components/QuizEditor';
import AddMultipleEditor from './components/AddMultipleEditor';
import AddBySearchEditor from './components/AddBySearchEditor';
import JsonEditor from './components/JsonEditor';
import store from './store';
import VueClipboard from 'vue-clipboard2';

require('./../scss/admin.scss');

let jQuery = require('jquery');
window.$ = window.jQuery = jQuery;

Vue.use(VueClipboard);

var filter = function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('truncate', filter);

if(document.getElementById('raven-fields-ui')) {
	Vue.config.devtools = true;
	new Vue({
		el: '#raven-fields-ui',
		store,
		components: {
			FileUpload,
			FieldGroupsEditor,
			AddBySearchEditor,
			AddMultipleEditor,
			QuizEditor,
			JsonEditor
		},
		data: {}
	});
}
