@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.fieldgroups.create.title'))
@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/fieldgroups', 'role' => 'form', 'id' => 'fieldGroupForm', 'method' => 'post']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.fieldgroups.create.title') }}</h1>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('title', __('raven::messages.fieldgroups.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Page to associate these fields to -->
<div class="row form-group">
    {!! Form::label('page_id', __('raven::messages.fieldgroups.create.form.page'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('page_id', $all_pages, null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Create Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.fieldgroups.create.title'), ['class' => 'btn btn-primary btn-md margintop-sm']) !!}
    </div>
</div>
@endsection

@section('sidebar')
@endsection

@section('formend')
{!! Form::close() !!}
@endsection