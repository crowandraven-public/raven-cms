@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.fieldgroups.edit.title'))
@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/fieldgroups/'.$fieldgroup->id, 'class' => 'form-fieldgroup-'.$fieldgroup->id, 'role' => 'form', 'id' => 'fieldGroupForm', 'method' => 'put']) !!}
@endsection

@section('content')

<?php $field_types_text = ''; ?>

@foreach ($field_types_array as $v)
    <?php $field_types_text .= sprintf('<option value="%s">%s</option>', $v, $v); ?>
@endforeach
<script>var fieldTypesText = '<?php echo $field_types_text; ?>';</script>

<script>var fieldGroupFields = <?php echo $fieldgroup->fields->toJson(); ?>;</script>

<h1>{{ __('raven::messages.fieldgroups.edit.title') }}</h1>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('title', __('raven::messages.fieldgroups.edit.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::text('title', $fieldgroup->title, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Page to associate these fields to -->
<div class="row form-group">
    {!! Form::label('page_id', __('raven::messages.fieldgroups.edit.form.page'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('page_id', $all_pages, $fieldgroup->page_id, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row form-group">
    <div class="col-md-12 margintop-xs">
        <strong>{{ __('raven::messages.fieldgroups.edit.form.fields') }}</strong>
    </div>
</div>

<div class="row form-group">
    <div class="col-md-12 margintop-xs">
        <div id="raven-fields-ui">
            <field-groups-editor></field-groups-editor>
        </div>
    </div>
</div>


<!-- Update Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.fieldgroups.edit.title'), ['class' => 'btn btn-primary btn-md margintop-sm']) !!}
    </div>
</div>

{!! Form::close() !!}

<div class="row">
    <div class="col-md-12">
        {!! Form::open(['url' => 'admin/fieldgroups/'.$fieldgroup->id.'/copy', 'class' => 'form-duplicate-fieldgroup-'.$fieldgroup->id, 'role' => 'form', 'id' => 'fieldGroupForm']) !!}
            {!! Form::submit(__('raven::messages.fieldgroups.edit.form.duplicate'), ['class' => 'btn btn-default margintop-sm']) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection

@section('sidebar')
@endsection

@section('formend')
@endsection

@section('fscripts')
@endsection