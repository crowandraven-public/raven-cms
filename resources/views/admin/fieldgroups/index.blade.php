@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.fieldgroups.index.title'))
@section('body-class', 'admin')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h1>
            {{ __('raven::messages.fieldgroups.index.title') }}
            <a href="/admin/fieldgroups/create" class="btn btn-primary">{{ __('raven::messages.fieldgroups.index.add_new') }}</a>
        </h1>
    </div>
</div>

<!-- Field Groups List -->
<div class="row">
    <div class="col-md-12 margintop-sm">
        @if ($fieldgroups)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.fieldgroups.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.fieldgroups.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col">
                        </th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($fieldgroups as $fieldgroup)
                        <tr>
                            <th scope="row">
                                <a href="/admin/fieldgroups/{{ $fieldgroup->id }}/edit">{{ $fieldgroup->title }}</a>
                            </th>
                            <td>
                                {{ $fieldgroup->updated_at->format('M j, Y g:i:s a') }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $fieldgroup->id }}">
                                    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ __('raven::messages.fieldgroups.index.table.delete') }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-sm-12">
        {{ $fieldgroups->appends(Request::except('page'))->links() }}
    </div>
</div>

@foreach ($fieldgroups as $fieldgroup)
    <!-- Delete Modal -->
    <div class="modal fade" id="delete-{{ $fieldgroup->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.fieldgroups.index.table.delete') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.fieldgroups.partials.delete')
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('fscripts')
@endsection
