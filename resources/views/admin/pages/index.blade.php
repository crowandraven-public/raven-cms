@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.pages.index.title'))

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h1>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('raven::messages.pages.index.title') }}
            @endif
            <a href="/admin/pages/create" class="btn btn-primary">{{ __('raven::messages.pages.index.add_new') }}</a>
        </h1>
    </div>
    <div class="col-md-6 text-right">
        <form class="inline form-inline" action="/admin/pages" method="GET">
            <div class="form-group">
                <label class="sr-only" for="search">{{ __('raven::messages.pages.index.search.label') }}</label>
                <input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.pages.index.search.placeholder') }}">
            </div>
        </form>
    </div>
</div>

<!-- Pages List -->
<div class="row">
    <div class="col-md-12 margintop-sm">
        @if (isset($pages))
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.pages.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.pages.index.table.slug') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.pages.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pages as $page)
                        <tr>
                            <th scope="row">
                                <a href="/{{ localize($page->slug) }}"><i class="fas fa-external-link-alt fa-fw"></i></a> <a href="/admin/pages/{{ $page->id }}/edit">{{ localize($page->title) }}</a>
                            </th>
                            <td>
                                {{ localize($page->slug) }}
                            </td>
                            <td>
                                {{ $page->updated_at->format('M j, Y g:i:s a') }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $page->id }}">
                                    <i class="fas fa-times" data-toggle="tooltip" data-placement="top" title="{{ __('raven::messages.pages.index.table.delete') }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            {{ $pages->appends(Request::except('page'))->links() }}
        </div>
    </div>
@endif

<!-- Delete Modal -->
@if (isset($pages))
    @foreach ($pages as $page)
        <!-- Delete Modal -->
        <div class="modal fade" id="delete-{{ $page->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.pages.index.table.delete') }}</h4>
                    </div>
                    <div class="modal-body">
                        @include('raven::admin.pages.partials.delete')
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
@endsection

@section('fscripts')
@endsection
