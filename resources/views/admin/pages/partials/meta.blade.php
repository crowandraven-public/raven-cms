@if (is_var_iterable($page->fieldGroup))

    <div class="row">
        <div class="col-md-12">
            <h3>{{ __('raven::messages.pages.edit.form.meta') }}</h3>
        </div>
    </div>

     <div class="row form-group">
        <div class="col-md-12">
            <div id="raven-fields-ui">
                <?php $fields = $page->fields->sortBy('order'); ?>
                @foreach ($fields as $field)
                    <?php
                    $attribs = (array) json_decode($field->attribs);
                    if (array_key_exists('class', $attribs)) {
                        unset($attribs['class']);
                    }
                    $options = [];
                    if ($field->type === 'select') {
                        if (isset($attribs->options)) {
                            $options = (array) $attribs['options'];
                        } elseif (array_key_exists('options', $attribs)) {
                            $options = $attribs['options'];
                        }
                        unset($attribs['options']);
                    } ?>

        
                    @if ($field->type == 'textarea')

                        {!! Form::label($field->name, ucwords(str_replace('_',' ',$field->name)), ($attribs + ['class' => 'col-md-12 control-label'])) !!}
                        {!! Form::textarea($field->name, (isset($field->meta->value)) ? $field->meta->value : '', $attribs + ['class' => 'form-control','rows' => '7']) !!}
                        @if ($field->description)
                            <p class="small help-block">{{ $field->description }}</p>
                        @endif

                    @elseif ($field->type == 'select')

                        {!! Form::label($field->name, ucwords(str_replace('_',' ',$field->name)), ($attribs + ['class' => 'col-md-12 control-label'])) !!}
                        {!! Form::select($field->name, $options, (isset($field->meta->value)) ? $field->meta->value : '', $attribs + ['class' => 'form-control']) !!}
                        @if ($field->description)
                            <p class="small help-block">{{ $field->description }}</p>
                        @endif

                    @elseif ($field->type == 'checkbox')

                        {!! Form::label($field->name, ucwords(str_replace('_',' ',$field->name)), ($attribs + ['class' => 'col-md-12 control-label'])) !!}
                        {!! Form::checkbox($field->name, '1', (!empty($field->meta->value)) ? true : false, $attribs + ['class' => 'form-control']) !!}
                        @if ($field->description)
                            
                        @endif

                    @elseif ($field->type == 'image')

                        <file-upload field-name="{{ $field->name }}">
                            {{ $field->description }}
                        </file-upload>

                    @elseif ($field->type == 'addbysearch')
                        <add-by-search-editor
                            ajax-url="{{ json_decode($field->attribs)->ajaxUrl }}" 
                            field-name="{{ $field->name }}"
                            search-fields="{{ json_decode($field->attribs)->fields }}"
                            image-field-name="{{ json_decode($field->attribs)->imageFieldName }}"
                            image-path-prefix="/img/"
                            :use-file-uploader="true">
                        </add-by-search-editor>                      

                    @elseif ($field->type == 'json_editor')

                        <json-editor
                            button-label="add row"
                            label-text="{{ $field->name }}"
                            field-name="{{ $field->name }}"
                            field-data-url="/admin/ravenfields/page/{{ $page->id }}?field={{ $field->name }}"
                            :enable-debug="{{ (env('APP_DEBUG')) ? 'true' : 'false' }}">
                        </json-editor>

                    @elseif ($field->type == 'horizontal_rule')
                        <hr>
                    @elseif ($field->type == 'help_section')
                        <h5>{{ ucwords(str_replace('_',' ',$field->name)) }}</h5>
                        <p>{{ $field->description }}</p>
                    @else
                        {!! Form::label($field->name, ucwords(str_replace('_',' ',$field->name)), ($attribs + ['class' => 'col-md-12 control-label'])) !!}
                        {!! Form::text($field->name, (isset($field->meta->value)) ? $field->meta->value : '' , $attribs + ['class' => 'form-control']) !!}

                        @if ($field->description)
                            <p class="small help-block">{{ $field->description }}</p>
                        @endif
                    @endif

                @endforeach 
            </div>
        </div>
    </div>

    

    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
@endif
