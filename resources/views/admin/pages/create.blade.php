@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.pages.create.title'))

@section('scripts')
<script src="https://cdn.tiny.cloud/1/va0cczdbx8gc8utla2gv66pu2ip8nq2yanvgzp9sq0g1bvsj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({
    selector:'textarea',
    height: 300,
    theme: 'silver',
    plugins: [
        'advlist autolink lists link image charmap hr anchor',
        'searchreplace wordcount visualblocks visualchars code',
        'media nonbreaking save table contextmenu directionality',
        'paste imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
    menubar: 'tools',
    image_advtab: true,
});</script>
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/pages', 'role' => 'form', 'method' => 'post']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.pages.create.title') }}</h1>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('title', __('raven::messages.pages.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('title-'.$locale, '', ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('title-'.config('raven-cms.fallback_locale'), '', ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- Subitle -->
@if (config('raven-cms.show.pages.subtitle'))
    <div class="row form-group">
        {!! Form::label('subtitle', __('raven::messages.pages.create.form.subtitle'), ['class' => 'col-md-12 control-label']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::text('subtitle-'.$locale, '', ['class' => 'form-control']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::text('subtitle-'.config('raven-cms.fallback_locale'), '', ['class' => 'form-control']) !!}
            </div>
        @endif
    </div>
@endif

<!-- Content -->
<div class="row form-group">
    {!! Form::label('content', __('raven::messages.pages.create.form.content'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('content-'.$locale, '', null, ['class' => 'form-control','rows' => '5', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('content-'.config('raven-cms.fallback_locale'), '', null, ['class' => 'form-control','rows' => '5', 'required' => 'required']) !!}
        </div>
    @endif
</div>


<!-- Image -->
@if (config('raven-cms.show.pages.image'))
    @if ($all_images->count() > 1)
        <div class="row form-group">
            {!! Form::label('image', __('raven::messages.pages.create.form.image'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('image', $all_images, '', ['class' => 'form-control']) !!}
                
                @if (!$using_attachments)
                    <p class="small help-block">Pick one from the <a href="/admin/media" target="_blank">Media Library</a>.</p>
                @else 
                    <p class="small help-block">Pick one from the <a href="/admin/attachments" target="_blank">Attachments Library</a>.</p>
                @endif
            </div>
        </div>
    @endif
@endif
@endsection

@section('sidebar')
<!-- Template -->
<div class="row form-group">
    {!! Form::label('template', __('raven::messages.pages.create.form.template'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('template', $templates, 'default', ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Status -->
<div class="row form-group">
    {!! Form::label('status', __('raven::messages.pages.create.form.status'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('status', ['0' => 'Draft', '1' => 'Published'], 1, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Create Button -->
<div class="row form-group">
    <div class="col-md-12 margintop-sm">
        {!! Form::submit(__('raven::messages.pages.create.title'), ['class' => 'btn btn-primary btn-md btn-block']) !!}
    </div>
</div>
@endsection

@section('formend')
{!! Form::close() !!}
@endsection
