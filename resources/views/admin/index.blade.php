@extends('raven::admin.layouts.admin')

@section('body-class', 'admin')

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card card-default">
		  	<div class="card-heading">
		    	<h3 class="card-title">Some Title</h3>
		  	</div>
		  	<div class="card-body">
		  		Some content.
		  	</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card card-default">
		  	<div class="card-heading">
		    	<h3 class="card-title">Another Title</h3>
		  	</div>
		  	<div class="card-body">
		  		Some more content.
		  	</div>
		</div>
	</div>
	<div class="col-md-12 text-right">
		<a href="https://gitlab.com/crowandraven-public/raven-cms">Powered by Raven CMS v.{{ $version }}</a>
	</div>
</div>
@endsection
