@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.users.create.title'))

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/users', 'role' => 'form', 'method' => 'post']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.users.create.title') }}</h1>
  	
<!-- Name -->
<div class="row form-group">
    {!! Form::label('name', __('raven::messages.users.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Email -->
<div class="row form-group">
    {!! Form::label('email', __('raven::messages.users.create.form.email'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <p class="small help-block">{{ __('raven::messages.users.create.form.email_help') }}</p>
    </div>
</div>

<!-- Password -->
<div class="row form-group">
    {!! Form::label('password', __('raven::messages.users.create.form.password'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::text('password', null, ['class' => 'form-control', 'required' => 'required']) !!}
        <p class="small help-block">{{ __('raven::messages.users.create.form.password_help') }}</p>
    </div>
</div>

<!-- Bio -->
@if (config('raven-cms.show.users.bio'))
    <div class="row form-group">
        {!! Form::label('bio', __('raven::messages.users.create.form.bio'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::textarea('bio', '', null, ['class' => 'form-control', 'rows' => '5']) !!}
            <p class="small help-block">{{ __('raven::messages.users.create.form.bio_help') }}</p>
        </div>
    </div>
@endif

<!-- Roles -->
<div class="row">
    {!! Form::label('roles', __('raven::messages.users.create.form.roles'), ['class' => 'col-md-12 control-label']) !!}
</div>
<div class="input_role_fields_wrap">
</div>
<button class="btn btn-default add_role_field_button marginbottom">{{ __('raven::messages.users.create.form.add_role') }}</button>

<!-- Is Admin -->
<div class="row form-group">
    <div class="col-md-12">
        <label>
            {!! Form::checkbox('is_admin', 1) !!} {{ __('raven::messages.users.create.form.is_admin') }}
        </label>
    </div>
</div>

<!-- Create Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.users.create.title'), ['class' => 'btn btn-primary btn-md margintop-sm']) !!}
    </div>
</div>
@endsection

@section('sidebar')
@endsection

@section('formend')
{!! Form::close() !!}
@endsection

@section('fscripts')
<script>
    $(document).ready(function() {
        var max_role_fields         = 10; //maximum input boxes allowed
        var wrapper_role            = $(".input_role_fields_wrap"); //Fields wrapper
        var add_role_button         = $(".add_role_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_role_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_role_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper_role).append('<div class="row form-group"><div class="col-md-12">{!! Form::select('roles[]', $all_roles, null, ['class' => 'form-control']) !!}</div></div>'); //add input box
            }
        });
        
        $(wrapper_role).on("click",".remove_role_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });
</script>
@endsection
