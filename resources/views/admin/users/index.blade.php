@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.users.index.title'))

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h1>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('raven::messages.users.index.title') }}
            @endif
            <a href="/admin/users/create" class="btn btn-primary">{{ __('raven::messages.users.index.add_new') }}</a>
        </h1>
    </div>
    <div class="col-md-6 text-right">
        <form class="inline form-inline" action="/admin/users" method="GET">
            <div class="form-group">
                <label class="sr-only" for="search">{{ __('raven::messages.users.index.search.label') }}</label>
                <input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.users.index.search.placeholder') }}">
            </div>
        </form>
    </div>
</div>
<!-- Users List -->
<div class="row">
    <div class="col-md-12 margintop">
        
        @if ($users)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.users.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.users.index.table.email') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.users.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($users as $user)
                        <tr>
                            <th scope="row">
                                <a href="/admin/users/{{ $user->id }}/edit">{{ $user->name }}</a>
                            </th>
                            <td>
                                {{ $user->email }}
                            </td>
                            <td>
                                {{ $user->updated_at->toDayDateTimeString() }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $user->id }}">
                                    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ __('raven::messages.users.index.table.delete') }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-sm-12">
        {{ $users->appends(Request::except('page'))->links() }}
    </div>
</div>

@foreach ($users as $user)
    <!-- Delete Modal -->
    <div class="modal fade" id="delete-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.users.index.table.delete') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.users.partials.delete')
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('fscripts')
@endsection
