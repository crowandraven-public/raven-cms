@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.media.index.title'))

@section('styles')
<style>
.image{
    position:relative;
    overflow:hidden;
    padding-bottom:76%;
}
.image img{
    position:absolute;
}
</style>
@endsection

@section('scripts')
@endsection

@section('body-class', 'admin files')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h1>{{ __('raven::messages.media.index.title') }}</h1>
    </div>
    <div class="col-md-6 text-right">
        <a href="/admin/media/create" class="btn btn-primary" data-toggle="modal" data-target="#addMedia">{{ __('raven::messages.media.index.add_new') }}</a>
    </div>
</div>
<!-- Media List -->
<div class="row margintop">

    @foreach ($media as $file)

        @if (isset($file))
            <div class="col-md-4 marginbottom text-center" style="height:375px;">
                <div class="media-entry">
                    @if ($file->type === 'application/pdf')
                        <div style="height:300px;"><i class="far fa-file-pdf fa-4x placeholder-icon"></i></div>
                    @elseif ($file->type === 'image/jpeg' || $file->type === 'image/png' || $file->type === 'image/gif')
                        <div class="images">
                            <a href="#" data-toggle="modal" data-target="#view-{{ $file->id }}">
                                <img src="/img/{{ $file->file }}?w=380&h=180&fit=crop&q=70" class="img img-responsive full-width" alt="{{ $file->title }}">
                            </a>
                        </div>
                    @elseif ($file->type === 'video/mp4')
                        <i class="fas fa-video fa-4x placeholder-icon"></i>
                    @else
                        <i class="far fa-file fa-4x placeholder-icon"></i>
                    @endif

                    <div class="title">
                        <a href="#" data-toggle="modal" data-target="#view-{{ $file->id }}">{{ $file->title }}</a>
                    </div>
                    <div class="link small">
                        @if ($file->type === 'application/pdf')
                            <a href="/storage/uploads/{{ $file->file }}">/storage/uploads/{{ $file->file }}</a>
                        @elseif ($file->type === 'image/jpeg' || $file->type === 'image/png' || $file->type === 'image/gif')
                            <a href="/img/{{ $file->file }}">/img/{{ $file->file }}</a>
                        @endif
                    </div>
                    
                    <button class="btn btn-default" data-toggle="modal" data-target="#view-{{ $file->id }}">
                        <i class="fas fa-info-circle fa-fw" data-toggle="tooltip" data-placement="top" title="View"></i>
                    </button>
                    
                    <button class="btn btn-default" data-toggle="modal" data-target="#edit-{{ $file->id }}">
                        <i class="fas fa-pencil-alt fa-fw" data-toggle="tooltip" data-placement="top" title="Edit"></i>
                    </button>
                    <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $file->id }}">
                        <i class="fas fa-times fa-fw" data-toggle="tooltip" data-placement="top" title="Delete"></i>
                    </button>
                </div>
            </div>
        @else
            <div class="col-md-4 marginbottom text-center" style="height:300px;">
                {{ __('raven::messages.media.index.error') }}
            </div>
        @endif

    @endforeach

</div>
<div class="row">
    <div class="col-sm-12">
        {{ $media->appends(Request::except('page'))->links() }}
    </div>
</div>

<!-- Create Modal -->
<div class="modal fade" id="addMedia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.media.create.title') }}</h4>
            </div>
            <div class="modal-body">
                @include('raven::admin.media.partials.create')
            </div>
        </div>
    </div>
</div>

@foreach ($media as $file)
    <!-- View Modal -->
    <div class="modal fade" id="view-{{ $file->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-view-{{ $file->id }}">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel-view-{{ $file->id }}">{{ __('raven::messages.media.view.title') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.media.partials.view')
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="edit-{{ $file->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-edit-{{ $file->id }}">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel-edit-{{ $file->id }}">{{ __('raven::messages.media.edit.title') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.media.partials.edit')
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Modal -->
    <div class="modal fade" id="delete-{{ $file->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-delete-{{ $file->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel-delete-{{ $file->id }}">{{ __('raven::messages.media.delete.title') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.media.partials.delete')
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('fscripts')
@endsection
