{!! Form::open(['url' => 'admin/media/'.$file->id, 'role' => 'form', 'method' => 'put']) !!}
    <!-- Title -->
    <div class="row form-group">
        {!! Form::label('title-'.$file->id, __('raven::messages.media.edit.form.title'), ['class' => 'col-md-12 control-label required']) !!}
        <div class="col-md-12">
            {!! Form::text('title', $file->title, ['class' => 'form-control', 'required' => 'required','id' => 'title-'.$file->id]) !!}
        </div>
    </div>

    <!-- Update Button -->
    <div class="row form-group">
        <div class="col-md-12">
            {!! Form::submit(__('raven::messages.media.edit.title'), ['class' => 'btn btn-primary btn-md margintop-sm']) !!}
        </div>
    </div>
{!! Form::close() !!}
