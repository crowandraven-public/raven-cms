{!! Form::open(['url' => 'admin/media', 'role' => 'form', 'id' => 'fileForm', 'files' => true, 'method' => 'post']) !!}
    <!-- Title -->
    <div class="row form-group">
        {!! Form::label('title', __('raven::messages.media.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
        <div class="col-md-12">
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>

    <!-- File -->
    <div class="row form-group">
        {!! Form::label('file', __('raven::messages.media.create.form.file'), ['class' => 'col-md-12 control-label required']) !!}
        <div class="col-md-12">
            {!! Form::file('file', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Create Button -->
    <div class="row form-group">
        <div class="col-md-12">
            {!! Form::submit(__('raven::messages.media.create.title'), ['class' => 'btn btn-primary btn-md margintop-sm']) !!}
        </div>
    </div>
{!! Form::close() !!}
