@if (isset($file) && ($file->type === 'image/jpeg' || $file->type === 'image/png'))
	<?php
    list($width, $height, $type, $attr) = @getimagesize(env('APP_URL').Storage::url('public/uploads/'.$file->file));
    ?>

	<div class="row">
		<div class="col-md-7">
			<img src="{{ env('APP_URL').Storage::url('uploads/'.$file->file) }}" class="img-responsive" alt="{{ $file->title }}">
		</div>
		<div class="col-md-5">
			<h4>{{ __('raven::messages.media.view.subtitle') }}</h4>
			<strong>{{ __('raven::messages.media.view.file_title') }}:</strong> {{ $file->title }}<br>
			<strong>{{ __('raven::messages.media.view.file_url') }}:</strong> {{ Storage::url('uploads/'.$file->file) }} <br>
			<strong>{{ __('raven::messages.media.view.file_size') }}:</strong> {{ $width }}px x {{ $height }}px<br>
			<strong>{{ __('raven::messages.media.view.file_type') }}:</strong> {{ $file->type }}<br>
			<strong>{{ __('raven::messages.media.view.file_added') }}:</strong> {{ $file->created_at->format('M j, Y g:i:s a') }} <br>
			<strong>{{ __('raven::messages.media.view.file_updated') }}:</strong> {{ $file->updated_at->format('M j, Y g:i:s a') }}<br>
		</div>
	</div>
@endif