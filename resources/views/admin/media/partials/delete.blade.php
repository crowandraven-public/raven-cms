{!! Form::open(['url' => '/admin/media/'.$file->id, 'method' => 'delete']) !!}
    <div class="row form-group">
        {!! Form::label('title', __('raven::messages.media.delete.form.delete_message'), ['class' => 'col-sm-12 control-label required']) !!}
        <div class="col-sm-12">
            {!! Form::text('confirm_destroy', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'DESTROY']) !!}
            <p class="small help-block">{{ __('raven::messages.media.delete.form.delete_confirm') }}</p>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm-12">
            {!! Form::submit(__('raven::messages.media.delete.title'), ['class' => 'btn btn-danger btn-md margintop-sm']) !!}
        </div>
    </div>
{!! Form::close() !!}
