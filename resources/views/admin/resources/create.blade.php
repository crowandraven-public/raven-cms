@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.resources.create.title'))

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/resources', 'role' => 'form', 'method' => 'post']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.resources.create.title') }}</h1>
  	
<!-- Title -->
<div class="row form-group">
    {!! Form::label('name', __('raven::messages.resources.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('title-'.$locale, '', ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('title-'.config('raven-cms.fallback_locale'), '', ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- URL -->
<div class="row form-group">
    {!! Form::label('url_file', __('raven::messages.resources.create.form.local'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('url_file', null, ['class' => 'form-control']) !!}
        <p class="small help-block">{{ __('raven::messages.resources.create.form.local_help') }} Pick one from the <a href="/admin/media" target="_blank">Media Library</a>.</p>
    </div>
</div>

<!-- URL -->
<div class="row form-group">
    {!! Form::label('url_link', __('raven::messages.resources.create.form.remote'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('url_link', null, ['class' => 'form-control']) !!}
        <p class="small help-block">Optional. {{ __('raven::messages.resources.create.form.remote_help') }}</p>
    </div>
</div>
@endsection

@section('sidebar')
<!-- Create Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.resources.create.title'), ['class' => 'btn btn-primary btn-md btn-block margintop-sm']) !!}
    </div>
</div>
@endsection

@section('formend')
{!! Form::close() !!}
@endsection
