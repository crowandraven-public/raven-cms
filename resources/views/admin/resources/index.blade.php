@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.resources.index.title'))

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h1>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('raven::messages.resources.index.title') }}
            @endif
            <a href="/admin/resources/create" class="btn btn-primary">{{ __('raven::messages.resources.index.add_new') }}</a>
        </h1>
    </div>
    <div class="col-md-6 text-right">
        <form class="inline form-inline" action="/admin/resources" method="GET">
            <div class="form-group">
                <label class="sr-only" for="search">{{ __('raven::messages.resources.index.search.label') }}</label>
                <input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.resources.index.search.placeholder') }}">
            </div>
        </form>
    </div>
</div>
<!-- Resources List -->
<div class="row">
    <div class="col-md-12 margintop">
        
        @if ($resources)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.resources.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.resources.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col">

                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($resources as $resource)
                        <tr>
                            <th scope="row">
                                <a href="/admin/resources/{{ $resource->id }}/edit">{{ localize($resource->title) }}</a>
                            </th>
                            <td>
                                {{ $resource->updated_at->toDayDateTimeString() }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $resource->id }}">
                                    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ __('raven::messages.resources.index.table.delete') }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-sm-12">
        {{ $resources->appends(Request::except('page'))->links() }}
    </div>
</div>

@foreach ($resources as $resource)
    <!-- Delete Modal -->
    <div class="modal fade" id="delete-{{ $resource->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.resources.index.table.delete') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.resources.partials.delete')
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('fscripts')
@endsection
