@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.attachments.edit.title'))

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/attachments/'.$attachment->id, 'role' => 'form', 'method' => 'put', 'files' => true]) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.attachments.edit.title') }}</h1>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('name', __('raven::messages.attachments.edit.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('title-'.$locale, localize($attachment->title, $locale), ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('title-'.config('raven-cms.fallback_locale'), localize($attachment->title, config('raven-cms.fallback_locale')), ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

@if ($attachment->file_name)
    <div class="row form-group">
        <div class="col-md-12">
            <br>    
            <div class="media-entry">
                @include('raven::admin.attachments.partials.thumb', ['image_settings' => '?w=380&h=180&fit=crop&q=70'])
            </div>
        </div>
    </div>
@endif

@if ($attachment->file_name)
    <div class="row form-group">
        <div class="col-md-12">
            <strong>{{ __('raven::messages.attachments.view.file_title') }}:</strong> {{ localize($attachment->title) }}<br>
            
            @if ($attachment->file_type === 'image/jpeg' || $attachment->file_type === 'image/png' || $attachment->file_type === 'image/gif')
                <strong>{{ __('raven::messages.attachments.view.file_url') }}:</strong> {{ getenv('APP_URL') }}/img/{{ $attachment->file_name }} <br>
            @else 
                <strong>{{ __('raven::messages.attachments.view.file_url') }}:</strong> {{ getenv('APP_URL') }}{{ Storage::url('uploads/'.$attachment->file_name) }} <br>
            @endif
            
            <strong>{{ __('raven::messages.attachments.view.file_type') }}:</strong> {{ $attachment->file_type }}<br>
            <strong>{{ __('raven::messages.attachments.view.file_added') }}:</strong> {{ $attachment->created_at->format('M j, Y g:i:s a') }} <br>
            <strong>{{ __('raven::messages.attachments.view.file_updated') }}:</strong> {{ $attachment->updated_at->format('M j, Y g:i:s a') }}<br>
            @if ($attachment->file_type === 'image/jpeg' || $attachment->file_type === 'image/png' || $attachment->file_type === 'image/gif')
                <a href="/img/{{ $attachment->file_name }}" target="_blank">View the original image.</a>
            @else 
                <a href="{{ Storage::url('uploads/'.$attachment->file_name) }}" target="_blank">Download this file.</a>
            @endif
            <br>
            @if ($attachment->file_type === 'image/jpeg' || $attachment->file_type === 'image/png' || $attachment->file_type === 'image/gif')
                <strong style="color: black;">If you need to paste this url in a field, copy this:</strong> /img/{{ $attachment->file_name }}
            @else
                <strong style="color: black;">If you need to paste this url in a field, copy this:</strong> /storage/uploads/{{ $attachment->file_name }}
            @endif
            <hr>
        </div>
    </div>
@endif


<!-- File -->
<div class="row form-group">
    @if ($attachment->file_name)
        {!! Form::label('file', __('raven::messages.attachments.edit.form.replace_file'), ['class' => 'col-md-12 control-label']) !!}
    @else 
        {!! Form::label('file', __('raven::messages.attachments.create.form.file'), ['class' => 'col-md-12 control-label']) !!}
    @endif
    <div class="col-md-12">
        {!! Form::file('file', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- External URL -->
<div class="row form-group">
    {!! Form::label('external_url', __('raven::messages.attachments.edit.form.external_url'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('external_url', $attachment->external_url, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Description -->
<div class="row form-group">
    {!! Form::label('description', __('raven::messages.attachments.edit.form.description'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('description-'.$locale, '', localize($attachment->description, $locale), ['class' => 'form-control','rows' => '5']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('description-'.config('raven-cms.fallback_locale'), localize($attachment->description, config('raven-cms.fallback_locale')), ['class' => 'form-control','rows' => '5']) !!}
        </div>
    @endif
</div>

<!-- Caption -->
<div class="row form-group">
    {!! Form::label('caption', __('raven::messages.attachments.edit.form.caption'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('caption-'.$locale, '', localize($attachment->caption, $locale), ['class' => 'form-control','rows' => '5']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('caption-'.config('raven-cms.fallback_locale'), localize($attachment->caption, config('raven-cms.fallback_locale')), ['class' => 'form-control','rows' => '5']) !!}
        </div>
    @endif
</div>

<!-- Alternative Text -->
<div class="row form-group marginbottom">
    {!! Form::label('caption', __('raven::messages.attachments.edit.form.alt_text'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('alt_text-'.$locale, '', localize($attachment->alt_text, $locale), ['class' => 'form-control','rows' => '5']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('alt_text-'.config('raven-cms.fallback_locale'), localize($attachment->alt_text, config('raven-cms.fallback_locale')), ['class' => 'form-control','rows' => '5']) !!}
        </div>
    @endif
</div>

@endsection

@section('sidebar')
<!-- Update Button -->
<div class="row form-group">
    <div class="col-md-12">
        
        {!! Form::submit(__('raven::messages.attachments.edit.title'), ['class' => 'btn btn-primary btn-md btn-block margintop-sm']) !!}
    </div>
</div>
@endsection

@section('formend')
{!! Form::close() !!}
@endsection
