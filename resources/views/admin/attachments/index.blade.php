@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.attachments.index.title'))

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        @if (isset($title))
            <h1>{{ $title }}</h1>
        @else
            <h1>{{ __('raven::messages.attachments.index.title') }}</h1>
        @endif
        <a href="/admin/attachments/create" class="btn btn-primary">{{ __('raven::messages.attachments.index.add_new') }}</a>

    </div>
    <div class="col-md-6 text-right">
        <form class="inline form-inline" action="/admin/attachments" method="GET">
            <div class="form-group">
                <label class="sr-only" for="search">{{ __('raven::messages.attachments.index.search.label') }}</label>
                <input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.attachments.index.search.placeholder') }}">
            </div>
        </form>
    </div>
</div>
<!-- attachments List -->
<div class="row">
    <div class="col-md-12 margintop">
        
        @if ($attachments)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.attachments.index.table.thumb') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.attachments.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.attachments.index.table.file_type') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.attachments.index.table.updated') }}</strong>
                        </th>
                        <th scope="col">
                        </th>
                    </tr>
                <thead>

                <tbody>
                    @foreach ($attachments as $attachment)
                        <tr>

                            <!-- Thumb -->
                            <th scope="row">
                                @include('raven::admin.attachments.partials.thumb', ['image_settings' => '?w=50&h=50&fit=crop&q=50'])
                            </th>

                            <!-- Title -->
                            <td>
                                <a href="/admin/attachments/{{ $attachment->id }}/edit">{{ localize($attachment->title) }}</a>
                            </td>

                            <!-- Type -->
                            <td>
                                {{ $attachment->file_type }}
                            </td>

                            <!-- Last Updated -->
                            <td>
                                {{ $attachment->updated_at->toDayDateTimeString() }}
                            </td>

                            <!-- Edit/Delete Button -->
                            <td >
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $attachment->id }}">
                                    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ __('raven::messages.attachments.index.table.delete') }}"></i>
                                </button>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            {{ $attachments->appends(Request::except('page'))->links() }}
        </div>
        @endif
    </div>
</div>

@foreach ($attachments as $attachment)
    <!-- Delete Modal -->
    <div class="modal fade" id="delete-{{ $attachment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.attachments.index.table.delete') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.attachments.partials.delete')
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('fscripts')
@endsection
