
@if ($attachment->file_type === 'application/pdf')
    <div><i class="far fa-file-pdf fa-2x placeholder-icon"></i></div>
@elseif ($attachment->file_type === 'image/jpeg' || $attachment->file_type === 'image/png' || $attachment->file_type === 'image/gif')
    <div class="images">
        <img style="border: solid thin #d6d6d6;" src="/img/{{ $attachment->file_name }}{{ $image_settings }}" class="img img-responsive full-width" alt="{{ $attachment->title }}">
    </div>
@elseif (preg_match('/video/', $attachment->file_type))
    <i class="fas fa-video fa-2x placeholder-icon"></i>
@elseif (preg_match('/audio/', $attachment->file_type))
    <i class="fas fa-volume-up fa-2x placeholder-icon"></i>
@elseif (preg_match('/zip/', $attachment->file_type))
    <i class="far fa-file-archive fa-2x placeholder-icon"></i>
@elseif (preg_match('/xlsx/', $attachment->file_name))
    <i class="far fa-file-excel fa-2x placeholder-icon"></i>
@else
    <i class="far fa-file-alt fa-2x placeholder-icon"></i>
@endif