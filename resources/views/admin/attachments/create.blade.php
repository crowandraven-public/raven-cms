@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.attachments.create.title'))

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/attachments', 'role' => 'form', 'method' => 'post', 'files' => true]) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.attachments.create.title') }}</h1>
  	
<!-- Title -->
<div class="row form-group">
    {!! Form::label('name', __('raven::messages.attachments.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('title-'.$locale, '', ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('title-'.config('raven-cms.fallback_locale'), '', ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- File -->
<div class="row form-group">
    {!! Form::label('file', __('raven::messages.attachments.create.form.file'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::file('file', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- External URL -->
<div class="row form-group">
    {!! Form::label('external_url', __('raven::messages.attachments.create.form.external_url'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('external_url', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Description -->
<div class="row form-group">
    {!! Form::label('description', __('raven::messages.attachments.create.form.description'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('description-'.$locale, '', null, ['class' => 'form-control','rows' => '5']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('description-'.config('raven-cms.fallback_locale'), '', null, ['class' => 'form-control','rows' => '5']) !!}
        </div>
    @endif
</div>


<!-- Caption -->
<div class="row form-group">
    {!! Form::label('caption', __('raven::messages.attachments.create.form.caption'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('caption-'.$locale, '', null, ['class' => 'form-control','rows' => '5']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('caption-'.config('raven-cms.fallback_locale'), '', null, ['class' => 'form-control','rows' => '5']) !!}
        </div>
    @endif
</div>

<!-- Alternative Text -->
<div class="row form-group marginbottom">
    {!! Form::label('alt_text', __('raven::messages.attachments.create.form.alt_text'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('alt_text-'.$locale, '', null, ['class' => 'form-control','rows' => '5']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('alt_text-'.config('raven-cms.fallback_locale'), '', null, ['class' => 'form-control','rows' => '5']) !!}
        </div>
    @endif
</div>

@endsection

@section('sidebar')
<!-- Create Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.attachments.create.title'), ['class' => 'btn btn-primary btn-md btn-block margintop-sm']) !!}
    </div>
</div>
@endsection

@section('formend')
{!! Form::close() !!}
@endsection
