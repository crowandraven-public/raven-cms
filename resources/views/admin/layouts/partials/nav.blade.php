<div class="spark-settings-tabs">
    <ul class="nav spark-settings-stacked-tabs" role="tablist">
        @foreach (config('raven-cms.nav') as $item)
            <li {{ (Request::is($item['path']) || Request::is($item['path'].'/create') || Request::is($item['path'].'/edit/*')) ? ' class=active' : '' }} role="presentation"><a href="/{{ $item['path'] }}"><i class="fas {{ $item['icon'] }} fa-fw"></i> {{ $item['name'] }}</a></li>
        @endforeach
    </ul>
</div>
