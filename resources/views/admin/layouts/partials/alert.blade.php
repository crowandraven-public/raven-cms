@if (isset($request) && $request->session()->get('flash_message'))
    <div class="row">
        <div class="col-xs-12">
            <div class="bs-component">
                <div class="alert alert-flash alert-{{ $request->session()->get('flash_type') }}">
                    <p>
                        <strong>{{ $request->session()->get('flash_title') }}</strong>
                        {{ $request->session()->get('flash_message') }}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endif

@if ($errors->count())
    <div class="row">
        <div class="col-xs-12">
            <div class="bs-component">
                <div class="alert alert-flash alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif