<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title', config('app.name'))</title>
    <link rel="canonical" href="{{ Request::url() }}" />

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,400" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.1/js/all.js"></script>


    <!-- CSS -->
    <link rel="stylesheet" href="/raven/css/admin.css">

    <!-- Injected Styles -->
    @yield('styles', '')

    <!-- Scripts -->
    @yield('scripts', '')
</head>
<body class="@yield('body-class')">
    @include('raven::admin.nav.admin')

    <!-- Main Content -->
    <div class="flex-fill flex-center">
        
        @yield('formstart')
        
        <div class="container-fluid main">
            @if ($__env->yieldContent('sidebar'))
                <div class="row flex">
                    <div class="col-md-2 admin-nav">
                        @include('raven::admin.layouts.partials.nav')
                    </div>
                    <div class="col-md-7 admin-main">
                        @include('raven::admin.layouts.partials.alert')
                        @yield('content')
                    </div>
                    <div class="col-md-3 admin-right">
                        @yield('sidebar')
                    </div>
                </div>
            @else
                <div class="row flex">
                    <div class="col-md-2 admin-nav">
                        @include('raven::admin.layouts.partials.nav')
                    </div>
                    <div class="col-md-10 admin-main">
                        @include('raven::admin.layouts.partials.alert')
                        @yield('content')
                    </div>
                </div>
            @endif
        </div>

        @yield('formend')
    </div>

    <!-- JavaScript -->

    <script src="/raven/js/admin.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

    @yield('fscripts')

    <script>
        $('div.alert').delay(7000).slideUp(300);
    </script>
</body>
</html>
