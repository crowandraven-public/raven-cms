@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.tags.index.title'))

@section('body-class', 'admin')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h1>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('raven::messages.tags.index.title') }}
            @endif
            <a href="/admin/tags/create" class="btn btn-primary">{{ __('raven::messages.tags.index.add_new') }}</a>
        </h1>
	</div>
	<div class="col-md-6 text-right">
        <form class="inline form-inline" action="/admin/tags" method="GET">
 			<div class="form-group">
 				<label class="sr-only" for="search">{{ __('raven::messages.tags.index.search.label') }}</label>
 				<input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.tags.index.search.placeholder') }}">
 			</div>
 		</form>
	</div>
</div>

<div class="row">
	<div class="col-md-12 margintop-sm">
		@if ($tags)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.tags.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.tags.index.table.slug') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.tags.index.table.type') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.tags.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($tags as $tag)
				        <tr>
                            <th scope="row">
                                <a href="/admin/tags/{{ $tag->id }}/edit">{{ localize($tag->name) }}</a>
                            </th>
                            <td>
                                {{ localize($tag->slug) }}
                            </td>
                            <td>
                                {{ $tag->type or "null" }}
                            </td>
                            <td>
                                {{ $tag->updated_at->toDayDateTimeString() }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $tag->id }}">
                                    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ __('raven::messages.tags.index.table.delete') }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
		@endif
		<div class="pagination">
			{{ $tags->appends(Request::except('page'))->links() }}
		</div>
	</div>
</div>

<!-- Delete Modal -->
@if (isset($tags))
    @foreach ($tags as $tag)
        <!-- Delete Modal -->
        <div class="modal fade" id="delete-{{ $tag->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.tags.index.table.delete') }}</h4>
                    </div>
                    <div class="modal-body">
                        @include('raven::admin.tags.partials.delete')
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
@endsection
