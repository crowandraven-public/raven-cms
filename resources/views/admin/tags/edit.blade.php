@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.tags.edit.title'))

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/tags/'.$tag->id, 'role' => 'form', 'id' => 'tagForm', 'method' => 'put']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.tags.edit.title') }}</h1>

<!-- Type -->
<div class="row form-group">
    {!! Form::label('type', __('raven::messages.tags.edit.form.type'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('type', config('raven-cms.tag-category'), $tag->type, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('name', __('raven::messages.tags.edit.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('name-'.$locale, localize($tag->name, $locale), ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('name-'.config('raven-cms.fallback_locale'), localize($tag->name, config('raven-cms.fallback_locale')), ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- Slug -->
<div class="row form-group">
    {!! Form::label('slug', __('raven::messages.tags.edit.form.slug'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('slug-'.$locale, localize($tag->name, $locale), ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('slug-'.config('raven-cms.fallback_locale'), localize($tag->name, config('raven-cms.fallback_locale')), ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- Description -->
<div class="row form-group">
    {!! Form::label('description', __('raven::messages.tags.edit.form.description'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('description-'.$locale, localize($tag->description, $locale), ['class' => 'form-control']) !!}
                        <p class="small help-block">{{ $locale }} - Optional.</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('description-'.config('raven-cms.fallback_locale'), localize($tag->description, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
            <p class="small help-block">Optional.</p>
        </div>
    @endif
</div>

<!-- Synonyms -->
<div class="row form-group">
    {!! Form::label('synonyms', __('raven::messages.tags.edit.form.synonyms'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('synonyms', $tag->synonyms, ['class' => 'form-control']) !!}
        <p class="small help-block">{{ __('raven::messages.tags.edit.form.synonyms_help') }}</p>
    </div>
</div>

<!-- Submit Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.tags.edit.title'), ['class' => 'btn btn-primary margintop-sm']) !!}
    </div>
</div>
@endsection


@section('sidebar')
@endsection

@section('formend')
{!! Form::close() !!}
@endsection

@section('fscripts')
@endsection
