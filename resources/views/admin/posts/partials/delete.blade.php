{!! Form::open(['url' => '/admin/posts/'.$post->id, 'method' => 'delete']) !!}
    <div class="row form-group">
        {!! Form::label('title', __('raven::messages.posts.index.table.delete_message'), ['class' => 'col-sm-12 control-label required']) !!}
        <div class="col-sm-12">
            {!! Form::text('confirm_destroy', null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=>'DESTROY']) !!}
            <p class="small help-block">{{ __('raven::messages.posts.index.table.delete_confirm') }}</p>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm-12">
            {!! Form::submit(__('raven::messages.posts.index.table.delete'), ['class' => 'btn btn-danger btn-md margintop-sm']) !!}
        </div>
    </div>
{!! Form::close() !!}
