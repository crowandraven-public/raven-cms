@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.posts.edit.title'))

@section('scripts')
<script src="https://cdn.tiny.cloud/1/va0cczdbx8gc8utla2gv66pu2ip8nq2yanvgzp9sq0g1bvsj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({
    selector:'textarea',
    height: 300,
    theme: 'silver',
    plugins: [
        'advlist autolink lists link image charmap hr anchor',
        'searchreplace wordcount visualblocks visualchars code',
        'media nonbreaking save table contextmenu directionality',
        'paste imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
    menubar: 'tools',
    image_advtab: true,
});


// development (here for testing)
<?php if (App::environment() == 'local') : ?>
    <?php
    $related_items =  \CrowAndRaven\CMS\Models\Media::getAllImages()->toArray();
    $fixed = [];
    foreach ($related_items as $file => $title) {
        $fixed[] = ['title' => $title, 'image' => '/img/'.$file, 'id' => $file];
    } ?>

    var relatedItems = {!! json_encode($fixed) !!};
<?php endif; ?>
</script>
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/posts/'.$post->id, 'role' => 'form', 'method' => 'put']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.posts.edit.title') }}</h1>

<div class="row">
    <div class="col-md-12 margintop-xs marginbottom-xs">
        <a href="{{ env('APP_URL') }}/{{ CrowAndRaven\CMS\Models\Tag::getSlugById($post->type) }}/{{ localize($post->slug) }}">{{ env('APP_URL') }}/{{ CrowAndRaven\CMS\Models\Tag::getSlugById($post->type) }}/{{ localize($post->slug) }}</a>
    </div>
</div>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('title', __('raven::messages.posts.edit.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('title-'.$locale, localize($post->title, $locale), ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('title-'.config('raven-cms.fallback_locale'), localize($post->title, config('raven-cms.fallback_locale')), ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- Slug -->
<div class="row form-group">
    {!! Form::label('slug', __('raven::messages.posts.edit.form.slug'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('slug-'.$locale, localize($post->slug, $locale), ['class' => 'form-control']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('slug-'.config('raven-cms.fallback_locale'), localize($post->slug, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
        </div>
    @endif
</div>

<!-- SubTitle -->
@if (config('raven-cms.show.posts.subtitle'))
    <div class="row form-group">
        {!! Form::label('subtitle', __('raven::messages.posts.edit.form.subtitle'), ['class' => 'col-md-12 control-label']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::text('subtitle-'.$locale, localize($post->subtitle, $locale), ['class' => 'form-control']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::text('subtitle-'.config('raven-cms.fallback_locale'), localize($post->subtitle, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
            </div>
        @endif
    </div>
@endif

<!-- Content -->
<div class="row form-group">
    {!! Form::label('content', __('raven::messages.posts.edit.form.content'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::textarea('content-'.$locale, localize($post->content, $locale), ['class' => 'form-control','rows' => '5']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::textarea('content-'.config('raven-cms.fallback_locale'), localize($post->content, config('raven-cms.fallback_locale')), ['class' => 'form-control','rows' => '5']) !!}
        </div>
    @endif
</div>

<div class="row">
    <div class="col-md-12">
        <h3>{{ __('raven::messages.posts.edit.form.meta') }}</h3>
    </div>
</div>

<!-- Excerpt -->
@if (config('raven-cms.show.posts.excerpt'))
    <div class="row form-group">
        {!! Form::label('excerpt', __('raven::messages.posts.edit.form.excerpt'), ['class' => 'col-md-12 control-label']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::text('excerpt-'.$locale, localize($post->excerpt, $locale), ['class' => 'form-control']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::text('excerpt-'.config('raven-cms.fallback_locale'), localize($post->excerpt, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
            </div>
        @endif
    </div>
@endif

<!-- Track Tags -->
@if (config('raven-cms.show.posts.track_tags'))
    @if ($all_tracks->count() > 0)
        <div class="row form-group">
            {!! Form::label('track_tags', __('raven::messages.posts.edit.form.tracks'), ['class' => 'col-md-12 control-label required']) !!}
            
            <div class="tags-container">
                @foreach ($all_tracks as $key => $value)
                    <span class="button-checkbox">
                        <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                        <input type="checkbox" class="hidden" name="track_tags[]" value="{{ $key }}" {{ (in_array($key, $post_tags->toArray()) ? 'checked' : '') }} />
                    </span>
                @endforeach
            </div>
        </div>
    @endif
@endif

<!-- Topic Tags -->
@if (config('raven-cms.show.posts.topic_tags'))
    @if ($all_topics->count() > 0)
        <div class="row form-group">
            {!! Form::label('topic_tags', __('raven::messages.posts.edit.form.topics'), ['class' => 'col-md-12 control-label']) !!}
            
            <div class="tags-container">
                @foreach ($all_topics as $key => $value)
                    <span class="button-checkbox">
                        <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                        <input type="checkbox" class="hidden" name="topic_tags[]" value="{{ $key }}" {{ (in_array($key, $post_tags->toArray()) ? 'checked' : '') }} />
                    </span>
                @endforeach
            </div>
        </div>
    @endif
@endif

<!-- Author -->
@if (config('raven-cms.show.posts.author'))
    @if ($all_authors->count() > 1)
        <div class="row form-group">
            {!! Form::label('author', __('raven::messages.posts.edit.form.author'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('author', $all_authors, $post->author, ['class' => 'form-control']) !!}
            </div>
        </div>
    @endif
@endif

<!-- Resources -->
@if (config('raven-cms.show.posts.resources'))
    @if ($all_resources->count() > 1)
        <div class="row">
            {!! Form::label('resources', __('raven::messages.posts.edit.form.resources'), ['class' => 'col-md-12 control-label']) !!}
        </div>
        <div class="input_resource_fields_wrap">
            @if ($resources)
                @foreach ($resources as $resource)
                    <div class="row form-group">
                        <div class="col-md-12">
                            {!! Form::select('resources[]', $all_resources, $resource, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <button class="btn btn-default add_resource_field_button marginbottom">{{ __('raven::messages.posts.edit.form.add_resource') }}</button>
    @endif
@endif

@if (config('raven-cms.show.posts.attachments'))
    <div class="row">
        <div class="col-md-12">
            <div id="raven-fields-ui">
                <add-by-search-editor
                    field-name="attachments"
                    :use-file-uploader="true"
                    image-path-prefix="/img"
                    field-label="Search and add attachments."
                    :initial-value="{{ $post->getPreppedAttachments() }}"
                    ajax-url="/admin/ravenfields/collection/attachment"
                    search-fields="title"
                    display-value-left="title"
                    lang="en"
                    display-value-right="file_type"
                    image-field-name="file_name">
                </add-by-search-editor>
            </div>
        </div>
    </div>
@endif

<!-- Image -->
@if (config('raven-cms.show.posts.image'))
    @if ($all_images->count() > 1)
        <div class="row form-group">
            {!! Form::label('image', __('raven::messages.posts.create.form.image'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('image', $all_images, $post->image, ['class' => 'form-control']) !!}
                
                @if (!$using_attachments)
                    <p class="small help-block">Pick one from the <a href="/admin/media" target="_blank">Media Library</a>.</p>
                @else
                    <p class="small help-block">Pick one from the <a href="/admin/attachments" target="_blank">Attachments Library</a>.</p>
                @endif
            </div>
        </div>
    @endif
@endif


<!-- File -->
@if (config('raven-cms.show.posts.file'))
    @if ($all_files->count() > 1)
        <div class="row form-group">
            {!! Form::label('file', __('raven::messages.posts.create.form.file'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('file', $all_files, $post->file, ['class' => 'form-control']) !!}
                <p class="small help-block">Pick one from the <a href="/admin/media" target="_blank">Media Library</a>.</p>
            </div>
        </div>
    @endif
@endif

@endsection

@section('sidebar')
<!-- Type -->
@if (config('raven-cms.show.posts.type'))
    @if ($all_types->count() > 0)
        <div class="row form-group margintop">
            {!! Form::label('type', __('raven::messages.posts.edit.form.type'), ['class' => 'col-md-12 control-label required']) !!}
            <div class="col-md-12">
                {!! Form::select('type', $all_types, $post->type, ['class' => 'form-control']) !!}
            </div>
        </div>
    @endif
@endif

<!-- Status -->
<div class="row form-group">
    {!! Form::label('status', __('raven::messages.posts.edit.form.status'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('status', ['0' => 'Draft', '1' => 'Published'], $post->status, ['class' => 'form-control']) !!}
        <p class="small help-block">{{ __('raven::messages.posts.edit.form.status_help') }}</p>
    </div>
</div>

<!-- Published At -->
<div class="row form-group">
    {!! Form::label('published_at', __('raven::messages.posts.edit.form.published'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('published_at', $post->published_at, ['class' => 'form-control', 'placeholder' => '']) !!}
        <p class="small help-block">{{ __('raven::messages.posts.edit.form.published_help') }}</p>
    </div>
</div>

<!-- Is Featured -->
@if (config('raven-cms.show.posts.is_featured'))
    <div class="row form-group">
        <div class="col-md-12">
            <label>
                <input type="checkbox" name="is_featured" value="1" {{ (($post->is_featured == 1) ? 'checked' : '') }} /> {{ __('raven::messages.posts.edit.form.is_featured') }}
            </label>
        </div>
    </div>
@endif

<!-- Update Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.posts.edit.title'), ['class' => 'btn btn-primary margintop-sm btn-block']) !!}
    </div>
</div>
@endsection

@section('formend')
{!! Form::close() !!}
@endsection

@section('fscripts')
<script>
    $(document).ready(function() {
        var max_resource_fields      = 10; //maximum input boxes allowed
        var wrapper_resource         = $(".input_resource_fields_wrap"); //Fields wrapper
        var add_resource_button      = $(".add_resource_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_resource_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_resource_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper_resource).append('<div class="row form-group"><div class="col-md-12">{!! Form::select('resources[]', $all_resources, null, ['class' => 'form-control']) !!}</div></div>'); //add input box
            }
        });
        
        $(wrapper_resource).on("click",".remove_resource_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

    // limit characters for certain fields
    function limitText(limitField, limitCount, limitNum) {
        if (limitField.value.length > limitNum) {
            limitField.value = limitField.value.substring(0, limitNum);
        } else {
            limitCount.value = limitNum - limitField.value.length;
        }
    }

    // checkbox buttons for tags
    $(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
</script>
@endsection
