@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.posts.index.title'))

@section('body-class', 'admin')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h1>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('raven::messages.posts.index.title') }}
            @endif
            <a href="/admin/posts/create" class="btn btn-primary">{{ __('raven::messages.posts.index.add_new') }}</a>
        </h1>
	</div>
	<div class="col-md-6 text-right">
		<form class="inline form-inline" action="/admin/posts" method="GET">
 			<div class="form-group">
 				<label class="sr-only" for="search">{{ __('raven::messages.posts.index.search.label') }}</label>
 				<input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.posts.index.search.placeholder') }}">
 			</div>
 		</form>
	</div>
</div>

<div class="row">
	<div class="col-md-12 margintop-sm">

    	@if ($posts)
            <table class="table table-hover">
                <thead>
                    <tr>
                        @if (config('raven-cms.show.posts.is_featured'))
                            <th width="50" scope="col">
                                <strong>{{ __('raven::messages.posts.index.table.featured') }}</strong>
                            </th>
                        @endif
                        <th scope="col">
                            <strong>{{ __('raven::messages.posts.index.table.title') }}</strong>
                        </th>
                        @if (config('raven-cms.show.posts.type'))
                            <th scope="col">
                                <strong>{{ __('raven::messages.posts.index.table.type') }}</strong>
                            </th>
                        @endif
                        <th scope="col">
                            <strong>{{ __('raven::messages.posts.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($posts as $post)
				        <tr>
                            @if (config('raven-cms.show.posts.is_featured'))
                                <th scope="row">
                                    @if ($post->is_featured)
                                        <i class="fas fa-check"></i>
                                    @endif
                                </th>
                            @endif
                            <td>
                                <a href="/{{ CrowAndRaven\CMS\Models\Tag::getSlugById($post->type) }}/{{ localize($post->slug) }}"><i class="fas fa-external-link-alt fa-fw"></i></a> <a href="/admin/posts/{{ $post->id }}/edit">{{ localize($post->title) }}</a>
                            </td>
                            @if (config('raven-cms.show.posts.type'))
                                <td>
                                    {{ CrowAndRaven\CMS\Models\Tag::getNameById($post->type) }}
                                </td>
                            @endif
                            <td>
                                {{ $post->updated_at->toDayDateTimeString() }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $post->id }}">
                                    <i class="fas fa-times" data-toggle="tooltip" data-placement="top" title="{{ __('raven::messages.posts.index.table.delete') }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
		@endif

		<div class="pagination">
			{{ $posts->appends(Request::except('page'))->links() }}
		</div>

	</div>
</div>

<!-- Delete Modal -->
@if (isset($posts))
    @foreach ($posts as $post)
        <!-- Delete Modal -->
        <div class="modal fade" id="delete-{{ $post->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.posts.index.table.delete') }}</h4>
                    </div>
                    <div class="modal-body">
                        @include('raven::admin.posts.partials.delete')
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
@endsection
