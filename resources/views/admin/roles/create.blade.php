@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.roles.create.title'))

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/roles', 'role' => 'form', 'id' => 'roleForm', 'method' => 'post']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.roles.create.title') }}</h1>

<!-- Name -->
<div class="row form-group">
    {!! Form::label('name', __('raven::messages.roles.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('name-'.$locale, '', ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('name-'.config('raven-cms.fallback_locale'), '', ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<div class="row form-group">
    {!! Form::label('slug', __('raven::messages.roles.create.form.slug'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::text('slug', '', ['class' => 'form-control', 'placeholder' => '']) !!}
        <p class="small help-block">{{ __('raven::messages.roles.create.form.slug_help') }}</p>
    </div>
</div>

<!-- Description -->
<div class="row form-group">
    {!! Form::label('description', __('raven::messages.roles.create.form.description'), ['class' => 'col-md-12 control-label']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('description-'.$locale, '', ['class' => 'form-control']) !!}
                        <p class="small help-block">{{ $locale }} - Optional.</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('description-'.config('raven-cms.fallback_locale'), '', ['class' => 'form-control']) !!}
            <p class="small help-block">Optional.</p>
        </div>
    @endif
</div>

<!-- Submit Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.roles.create.title'), ['class' => 'btn btn-primary btn-md']) !!}
    </div>
</div>
@endsection

@section('sidebar')
@endsection

@section('formend')
{!! Form::close() !!}
@endsection

@section('fscripts')
@endsection
