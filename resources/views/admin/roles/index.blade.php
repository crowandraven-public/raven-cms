@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.roles.index.title'))

@section('body-class', 'admin')

@section('content')
<div class="row">
	<div class="col-md-12">
		<h1>
            {{ __('raven::messages.roles.index.title') }}
            <a href="/admin/roles/create" class="btn btn-primary">{{ __('raven::messages.roles.index.add_new') }}</a>
        </h1>
	</div>
</div>

<div class="row">
	<div class="col-md-12 margintop-sm">
		@if ($roles)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.roles.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.roles.index.table.slug') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.roles.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col">
                        </th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($roles as $role)
                        <tr>
                            <th scope="row">
                                <a href="/admin/roles/{{ $role->id }}/edit">{{ localize($role->name) }}</a>
                            </th>
                            <td>
                                {{ $role->slug }}
                            </td>
                            <td>
                                {{ $role->updated_at->toDayDateTimeString() }}
                            </td>
                            <td>
                                @if (($role->slug != 'admin') && ($role->slug != 'content'))
                                    <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $role->id }}">
                                        <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="{{ localize($role->name) }}"></i>
                                    </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
		@endif

		<div class="pagination">
			{{ $roles->appends(Request::except('page'))->links() }}
		</div>
	</div>
</div>

<!-- Delete Modal -->
@if (isset($roles))
    @foreach ($roles as $role)
        <!-- Delete Modal -->
        <div class="modal fade" id="delete-{{ $role->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.roles.index.table.delete') }}</h4>
                    </div>
                    <div class="modal-body">
                        @include('raven::admin.roles.partials.delete')
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
@endsection
