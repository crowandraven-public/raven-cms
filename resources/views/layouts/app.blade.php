<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', env('APP_NAME'))</title>
    <link rel="canonical" href="{{ Request::url() }}" />
    <meta name="description" content="@yield('description')">

    <!-- Open Graph -->
    <meta property="og:site_name" content="{{ env('APP_NAME') }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::url() }}" />
    <meta property="fb:app_id" content="" />
    <meta property="og:title" content="{{ env('APP_NAME') }}: @yield('title')" />
    <meta property="og:description" content="@yield('description')" />
    <meta property="og:image" content="@yield('image')" />

    <!-- Twitter Social -->
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:description" content="@yield('description')"/>
    <meta name="twitter:title" content="@yield('title')"/>
    <meta name="twitter:site" content="{{ '@'.Config::get('site.social.twitter') }}"/>
    <meta name="twitter:domain" content="{{ env('APP_NAME') }}"/>
    <meta name="twitter:image" content="@yield('image')"/>
    <meta name="twitter:creator" content="{{ '@'.Config::get('site.social.twitter') }}"/>
    <meta itemprop="image" content="@yield('image')">
    
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#b95f32">
    <meta name="apple-mobile-web-app-title" content="{{ env('APP_NAME') }}">
    <meta name="application-name" content="{{ env('APP_NAME') }}">
    <meta name="theme-color" content="#ffffff">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Injected Styles -->
    @yield('styles', '')

    <script defer src="https://use.fontawesome.com/releases/v5.0.1/js/all.js"></script>
    <!-- Scripts -->
    @yield('scripts', '')
</head>
<body class="@yield('body-class')">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Show admin links for content -->
                    @if (Auth::check() && Auth::user()->is_admin)
                        @if (isset($page))
                            <li>
                                <a href="/admin/pages/{{ $page->id }}/edit"><i class="fa fa-pencil fa-fw pointer"></i></a>
                            </li>
                        @elseif (isset($post))
                            <li>
                                <a href="/admin/posts/{{ $post->id }}/edit"><i class="fa fa-pencil fa-fw pointer"></i></a>
                            </li>
                        @endif
                    @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <!-- Show Content Admin link -->
                                    @if (Auth::check() && Auth::user()->is_admin)
                                        <li>
                                            <a href="/admin">
                                                <i class="fa fa-fw fa-btn fas-cog"></i>Content Admin
                                            </a>
                                        </li>
                                    @endif

                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/raven/js/public.js"></script>

    <!-- Footer Scripts -->
    @yield('fscripts')
</body>
</html>
