@extends('raven::layouts.email')

@section('subject', trans('raven::messages.posts.contact.email.subject'))

@section('content')
<p>
	Regarding the {{ $data['post'] }} post, <a href="mailto:{{ $data['from_email'] }}" style="color:#333;">{{ $data['from_name'] }}</a> asked...
</p>
<hr>
<p>
	{{ $data['content'] }}
</p>
<hr>
<p>
	To answer this question, simply reply to this email.
</p>
@endsection

@section('action')
@endsection