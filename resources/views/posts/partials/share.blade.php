<span class="fa-2x">
    <a href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}&t={{ localize($post->title) }}" target="_blank" title="Share on Facebook" class="plain"><i class="fab fa-facebook-square fa-fw"></i></a>

    <a href="https://twitter.com/intent/tweet?source={{ Request::url() }}&text={{ localize($post->title) }}:Evenpulse&via={{ Config::get('site.social.twitter') }}" target="_blank" title="Tweet" class="plain"><i class="fab fa-twitter-square fa-fw"></i></a>

    <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{ Request::url() }}&amp;title={{ localize($post->title) }}&amp;summary={{ localize($post->excerpt) }}" target="_blank" class="plain"><i class="fab fa-linkedin fa-fw"></i></a>
</span>
