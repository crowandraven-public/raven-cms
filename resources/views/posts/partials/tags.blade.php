@if($post->tags)
    @foreach($post->tags->where('type', 'topic') as $tag)
        <a href="/{{ CrowAndRaven\CMS\Models\Tag::getSlugById($post->type) }}#{{ localize($tag->slug) }}">{{ ucwords(localize($tag->name)) }}</a>{{ !$loop->last ? ',' : '' }}
    @endforeach
    |
@endif