@if (isset($author->bio))
	<div class="marginbottom">
        <h3 class="sidebar section-title">Your Coach</h3>
        <p>
            <img src="{{ $author->photo_url }}" class="coach-thumbnail" alt="{{ $author->name }}">
            <strong>{{ $author->name }}</strong> {{ $author->bio }}
        </p>
        
        @if(is_subscribed_premium(Auth::user()))
            <a href="#" class="btn btn-md btn-primary btn-block" data-toggle="modal" data-target="#contact">Got a Question? Ask The Coach</a>
        @else
            <p><i class="fas fa-lock fa-fw"></i> <strong>Upgrade to <a href="/settings#/subscription">EVENPULSE Premium</a> to ask the coach a question.</strong></p>
        @endif
    </div>
@endif
