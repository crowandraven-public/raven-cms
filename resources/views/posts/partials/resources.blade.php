@if (is_var_iterable($post->resources))
    <div class="marginbottom">
        <h3 class="section-title">Related Resources</h3>
        <ul>
        
        @foreach ($post->resources as $resource)
            <li><a href="{{ $resource->url }}">{{ localize($resource->title) }}</a></li>
        @endforeach

        </ul>
    </div>
@endif
