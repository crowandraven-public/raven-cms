@if (is_subscribed_basic(Auth::user()))
	@component('raven::partials.likeable', [
		'likeable_action_url' => '/post/like/'.$post->id,
		'post_type_name' => 'Post',
		'liked' => (Auth::user()->likedPosts && in_array($post->id, Auth::user()->likedPosts->toArray())) ? '1': '0',
		'liked_class' => 'fas fa-bookmark fa-fw saved pointer',
		'not_liked_class' => 'fas fa-bookmark fa-fw pointer',
		'tooltip_title_liked' => 'Post Saved',
		'tooltip_title_not_liked' => 'Save this Post'
	])
	@endcomponent
@endif


