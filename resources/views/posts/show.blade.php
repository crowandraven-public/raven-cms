@extends('raven::layouts.app')

@section('title', localize($post->title))
@section('description', localize($post->excerpt))
@section('image', '/img/'.$post->image.'?w=1200&h=630&fit=crop', env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'page post')

@section('content')
<div class="hero" style="{{ isset($post->image) ? 'background-image: url("/storage/uploads/'.$post->image.'?width=1800&height=550");' : 'http://placehold.it/2000x800' }}">
    <div class="hero-overlay"></div>
    <div class="container">
        <div class="row hero-content">
            <div class="col-md-8 title-container">
                <h1>{{ localize($post->title) }}</h1>
                <div class="sub-heading">
                    @if (localize($post->subtitle))
                        <div class="subtitle">
                            {{ localize($post->subtitle) }}
                        </div>
                    @endif
                    <div class="post-meta">
                        @include('raven::posts.partials.tags')
                        Published on {{ $post->published_at->toFormattedDateString() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8 margintop marginbottom">
            <div class="marginbottom">
                {!! localize($post->content) !!}
            </div>
        </div>
        <div class="col-md-4 sidebar marginbottom">
            @include('raven::posts.partials.author')
            @include('raven::posts.partials.share')
            @include('raven::posts.partials.resources')
        </div>
    </div>
</div>
@endsection

@section('fscripts')
@endsection