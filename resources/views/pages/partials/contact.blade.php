{!! Form::open(['url' => '/contact', 'role' => 'form']) !!}

    @unless (Auth::check())
        <div class="row form-group">
            {!! Form::label('name', 'Your Name', ['class' => 'col-md-12 control-label required']) !!}
            <div class="col-md-12">
                {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>

        <div class="row form-group">
            {!! Form::label('email', 'Your Email', ['class' => 'col-md-12 control-label required']) !!}
            <div class="col-md-12">
                {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
            </div>
        </div>
    @endunless

    <!-- Content -->
    <div class="row form-group">
        {!! Form::label('content', 'Message', ['class' => 'col-md-12 control-label required']) !!}
        <div class="col-md-12">
            {!! Form::textarea('content', null, ['class' => 'form-control', 'required' => 'required','rows' => '7']) !!}
        </div>
    </div>

    <!-- Submit Button -->
    <div class="row form-group">
        <div class="col-md-12">
            {!! Form::submit('Contact Us', ['class' => 'btn btn-primary btn-md margintop-sm']) !!}
        </div>
    </div>
{!! Form::close() !!}
