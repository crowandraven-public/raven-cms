@if (isset($page->image))
    <div class="hero d-flex align-items-center" style="{{ isset($page->image) ? 'background-image: url("/img/'.$page->image.'?width=1800&height=550");' : 'http://placehold.it/2000x800' }}">
        <div class="container">
            <div class="row hero-content">
                <div class="col-md-8 title-container">
                    <h1>{{ localize($page->title) }}</h1>
                    @if (localize($page->subtitle))
                        <div class="sub-heading">
                            {{ localize($page->subtitle) }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@else
    <div class="no-hero d-flex align-items-center">
        <div class="container">
            <div class="row hero-content">
                <div class="col-md-6 title-container">
                    <h1>{{ localize($page->title) }}</h1>
                    @if (localize($page->subtitle))
                        <div class="sub-heading">
                            {{ localize($page->subtitle) }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif
