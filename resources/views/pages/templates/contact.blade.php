@extends('raven::layouts.app')

@section('title', localize($page->title))
@section('description', localize($page->excerpt))
@section('image', env('APP_URL').$page->image, env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'page')

@section('content')
@include('raven::pages.partials.hero')

<div class="container">
    <div class="row">
        <div class="col-md-6 margintop marginbottom">
            {!! localize($page->content) !!}
        </div>
        <div class="col-md-6 margintop marginbottom">
            <h2>Have a Question?</h2>
            @include('pages.partials.contact')
        </div>
    </div>
</div>
@endsection

@section('fscripts')
@endsection