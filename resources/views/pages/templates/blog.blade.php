@extends('raven::layouts.app')

@section('title', localize($page->title))
@section('description', localize($page->excerpt))
@section('image', env('APP_URL').$page->image, env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'page')

@section('content')
@include('raven::pages.partials.hero')

@if (localize($page->content))
    <div class="container">
        <div class="row">
            <div class="col-md-8 margintop marginbottom">
                {!! localize($page->content) !!}
            </div>
        </div>
    </div>
@endif

@if ($posts)
    <div class="container">
        <div class="row">
            <div class="col-md-12 margintop marginbottom">
                
                @include('posts.partials.cards')

            </div>
        </div>
    </div>
@endif
@endsection

@section('fscripts')
@endsection