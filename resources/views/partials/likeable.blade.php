@php 
    $use_spans = (isset($uses_spans)) ? $use_spans : true;
    $post_type_name = (isset($post_type_name)) ? $post_type_name : '';
    $tooltip_title_liked = (isset($tooltip_title_liked)) ? $tooltip_title_liked : $post_type_name.'  Saved';
    $tooltip_title_not_liked = (isset($tooltip_title_not_liked)) ? $tooltip_title_not_liked : 'Save this '.$post_type_name;
@endphp


<div data-likeable="1"
    data-liked="{{ $liked }}"
    data-likeable-action-url="{{ $likeable_action_url }}"
    class="likeable likeable-type-likeable likeable-class-{{ strtolower($post_type_name) }}">

    @if ($use_spans)
        <span class="likeable-on on" data-toggle="tooltip" data-placement="top" title data-original-title="{{ $tooltip_title_liked }}">
            <i class="{{ $liked_class }}"></i>
        </span>

        <span class="likeable-off off" data-toggle="tooltip" data-placement="top" title data-original-title="{{ $tooltip_title_not_liked }}">
            <i class="{{ $not_liked_class }}"></i>
        </span>
    @endif
        
    {{ $slot }}
</div>
