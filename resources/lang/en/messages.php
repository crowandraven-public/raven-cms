<?php

return [
    'errors' => [
        '403'                           => 'It doesn\'t look like you have access to view this page.',
        '404'                           => 'We\'re sorry. That page was not found.',
        '500'                           => 'Hmm. Something went wrong.',
        '503'                           => 'We\'ll be back soon.',
        'flash' => [
            'auth' => [
                'title' => 'Authorization Error',
                'message' => 'You don\'t have permission to view that page.',
            ],
        ],
    ],
    'fieldgroups' => [
        'index' => [
            'title'                     => 'Field Groups',
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Field Group',
                'delete_message'        => 'Type the word DESTROY below to delete this field group.',
                'delete_confirm'        => 'Are you sure? Field Group deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Field Group',
            'form' => [
                'title'                 => 'Title',
                'page'                  => 'Page to Add Fields',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Field Group',
            'form' => [
                'title'                 => 'Title',
                'page'                  => 'Page to Add Fields',
                'fields'                => 'Fields',
                'duplicate'             => 'Duplicate This Field Group',
            ],
        ],
        'flash' => [
            'copy' => [
                'title'                 => 'Copied',
                'message'               => 'Field was duplicated. See in in the Field Groups dashboard.',
            ],
            'create' => [
                'success' => [
                    'title'             => 'Field Group Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Field Group Error',
                    'message'           => 'Hmmm, there was a problem creating the field group.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Field Group Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the field group.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Field Group Deleted',
                    'message'           => 'The field group was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that field group.',
                ]
            ]
        ],
    ],
    'lessons' => [
        'index' => [
            'title'                     => 'All Lessons',
            'search' => [
                'title'                 => 'Lessons with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Lessons',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'featured'              => 'Featured',
                'private'               => 'Private',
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Lesson',
                'delete_message'        => 'Type the word DESTROY below to delete this lesson.',
                'delete_confirm'        => 'Are you sure? Lesson deletion is permanent and irreversible.',
            ],
        ],
        'contact' => [
            'email' => [
                'subject'               => 'Lesson Question',
            ]
        ],
        'create' => [
            'title'                     => 'Add Lesson',
            'form' => [
                'title'                 => 'Title',
                'content'               => 'Content',
                'length'                => 'Length',
                'length_help'           => 'In seconds. Example: \'330\'',
                'config'                => 'Configuration',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'file'                  => 'File',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'quiz'                  => 'Quiz',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'resources'             => 'Related Resources',
                'add_resource'          => 'Add Resource',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Lesson',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'content'               => 'Content',
                'excerpt'               => 'Excerpt',
                'length'                => 'Length',
                'length_help'           => 'In seconds. Example: \'330\'',
                'config'                => 'Configuration',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'file'                  => 'File',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'quiz'                  => 'Quiz',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'resources'             => 'Related Resources',
                'add_resource'          => 'Add Resource',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Lesson Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Lesson Error',
                    'message'           => 'Hmmm, there was a problem creating the lesson.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Lesson Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the lesson.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Lesson Deleted',
                    'message'           => 'The lesson was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that lesson.',
                ]
            ]
        ]
    ],
    'media' => [
        'index' => [
            'title'                     => 'Media',
            'add_new'                   => 'Add New',
            'error'                     => 'There was an error displaying the file.',
        ],
        'create' => [
            'title'                     => 'Add Media',
            'form' => [
                'title'                 => 'Title',
                'file'                  => 'Media File',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Media',
            'form' => [
                'title'                 => 'Title',
            ],
        ],
        'delete' => [
            'title'                     => 'Delete Media',
            'form' => [
                'delete_message'        => 'Type the word DESTROY below to delete this file.',
                'delete_confirm'        => 'Are you sure? File deletion is permanent and irreversible.',
            ],
        ],
        'view' => [
            'title'                     => 'View Details',
            'subtitle'                  => 'Details',
            'file_title'                => 'Title',
            'file_url'                  => 'URL',
            'file_size'                 => 'Size',
            'file_type'                 => 'Type',
            'file_added'                => 'File Added',
            'file_updated'              => 'File Updated',
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Media Uploaded',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Media Error',
                    'message'           => 'Hmmm, there was a problem uploading the file.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Media Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the file.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Media Deleted',
                    'message'           => 'The file was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that file.',
                ]
            ]
        ]
    ],
    'pages' => [
        'index' => [
            'title'                     => 'All Pages',
            'search' => [
                'title'                 => 'Pages with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Pages',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'updated'               => 'Updated',
                'delete'                => 'Delete Page',
                'delete_message'        => 'Type the word DESTROY below to delete this page.',
                'delete_confirm'        => 'Are you sure? Page deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Page',
            'form' => [
                'title'                 => 'Title',
                'subtitle'              => 'Subtitle',
                'content'               => 'Content',
                'image'                 => 'Image',
                'template'              => 'Template',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Page',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'subtitle'              => 'Subtitle',
                'content'               => 'Content',
                'excerpt'               => 'Excerpt',
                'meta'                  => 'Meta',
                'image'                 => 'Image',
                'template'              => 'Template',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Page Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Page Error',
                    'message'           => 'Hmmm, there was a problem creating the page.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Page Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the page.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Page Deleted',
                    'message'           => 'The page was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that page.',
                ]
            ]
        ]
    ],
    'posts' => [
        'index' => [
            'title'                     => 'All Posts',
            'search' => [
                'title'                 => 'Posts with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Posts',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'featured'              => 'Featured',
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'type'                  => 'Type',
                'updated'               => 'Updated',
                'delete'                => 'Delete Post',
                'delete_message'        => 'Type the word DESTROY below to delete this post.',
                'delete_confirm'        => 'Are you sure? Post deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Post',
            'form' => [
                'title'                 => 'Title',
                'subtitle'              => 'Subtitle',
                'content'               => 'Content',
                'meta'                  => 'Meta',
                'excerpt'               => 'Excerpt',
                'excerpt_help'          => 'By default the first 200 characters of the content will be used.',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'resources'             => 'Related Resources',
                'add_resource'          => 'Add Resource',
                'attachments'           => 'Attachments',
                'image'                 => 'Image',
                'file'                  => 'File',
                'type'                  => 'Post Type',
                'status'                => 'Status',
                'status_help'           => 'If you want to publish in the future, set as published and enter the published date below.',
                'published'             => 'Published Date',
                'published_help'        => 'If left empty, it will default to now. Format: 2018-12-14 12:00:00',
                'is_featured'           => 'Is Featured',
                'select_one'            => 'Select One',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Post',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'subtitle'              => 'Subtitle',
                'content'               => 'Content',
                'meta'                  => 'Meta',
                'excerpt'               => 'Excerpt',
                'excerpt_help'          => 'By default the first 200 characters of the content will be used.',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'resources'             => 'Related Resources',
                'add_resource'          => 'Add Resource',
                'attachments'           => 'Attachments',
                'image'                 => 'Image',
                'file'                  => 'File',
                'type'                  => 'Post Type',
                'status'                => 'Status',
                'status_help'           => 'If you want to publish in the future, set as published and enter the published date below.',
                'published'             => 'Published Date',
                'published_help'        => 'If left empty, it will default to now. Format: 2018-12-14 12:00:00',
                'is_featured'           => 'Is Featured',
                'select_one'            => 'Select One',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Post Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Page Error',
                    'message'           => 'Hmmm, there was a problem creating the post.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Post Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the post.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Post Deleted',
                    'message'           => 'The post was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that post.',
                ]
            ]
        ]
    ],
    'quizzes' => [
        'index' => [
            'title'                     => 'All Quizzes',
            'search' => [
                'title'                 => 'Quizzes with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Quizzes',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Quiz',
                'delete_message'        => 'Type the word DESTROY below to delete this quiz.',
                'delete_confirm'        => 'Are you sure? Quiz deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Quiz',
            'form' => [
                'title'                 => 'Title',
                'content'               => 'Content',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'locale'                => 'Locale',
                'config'                => 'Configuration',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta'
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Quiz',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'content'               => 'Content',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'locale'                => 'Locale',
                'config'                => 'Configuration',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta'
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Quiz Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Quiz Error',
                    'message'           => 'Hmmm, there was a problem creating the quiz.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Quiz Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the quiz.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Quiz Deleted',
                    'message'           => 'The quiz was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that quiz.',
                ]
            ]
        ]
    ],
    'resources' => [
        'index' => [
            'title'                     => 'All Resources',
            'search' => [
                'title'                 => 'Resources with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Resources',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Resource',
                'delete_message'        => 'Type the word DESTROY below to delete this resource.',
                'delete_confirm'        => 'Are you sure? Resource deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Resource',
            'form' => [
                'title'                 => 'Title',
                'local'                 => 'Local File',
                'local_help'            => 'This will override anything you have set in Remote URL field.',
                'remote'                => 'or Remote URL',
                'remote_help'           => 'If exists, must be fully qualified URL and include http:// or https://',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Resource',
            'form' => [
                'title'                 => 'Title',
                'local'                 => 'Local File',
                'local_help'            => 'This will override anything you have set in Remote URL field.',
                'remote'                => 'or Remote URL',
                'remote_help'           => 'If exists, must be fully qualified URL and include http:// or https://',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Resource Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Resource Error',
                    'message'           => 'Hmmm, there was a problem creating the resource.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Resource Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the resource.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Resource Deleted',
                    'message'           => 'The resource was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that resource.',
                ]
            ]
        ]
    ],
    'attachments' => [
        'index' => [
            'title'                     => 'All Attachments',
            'search' => [
                'title'                 => 'Attachments with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Attachments',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Title',
                'thumb'                 => 'Thumb',
                'file_type'             => 'File Type',
                'updated'               => 'Updated',
                'delete'                => 'Delete Attachments',
                'delete_message'        => 'Type the word DESTROY below to delete this attachment.',
                'delete_confirm'        => 'Are you sure? Attachment deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Attachment',
            'form' => [
                'title'                 => 'Title',
                'description'           => 'Description',
                'alt_text'              => 'Alternative Text',
                'caption'               => 'Caption',
                'file'                  => 'File',
                'external_url'          => 'External URL',
                'remote'                => 'or Remote URL',
                'remote_help'           => 'If exists, must be fully qualified URL and include http:// or https://',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Attachment',
            'form' => [
                'title'                 => 'Title',
                'description'           => 'Description',
                'alt_text'              => 'Alternative Text',
                'caption'               => 'Caption',
                'replace_file'          => 'Replace existing file',
                'local'                 => 'Local File',
                'external_url'          => 'External URL',
                'local_help'            => 'This will override anything you have set in Remote URL field.',
                'remote'                => 'or Remote URL',
                'remote_help'           => 'If exists, must be fully qualified URL and include http:// or https://',
            ],
        ],
        'view' => [
            'title'                     => 'View Details',
            'subtitle'                  => 'Details',
            'file_title'                => 'Title',
            'file_url'                  => 'URL',
            'file_size'                 => 'Size',
            'file_type'                 => 'Type',
            'file_added'                => 'File Added',
            'file_updated'              => 'File Updated',
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Attachment Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Attachment Error',
                    'message'           => 'Hmmm, there was a problem creating the attachment.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Attachment Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the attachment.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Attachment Deleted',
                    'message'           => 'The attachment was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that attachment.',
                ]
            ]
        ]
    ],
    'roles' => [
        'index' => [
            'title'                     => 'All Roles',
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Role',
                'slug'                  => 'Slug',
                'updated'               => 'Updated',
                'delete'                => 'Delete Role',
                'delete_message'        => 'Type the word DESTROY below to delete this role.',
                'delete_confirm'        => 'Are you sure? Role deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Role',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'slug_help'             => 'Keep it simple. One word is best',
                'description'           => 'Description',
                'select_one'            => 'Select One',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Role',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'slug_help'             => 'Keep it simple. One word is best.',
                'description'           => 'Description',
                'select_one'            => 'Select One',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Role Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Role Error',
                    'message'           => 'Hmmm, there was a problem creating the role.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Role Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the role.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Role Deleted',
                    'message'           => 'The role was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that role.',
                ]
            ]
        ]
    ],
    'series' => [
        'index' => [
            'title'                     => 'All Series',
            'search' => [
                'title'                 => 'Series with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Series',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'featured'              => 'Featured',
                'private'               => 'Private',
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Series',
                'delete_message'        => 'Type the word DESTROY below to delete this series.',
                'delete_confirm'        => 'Are you sure? Series deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Series',
            'form' => [
                'title'                 => 'Title',
                'content'               => 'Content',
                'recommended_for'       => 'Recommended For',
                'testimony_text'        => 'Testimony Text',
                'testimony_attrib'      => 'Testimony Attribution',
                'config'                => 'Configuration',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
                'lessons'               => 'Lessons',
                'add_lesson'            => 'Add Lesson',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Series',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'content'               => 'Content',
                'excerpt'               => 'Excerpt',
                'recommended_for'       => 'Recommended For',
                'testimony_text'        => 'Testimony Text',
                'testimony_attrib'      => 'Testimony Attribution',
                'config'                => 'Configuration',
                'price'                 => 'Price',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
                'lessons'               => 'Lessons',
                'add_lesson'            => 'Add Lesson',
                'duplicate'             => 'Duplicate'
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Series Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Series Error',
                    'message'           => 'Hmmm, there was a problem creating the series.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Series Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the series.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Series Deleted',
                    'message'           => 'The series was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that series.',
                ]
            ],
            'copy' => [
                'title'                 => 'Copied',
                'message'               => 'Series was duplicated. See in in the Series dashboard.',
            ],
        ]
    ],
    'tags' => [
        'index' => [
            'title'                     => 'All Tags',
            'search' => [
                'title'                 => 'Tags with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Tags',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Tag',
                'slug'                  => 'Slug',
                'type'                  => 'Type',
                'updated'               => 'Updated',
                'delete'                => 'Delete Tag',
                'delete_message'        => 'Type the word DESTROY below to delete this tag.',
                'delete_confirm'        => 'Are you sure? Tag deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Tag',
            'form' => [
                'title'                 => 'Title',
                'type'                  => 'Type',
                'description'           => 'Description',
                'synonyms'              => 'Synonyms',
                'synonyms_help'         => 'This is optional and useful only if you need to build a search index.',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Tag',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'type'                  => 'Type',
                'description'           => 'Description',
                'synonyms'              => 'Synonyms',
                'synonyms_help'         => 'This is optional and useful only if you need to build a search index.',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Tag Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Tag Error',
                    'message'           => 'Hmmm, there was a problem creating the tag.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Tag Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the tag.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Tag Deleted',
                    'message'           => 'The tag was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that tag.',
                ]
            ]
        ]
    ],
    'users' => [
        'index' => [
            'title'                     => 'All Users',
            'search' => [
                'title'                 => 'Users with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Users',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Name',
                'email'                 => 'Email',
                'updated'               => 'Updated',
                'delete'                => 'Delete User',
                'delete_message'        => 'Type the word DESTROY below to delete this user.',
                'delete_confirm'        => 'Are you sure? User deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add User',
            'form' => [
                'title'                 => 'Name',
                'email'                 => 'Email',
                'email_help'            => 'Using this form does not send a welcome email to the user. You\'ll need to do this manually.',
                'password'              => 'Password',
                'password_help'         => 'Minimum of six characters are required. But seriously, you can do better than that.',
                'bio'                   => 'Bio',
                'bio_help'              => 'Limited to 250 characters. No HTML.',
                'is_admin'              => 'Is App Admin?',
                'roles'                 => 'Roles',
                'add_role'              => 'Add Role',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit User',
            'form' => [
                'title'                 => 'Name',
                'email'                 => 'Email',
                'email_help'            => 'Using this form does not send a welcome email to the user. You\'ll need to do this manually.',
                'password'              => 'Password',
                'password_help'         => 'Minimum of six characters are required. But seriously, you can do better than that.',
                'bio'                   => 'Bio',
                'bio_help'              => 'Limited to 250 characters. No HTML.',
                'is_admin'              => 'Is App Admin?',
                'roles'                 => 'Roles',
                'add_role'              => 'Add Role',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'User Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'User Error',
                    'message'           => 'Hmmm, there was a problem creating the user.',
                ],
                'email' => [
                    'title'             => 'User Error',
                    'message'           => 'That email is already in use.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'User Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the user.',
                ],
                'email' => [
                    'title'             => 'User Error',
                    'message'           => 'That email is already in use.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'User Deleted',
                    'message'           => 'The user was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that user.',
                ]
            ]
        ]
    ],
];
