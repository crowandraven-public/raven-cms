<?php

return [
    /*
    |--------------------------------------------------------------------------
    | CMS Localization
    |--------------------------------------------------------------------------
    |
    | Any language/locale you want to offer needs to be listed here. If
    | you don't specify any locale, the CMS will just assume you are
    | not interested in any translations, and it pick your locale
    | to use for all content. If you want a field translated
    | you'll need to use the JSON MySQL field type.
    |
    */
    
    // 'locale' => 'en',
    'locale' => ['en', 'es'],

    /*
    |--------------------------------------------------------------------------
    | CMS Default Locale
    |--------------------------------------------------------------------------
    |
    | This value determines the default locale that will be used by the
    | translation service provider. You are free to set this value
    | to any of the locales which will be supported by the CMS.
    |
    */
   
    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | CMS Navigation
    |--------------------------------------------------------------------------
    |
    | This array determines the navigational structure of the CMS dashboard.
    | You are free to add to this, reorder, or delete navigational items.
    | There may be functionality listed here that your app does not
    | need. If that's the case, just comment it out or delete.
    | The icons used are from the Font Awesome 5 library.
    |
    */
   
    'nav' => [
        [
            'name' => 'Admin Home',
            'path' => 'admin',
            'icon' => 'fa-home'
        ],
        [
            'name' => 'Attachments',
            'path' => 'admin/attachments',
            'icon' => 'fa-link'
        ],
        [
            'name' => 'Pages',
            'path' => 'admin/pages',
            'icon' => 'fa-file'
        ],
        [
            'name' => 'Page Field Groups',
            'path' => 'admin/fieldgroups',
            'icon' => 'fa-folder-open'
        ],
        [
            'name' => 'Posts',
            'path' => 'admin/posts',
            'icon' => 'fa-file'
        ],
        [
            'name' => 'Tags',
            'path' => 'admin/tags',
            'icon' => 'fa-tag'
        ],
        [
            'name' => 'Users',
            'path' => 'admin/users',
            'icon' => 'fa-users'
        ],
        [
            'name' => 'User Roles',
            'path' => 'admin/roles',
            'icon' => 'fa-user-secret'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Display Properties
    |--------------------------------------------------------------------------
    |
    | The CMS comes with quite a bit out of the box. Perhaps you don't need
    | everything and want to hide some particular functionality so the
    | admin doesn't see something that is of no use to them. This is
    | also, where you can "turn on" new functionality that gets
    | added to the CMS over time.
    |
    */
   
    'show' => [
        'attachments' => [],
        'fieldgroups' => [],
        'pages' => [
            'subtitle' => 1,
            'excerpt' => 1,
            'image' => 1,
            'attachments' => 1
        ],
        'posts' => [
            'type' => 1,
            'subtitle' => 1,
            'excerpt' => 1,
            'file' => 1,
            'image' => 1,
            'author' => 1,
            'track_tags' => 1,
            'topic_tags' => 1,
            'resources' => 1,
            'is_featured' => 1,
            'attachments' => 1
        ],
        'roles' => [],
        'tags' => [],
        'users' => [
            'bio' => 1
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | CMS Tag Categories
    |--------------------------------------------------------------------------
    |
    | This array determines the different kinds of tags available for your
    | application. You are free to add as many (or delete) kinds. For
    | starters we'll include the ones you'll most likely want: 'topic'
    | is used for tagging content for various categorization and
    | 'type' is used for identifying various post types (blog,
    | news, articles, etc).
    |
    */
   
    'tag-category' => [
        'topic' => 'Topic',
        'track' => 'Track',
        'type' => 'Post Type'
    ]
];
