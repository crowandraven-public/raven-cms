<?php

namespace CrowAndRaven\CMS\Test;

use CMS;

class CMSFunctionTest extends TestCase
{
    /**
     * Check that the multiply method returns correct result
     * @return void
     */
    public function testTest()
    {
        $this->assertSame(raven::test(), 'Hello World!');
    }
}
