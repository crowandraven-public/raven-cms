<?php
namespace CrowAndRaven\CMS\Test;
use crowandraven\CMS\CMSFacade;
use crowandraven\CMS\CMSServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return CrowAndRaven\CMS\CMSServiceProvider
     */
    protected function getPackageProviders($app)
    {
        return [CMSServiceProvider::class];
    }
    /**
     * Load package alias
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageAliases($app)
    {
        return [
            'CMS' => CMSFacade::class,
        ];
    }
}